package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-29.
 */
public class Kategorie_Ważności {
    private int poziomWażności_PK;
    private String symbol, opis;
    private double kwotaDozwolona;

    public Kategorie_Ważności(){}

    public Kategorie_Ważności(int poziom, String symbol, double kwota, String opis){
        poziomWażności_PK=poziom;
        this.symbol = symbol;
        kwotaDozwolona=kwota;
        this.opis=opis;
    }


    //region Gettery/Settery

    public int getPoziomWażności_PK() {
        return poziomWażności_PK;
    }

    public void setPoziomWażności_PK(int poziomWażności_PK) {
        this.poziomWażności_PK = poziomWażności_PK;
    }


    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


    public double getKwotaDozwolona() {
        return kwotaDozwolona;
    }

    public void setKwotaDozwolona(double kwotaDozwolona) {
        this.kwotaDozwolona = kwotaDozwolona;
    }


    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_KategorieWażności), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaKategoriiWażności.add(new Kategorie_Ważności(kursor.getInt(0), kursor.getString(1), kursor.getDouble(2), kursor.getString(3)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] wartości = {
                String.valueOf(poziomWażności_PK),
                symbol,
                String.valueOf(kwotaDozwolona),
                opis
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_KategorieWażności, wartości));

        połączenieBD.rozłączBD();
    }
}
