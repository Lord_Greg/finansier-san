package com.example.greg.finansier_san.Dialogi;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.Date;

/**
 * Created by Admin on 29.12.15.
 */
public class Dialog_Wyboru_Daty {

    public final Date nowaData = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), Zmienne_Globalne.wybranaData.getDate());;

    Button btnPlusDzień;
    Button btnPlusMiesiąc;
    Button btnPlusRok;
    public EditText etxnDzień;
    public Spinner spnMiesiąc;
    public EditText etxnRok;
    Button btnMinusDzień;
    Button btnMinusMiesiąc;
    Button btnMinusRok;
    public Button btnOk;
    public Button btnAnuluj;
    public Dialog dialog;

    public void działajBezOk(Context kontekst){
        dialog = new Dialog(kontekst);
        dialog.setTitle(Wyświetlanie_Tekstu.tytułyNazwaDialoguZmianaDaty());
        dialog.setContentView(R.layout.dialog_wyboru_daty);
        dialog.show();

        nowaData.setYear(Zmienne_Globalne.wybranaData.getYear());
        nowaData.setMonth(Zmienne_Globalne.wybranaData.getMonth());
        nowaData.setDate(Zmienne_Globalne.wybranaData.getDate());

        //region Inicjalizacja kontrolek
        btnPlusDzień = (Button) dialog.findViewById(R.id.btnPlusDzien);
        btnPlusMiesiąc = (Button) dialog.findViewById(R.id.btnPlusMiesiac);
        btnPlusRok = (Button) dialog.findViewById(R.id.btnPlusRok);
         etxnDzień = (EditText) dialog.findViewById(R.id.etxnDzien);
         spnMiesiąc = (Spinner) dialog.findViewById(R.id.spnMiesiac);
         etxnRok = (EditText) dialog.findViewById(R.id.etxnRok);
        btnMinusDzień = (Button) dialog.findViewById(R.id.btnMinusDzien);
        btnMinusMiesiąc = (Button) dialog.findViewById(R.id.btnMinusMiesiac);
        btnMinusRok = (Button) dialog.findViewById(R.id.btnMinusRok);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnAnuluj = (Button) dialog.findViewById(R.id.btnAnuluj);

        //region Ustawianie tekstu kontrolek
        btnOk.setText(Wyświetlanie_Tekstu.gui_nagłówek_ok());
        btnAnuluj.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());


        String[] miesiące = new String[12];
        Metody_Pomocnicze.wczytajNazwyMiesięcy(miesiące);
        ArrayAdapter adapter = new ArrayAdapter(kontekst, R.layout.styl_elementu_spinnera, miesiące);
        spnMiesiąc.setAdapter(adapter);
        etxnDzień.setText(String.valueOf(nowaData.getDate()));
        spnMiesiąc.setSelection(nowaData.getMonth());
        etxnRok.setText(String.valueOf(nowaData.getYear() + Zmienne_Globalne.y));

        //endregion

        //endregion

        //region Buttony +/-
        btnPlusDzień.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dzień = Integer.parseInt(etxnDzień.getText().toString());
                if (dzień + 1 > Metody_Pomocnicze.wyznaczIlośćDniWmiesiącu(nowaData)) {
                    dzień = 1;
                } else {
                    dzień++;
                }

                nowaData.setDate(dzień);
                etxnDzień.setText(String.valueOf(dzień));
            }
        });

        btnMinusDzień.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dzień = Integer.parseInt(etxnDzień.getText().toString());
                if (dzień - 1 < 1) {
                    dzień = Metody_Pomocnicze.wyznaczIlośćDniWmiesiącu(nowaData);
                } else {
                    dzień--;
                }

                nowaData.setDate(dzień);
                etxnDzień.setText(String.valueOf(dzień));
            }
        });

        btnPlusMiesiąc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int miesiąc = (int) spnMiesiąc.getSelectedItemId();
                if (miesiąc + 1 > 11) {
                    miesiąc = 0;
                } else {
                    miesiąc++;
                }

                nowaData.setMonth(miesiąc);
                spnMiesiąc.setSelection(miesiąc);
            }
        });

        btnMinusMiesiąc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int miesiąc = (int)spnMiesiąc.getSelectedItemId();
                if (miesiąc - 1 < 0) {
                    miesiąc = 11;
                } else {
                    miesiąc--;
                }

                nowaData.setMonth(miesiąc);
                spnMiesiąc.setSelection(miesiąc);
            }
        });

        btnPlusRok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rok = Integer.parseInt(etxnRok.getText().toString());
                rok++;
                nowaData.setYear(rok - Zmienne_Globalne.y);
                etxnRok.setText(String.valueOf(rok));
            }
        });

        btnMinusRok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rok = Integer.parseInt(etxnRok.getText().toString());
                if (rok - 1 > 0) {
                    rok--;
                }
                nowaData.setYear(rok - Zmienne_Globalne.y);
                etxnRok.setText(String.valueOf(rok));
            }
        });
        //endregion


        btnAnuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        etxnDzień.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Metody_Pomocnicze.przepiszDzień(etxnDzień, nowaData);
                }
            }
        });


        etxnRok.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    Metody_Pomocnicze.przepiszRok(etxnRok, nowaData);
                }
            }
        });
    }

    public void czynnościKońcowe(){
        Metody_Pomocnicze.przepiszRok(etxnRok, nowaData);
        nowaData.setMonth(spnMiesiąc.getSelectedItemPosition());
        Metody_Pomocnicze.przepiszDzień(etxnDzień, nowaData);

        Metody_Pomocnicze.przepiszDate(nowaData, Zmienne_Globalne.wybranaData);
    }
}