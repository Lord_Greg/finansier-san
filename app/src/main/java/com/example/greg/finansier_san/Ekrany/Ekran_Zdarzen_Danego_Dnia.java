package com.example.greg.finansier_san.Ekrany;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Klucze_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Greg on 2015-12-02.
 */
public class Ekran_Zdarzen_Danego_Dnia extends AppCompatActivity {

    TextView txvData;
    ListView lvwZdarzenia;
    Button btnDodajZdarzenie;


    List<Klucze_Zdarzeń> listaZdarzeńDanegoDnia = new ArrayList<Klucze_Zdarzeń>();
    boolean czyPierwszyRaz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_zdarzen_danego_dnia);

        czyPierwszyRaz=true;

        //region Inicjalizacja kontrolek
        txvData = (TextView)findViewById(R.id.txvData);
        lvwZdarzenia = (ListView)findViewById(R.id.lvwListaZdarzen);
        btnDodajZdarzenie = (Button)findViewById(R.id.btnDodaj);

        btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.gui_Nagłówek_dodajZdarzenie());

        przeładujWyświetlanąDatę();
        przeładujZdarzenia(lvwZdarzenia);

        //endregion

        btnDodajZdarzenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Ekran_Zdarzen_Danego_Dnia.this)
                        .setTitle(Wyświetlanie_Tekstu.gui_Nagłówek_dodajZdarzenie())
                        .setMessage(Wyświetlanie_Tekstu.guiDodajNowe())
                        .setPositiveButton(Wyświetlanie_Tekstu.guiPojedynczeZdarzenie(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(Ekran_Zdarzen_Danego_Dnia.this, Ekran_Wprowadzania_Zdarzen.class);
                                startActivity(i);
                            }
                        })
                        .setNegativeButton(Wyświetlanie_Tekstu.guiSeriaZdarzeń(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(Ekran_Zdarzen_Danego_Dnia.this, Ekran_Wprowadzania_Serii_Zdarzen.class);
                                startActivity(i);
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_info)//ic_dialog_alert
                        .show();

            }
        });

        lvwZdarzenia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Ekran_Zdarzen_Danego_Dnia.this, Ekran_Przegladania_Zdarzenia.class);
                i.putExtra("czyToRozchód", listaZdarzeńDanegoDnia.get(position).getCzyToRozchód());
                i.putExtra("nazwa", listaZdarzeńDanegoDnia.get(position).getNazwaZdarzenia());
                i.putExtra("numer", listaZdarzeńDanegoDnia.get(position).getNumerWystąpienia());
                startActivity(i);
            }
        });


    }


    protected void onStart(){
        super.onStart();

        if(!czyPierwszyRaz){
            przeładujWyświetlanąDatę();
            przeładujZdarzenia(lvwZdarzenia);
        }
        else {
            czyPierwszyRaz=false;
        }
    }


    private void przeładujWyświetlanąDatę(){
        txvData.setText(String.valueOf(Zmienne_Globalne.wybranaData.getDate()) + " " + Metody_Pomocnicze.zwróćNazwęMiesiąca(Zmienne_Globalne.wybranaData.getMonth()) + " " + String.valueOf(Zmienne_Globalne.wybranaData.getYear()+Zmienne_Globalne.y));
    }

    private void przeładujZdarzenia(ListView lvwZdarzenia){
        listaZdarzeńDanegoDnia = new ArrayList<Klucze_Zdarzeń>();
        ArrayAdapter adapter;
        List<String> daneDoAdaptera = new ArrayList<String>();
        int pozycja=0;
        String danaDoAdaptera;
        for (Rozchody rozchód: Zmienne_Globalne.listaRozchodów) {
            if(rozchód.getData().getDate()==Zmienne_Globalne.wybranaData.getDate() && rozchód.getData().getMonth()==Zmienne_Globalne.wybranaData.getMonth() && rozchód.getData().getYear() == Zmienne_Globalne.wybranaData.getYear()){
                danaDoAdaptera = Wyświetlanie_Tekstu.guiSymbolRozchodu() + rozchód.getNazwaGrupy_PK_FK();
                for (Grupy_Zdarzeń grupa: Zmienne_Globalne.listaGrupZdarzeń) {
                    if(grupa.getNazwaGrupyZdarzeń_PK().equals(rozchód.getNazwaGrupy_PK_FK())){
                        if(grupa.getCzyJawnaNumeracja()) {
                            for (Jednostki jednostka : Zmienne_Globalne.listaJednostek) {
                                if (jednostka.getId_PK() == grupa.getIdJednostki_FK()) {
                                    danaDoAdaptera += " " + jednostka.getSkrót() + String.valueOf(rozchód.getNumerWystąpienia_PK());
                                    break;
                                }
                            }
                        }
                        listaZdarzeńDanegoDnia.add(new Klucze_Zdarzeń(pozycja, true, rozchód.getNazwaGrupy_PK_FK(), rozchód.getNumerWystąpienia_PK()));
                        daneDoAdaptera.add(danaDoAdaptera);
                        pozycja++;
                        break;
                    }

                }
            }
        }
        for (Przychody przychód: Zmienne_Globalne.listaPrzychodów) {
            if(przychód.getData().getDate()==Zmienne_Globalne.wybranaData.getDate() && przychód.getData().getMonth()==Zmienne_Globalne.wybranaData.getMonth() && przychód.getData().getYear() == Zmienne_Globalne.wybranaData.getYear()){
                danaDoAdaptera = Wyświetlanie_Tekstu.guiSymbolPrzychodu() + przychód.getNazwaGrupy_PK_FK();
                for (Grupy_Zdarzeń grupa: Zmienne_Globalne.listaGrupZdarzeń) {
                    if(grupa.getNazwaGrupyZdarzeń_PK().equals(przychód.getNazwaGrupy_PK_FK())){
                        if(grupa.getCzyJawnaNumeracja()) {
                            for (Jednostki jednostka : Zmienne_Globalne.listaJednostek) {
                                if (jednostka.getId_PK() == grupa.getIdJednostki_FK()) {
                                    danaDoAdaptera += " " + jednostka.getSkrót() + String.valueOf(przychód.getNumerWystąpienia_PK());
                                    break;
                                }
                            }
                        }
                        listaZdarzeńDanegoDnia.add(new Klucze_Zdarzeń(pozycja, false, przychód.getNazwaGrupy_PK_FK(), przychód.getNumerWystąpienia_PK()));
                        daneDoAdaptera.add(danaDoAdaptera);
                        pozycja++;
                        break;
                    }

                }
            }
        }


        adapter = new ArrayAdapter(Ekran_Zdarzen_Danego_Dnia.this, android.R.layout.simple_list_item_1, daneDoAdaptera);
        lvwZdarzenia.setAdapter(adapter);
    }

}
