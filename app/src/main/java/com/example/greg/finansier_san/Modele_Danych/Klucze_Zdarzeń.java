package com.example.greg.finansier_san.Modele_Danych;

/**
 * Created by Admin on 05.01.16.
 */
public class Klucze_Zdarzeń {
    private String nazwaZdarzenia;
    private int numerWystąpienia, id;
    private boolean czyToRozchód;

    public Klucze_Zdarzeń(){}
    public Klucze_Zdarzeń(int id, boolean czyToRozchód, String nazwaZdarzenia, int nrWystąpienia){
        this.id=id;
        this.czyToRozchód=czyToRozchód;
        this.nazwaZdarzenia=nazwaZdarzenia;
        numerWystąpienia=nrWystąpienia;
    }

    //region Gettery/Settery
    public String getNazwaZdarzenia() {
        return nazwaZdarzenia;
    }

    public void setNazwaZdarzenia(String nazwaZdarzenia) {
        this.nazwaZdarzenia = nazwaZdarzenia;
    }

    public int getNumerWystąpienia() {
        return numerWystąpienia;
    }

    public void setNumerWystąpienia(int numerWystąpienia) {
        this.numerWystąpienia = numerWystąpienia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean getCzyToRozchód() {
        return czyToRozchód;
    }

    public void setCzyToRozchód(boolean czyToRozchód) {
        this.czyToRozchód = czyToRozchód;
    }
    //endregion
}
