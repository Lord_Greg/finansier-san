package com.example.greg.finansier_san.Ekrany;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Zdarzenia_w_Scenariuszach;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 13.01.16.
 */
public class Ekran_Glowny_Lista extends Activity {

    ListView lvwBieżąceZdarzenia, lvwNadchodząceZdarzenia;
    List<Rozchody> listaBieżącychRozchodów = new ArrayList<Rozchody>();
    List<Rozchody> listaNadchodzącychRozchodów = new ArrayList<Rozchody>();
    boolean czyPierwszyRaz;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_glowny_lista);

        czyPierwszyRaz=true;

        //region Inicjalizacja kontrolek
        lvwBieżąceZdarzenia = (ListView) findViewById(R.id.lvwBiezaceZdarzenia);
        lvwNadchodząceZdarzenia = (ListView) findViewById(R.id.lvwNadchodzaceZdarzenia);

        TextView txvNaglowekBiezaceZdarzenia = (TextView)findViewById(R.id.txvNaglowekBiezaceZdarzenia);
        TextView txvNaglowekNadchodzaceZdarzenia = (TextView)findViewById(R.id.txvNaglowekNadchodzaceZdarzenia);

        txvNaglowekBiezaceZdarzenia.setText(Wyświetlanie_Tekstu.gui_Nagłówek_listaZakupów());
        txvNaglowekNadchodzaceZdarzenia.setText(Wyświetlanie_Tekstu.gui_Nagłówek_nadchodzące());

        przeładujListViewBieżącychZdarzeń();
        //endregion

        lvwBieżąceZdarzenia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{
                    Intent i = new Intent(Ekran_Glowny_Lista.this, Ekran_Przegladania_Zdarzenia.class);
                    i.putExtra("czyToRozchód", true);
                    i.putExtra("nazwa", listaBieżącychRozchodów.get(position).getNazwaGrupy_PK_FK());
                    i.putExtra("numer", listaBieżącychRozchodów.get(position).getNumerWystąpienia_PK());

                    startActivity(i);
                }
                catch (Exception e){};
            }
        });

        lvwNadchodząceZdarzenia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{
                    Intent i = new Intent(Ekran_Glowny_Lista.this, Ekran_Przegladania_Zdarzenia.class);
                    i.putExtra("czyToRozchód", true);
                    i.putExtra("nazwa", listaNadchodzącychRozchodów.get(position).getNazwaGrupy_PK_FK());
                    i.putExtra("numer", listaNadchodzącychRozchodów.get(position).getNumerWystąpienia_PK());

                    startActivity(i);
                }
                catch (Exception e){}
            }
        });

    }

    protected void onStart(){
        super.onStart();

        if(!czyPierwszyRaz) {
            przeładujListViewBieżącychZdarzeń();
        }
        else{
            czyPierwszyRaz=false;
        }

    }


    private void przeładujListViewBieżącychZdarzeń(){
        przeładujListyBieżącychINadchodzącychRozchodów();

        List <String> daneDoAdaptera = new ArrayList<String>();
        for (Rozchody rozchód : listaBieżącychRozchodów){
            if(!rozchód.getCzyPosiadane()) {
                String tekstDoWyświetlenia = rozchód.getNazwaGrupy_PK_FK();
                Grupy_Zdarzeń grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(rozchód.getNazwaGrupy_PK_FK());
                if (grupa.getCzyJawnaNumeracja()) {
                    tekstDoWyświetlenia += " " + Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK()).getSkrót() + String.valueOf(rozchód.getNumerWystąpienia_PK());
                }

                tekstDoWyświetlenia += "\t (" + String.valueOf(rozchód.getData().getDate()) + "." + String.valueOf(rozchód.getData().getMonth() + 1) + "." + String.valueOf(rozchód.getData().getYear() + Zmienne_Globalne.y) + ")";

                daneDoAdaptera.add(new String(tekstDoWyświetlenia));
            }
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Glowny_Lista.this, R.layout.styl_listview_listy_biezacych_zdarzen, daneDoAdaptera);
        lvwBieżąceZdarzenia.setAdapter(adapter);


        daneDoAdaptera = new ArrayList<String>();
        for (Rozchody rozchód : listaNadchodzącychRozchodów){
            if(!rozchód.getCzyPosiadane()) {
                String tekstDoWyświetlenia = rozchód.getNazwaGrupy_PK_FK();
                Grupy_Zdarzeń grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(rozchód.getNazwaGrupy_PK_FK());
                if (grupa.getCzyJawnaNumeracja()) {
                    tekstDoWyświetlenia += " " + Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK()).getSkrót() + String.valueOf(rozchód.getNumerWystąpienia_PK());
                }

                tekstDoWyświetlenia += "\t (" + String.valueOf(rozchód.getData().getDate()) + "." + String.valueOf(rozchód.getData().getMonth() + 1) + "." + String.valueOf(rozchód.getData().getYear() + Zmienne_Globalne.y) + ")";

                daneDoAdaptera.add(new String(tekstDoWyświetlenia));
            }
        }
        adapter = new ArrayAdapter(Ekran_Glowny_Lista.this, R.layout.styl_listview_listy_biezacych_zdarzen, daneDoAdaptera);
        lvwNadchodząceZdarzenia.setAdapter(adapter);
    }

    private void przeładujListyBieżącychINadchodzącychRozchodów(){
        listaBieżącychRozchodów = new ArrayList<Rozchody>();
        listaNadchodzącychRozchodów = new ArrayList<Rozchody>();

        Date dataPomocnicza = new Date();
        Metody_Pomocnicze.przepiszDate(Zmienne_Globalne.dzisiejszaData, dataPomocnicza);
        dataPomocnicza.setDate(dataPomocnicza.getDate() + 30);

        for (Rozchody rozchód : Zmienne_Globalne.listaRozchodów){
            if(!rozchód.getCzyPosiadane()){
                if(rozchód.getData().getYear() < dataPomocnicza.getYear() ||
                        (rozchód.getData().getYear() == dataPomocnicza.getYear() && ( rozchód.getData().getMonth() < dataPomocnicza.getMonth() ||
                                (rozchód.getData().getMonth() == dataPomocnicza.getMonth() && rozchód.getData().getDate() <= dataPomocnicza.getDate() ) ) ) ){
                    if(rozchód.getData().getYear() < Zmienne_Globalne.dzisiejszaData.getYear() ||
                            (rozchód.getData().getYear() == Zmienne_Globalne.dzisiejszaData.getYear() && ( rozchód.getData().getMonth() < Zmienne_Globalne.dzisiejszaData.getMonth() ||
                                    (rozchód.getData().getMonth() == Zmienne_Globalne.dzisiejszaData.getMonth() && rozchód.getData().getDate() <= Zmienne_Globalne.dzisiejszaData.getDate() ) ) ) ){
                        for (Zdarzenia_w_Scenariuszach zdarzenie : Zmienne_Globalne.listaZdarzeńWscenariuszach) {
                            if (zdarzenie.getNazwaZdarzenia_PK_FK().equals(rozchód.getNazwaGrupy_PK_FK()) && zdarzenie.getNumerWystąpieniaRozchodu_PK_FK() == rozchód.getNumerWystąpienia_PK() && zdarzenie.getNazwaScenariusza_PK_FK().equals(Zmienne_Globalne.aktualnyScenariusz)) {
                                listaBieżącychRozchodów.add(rozchód);
                                break;
                            }
                        }
                    }
                    else{
                        for (Zdarzenia_w_Scenariuszach zdarzenie : Zmienne_Globalne.listaZdarzeńWscenariuszach) {
                            if (zdarzenie.getNazwaZdarzenia_PK_FK().equals(rozchód.getNazwaGrupy_PK_FK()) && zdarzenie.getNumerWystąpieniaRozchodu_PK_FK() == rozchód.getNumerWystąpienia_PK() && zdarzenie.getNazwaScenariusza_PK_FK().equals(Zmienne_Globalne.aktualnyScenariusz)) {
                                listaNadchodzącychRozchodów.add(rozchód);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }


}
