package com.example.greg.finansier_san.Modele_Danych;

/**
 * Created by Greg on 2015-11-29.
 */
public class Waluty {
    private String nazwaWaluty, symbolWaluty;

    public Waluty(String nazwa, String symbol){
        nazwaWaluty=nazwa;
        symbolWaluty=symbol;
    }

    //region Gettery/Settery
    public String getNazwaWaluty() {
        return nazwaWaluty;
    }

    public void setNazwaWaluty(String nazwaWaluty) {
        this.nazwaWaluty = nazwaWaluty;
    }


    public String getSymbolWaluty() {
        return symbolWaluty;
    }

    public void setSymbolWaluty(String symbolWaluty) {
        this.symbolWaluty = symbolWaluty;
    }
    //endregion
}
