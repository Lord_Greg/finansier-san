package com.example.greg.finansier_san.Ekrany;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Grup_Zdarzen;
import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Jednostek;
import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Kategorii_Zdarzen;
import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 15.01.16.
 */
public class Ekran_Zarzadzania extends AppCompatActivity {

    ListView lvwZarządzaneElementy;
    String czymZarządzamy;
    int rozważanaPozycja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_zarzadzania);

        TextView txvNZarządzaj = (TextView)findViewById(R.id.txvEkranZarzadzania);

        lvwZarządzaneElementy = (ListView) findViewById(R.id.lvwZarzadzaneElementy);

        Bundle odebraneDane = getIntent().getExtras();
        if (odebraneDane != null) { //zawsze prawdziwe
            czymZarządzamy = odebraneDane.getString("coTo");

            if (czymZarządzamy.equals("jednostki")) {
                txvNZarządzaj.setText(Wyświetlanie_Tekstu.gui_Nagłówek_zarządzanieJednostkami());
                przeładujJednostki();
            } else {
                if (czymZarządzamy.equals("kategorie")) {
                    txvNZarządzaj.setText(Wyświetlanie_Tekstu.gui_Nagłówek_zarządzanieKategoriami());
                    przeładujKategorie();
                } else {
                    if (czymZarządzamy.equals("zdarzenia")) {
                        txvNZarządzaj.setText(Wyświetlanie_Tekstu.gui_Nagłówek_zarządzanieZdarzeniami());
                        przeładujZdarzenia();
                    }
                }
            }
        }


        lvwZarządzaneElementy.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(czymZarządzamy.equals("zdarzenia")){
                    Intent i = new Intent(Ekran_Zarzadzania.this, Ekran_Zdarzen_Danej_Grupy.class);
                    i.putExtra("nazwaGrupy", lvwZarządzaneElementy.getItemAtPosition(position).toString());
                    startActivity(i);
                }
                else{
                    if(czymZarządzamy.equals("kategorie")){
                        Intent i = new Intent(Ekran_Zarzadzania.this, Ekran_Grup_Danej_Kategorii.class);
                        i.putExtra("nazwaKategorii", lvwZarządzaneElementy.getItemAtPosition(position).toString());
                        startActivity(i);
                    }
                }
            }
        });

        lvwZarządzaneElementy.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                rozważanaPozycja = position;
                if (rozważanaPozycja != 0 || czymZarządzamy.equals("zdarzenia")) { // 0 są domyślne
                    registerForContextMenu(view);
                }
                return false;
            }
        });
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(czymZarządzamy);
        menu.add(0, v.getId(), 0, Wyświetlanie_Tekstu.czynnościEdytuj());
        menu.add(0, v.getId(), 0, Wyświetlanie_Tekstu.czynnościUsuń());
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
            if (item.getTitle().toString().equals(Wyświetlanie_Tekstu.czynnościUsuń())) {
                usuńElement(rozważanaPozycja);
            } else {
                if (item.getTitle().toString().equals(Wyświetlanie_Tekstu.czynnościEdytuj())) {
                    edytujElement(rozważanaPozycja);
                } else {
                    return false;
                }
            }
        return true;
    }


    private void usuńElement(final int pozycjaElementu) {
        new AlertDialog.Builder(Ekran_Zarzadzania.this)
                .setTitle(Wyświetlanie_Tekstu.czynnościUsuń())
                .setMessage(Wyświetlanie_Tekstu.guiCzyNapewnoUsunąć())
                .setPositiveButton(Wyświetlanie_Tekstu.guiTak(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Usuń

                        if (czymZarządzamy.equals("jednostki")) {
                            try {
                                Jednostki usuwanaJednostka = Zmienne_Globalne.listaJednostek.get(pozycjaElementu);

                                usuwanaJednostka.usuńZbazy(Ekran_Zarzadzania.this);

                                Zmienne_Globalne.listaJednostek.remove(pozycjaElementu);
                                Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Zarzadzania.this);

                                przeładujJednostki();

                                Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.guiUsunięto());
                            } catch (Exception e) {
                                Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.błądUsuwania());
                            }
                        } else if (czymZarządzamy.equals("kategorie")) {
                            try {
                                Kategorie_Zdarzeń usuwanaKategoria = Zmienne_Globalne.listaKategoriiZdarzeń.get(pozycjaElementu);

                                usuwanaKategoria.usuńZbazy(Ekran_Zarzadzania.this);

                                Zmienne_Globalne.listaKategoriiZdarzeń.remove(pozycjaElementu);
                                Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Zarzadzania.this);

                                przeładujKategorie();

                                Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.guiUsunięto());
                            } catch (Exception e) {
                                Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.błądUsuwania());
                            }


                        } else if (czymZarządzamy.equals("zdarzenia")) {

                            final Grupy_Zdarzeń usuwanaGrupa = Zmienne_Globalne.listaGrupZdarzeń.get(pozycjaElementu);

                            if (Metody_Pomocnicze.czyGrupaMaWystąpienia(usuwanaGrupa.getNazwaGrupyZdarzeń_PK())) {
                                new AlertDialog.Builder(Ekran_Zarzadzania.this)
                                        .setTitle(Wyświetlanie_Tekstu.czynnościUsuń())
                                        .setMessage(Wyświetlanie_Tekstu.guiGrupaMaZdarzeniaCzyNapewnoUsunąć())
                                        .setPositiveButton(Wyświetlanie_Tekstu.guiTak(), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {

                                                try {
                                                    Łącznik połączenieBD = new Łącznik();
                                                    połączenieBD.połączBD(Ekran_Zarzadzania.this);

                                                    usuwanaGrupa.usuńZbazy(połączenieBD, usuwanaGrupa.getNazwaGrupyZdarzeń_PK());

                                                    połączenieBD.rozłączBD();


                                                    Zmienne_Globalne.listaGrupZdarzeń.remove(pozycjaElementu);

                                                    Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Zarzadzania.this);
                                                    Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Zarzadzania.this);
                                                    Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Zarzadzania.this);
                                                    Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Zarzadzania.this);

                                                    przeładujZdarzenia();

                                                    Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.guiUsunięto());
                                                } catch (Exception e) {
                                                    Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.błądUsuwania());
                                                }

                                            }
                                        })
                                        .setNegativeButton(Wyświetlanie_Tekstu.guiNie(), new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                // Nie usuwaj
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();
                            } else {

                                try {
                                    Łącznik połączenieBD = new Łącznik();
                                    połączenieBD.połączBD(Ekran_Zarzadzania.this);

                                    usuwanaGrupa.usuńZbazy(połączenieBD, usuwanaGrupa.getNazwaGrupyZdarzeń_PK());

                                    połączenieBD.rozłączBD();


                                    Zmienne_Globalne.listaGrupZdarzeń.remove(pozycjaElementu);

                                    Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Zarzadzania.this);
                                    Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Zarzadzania.this);
                                    Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Zarzadzania.this);
                                    Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Zarzadzania.this);

                                    przeładujZdarzenia();

                                    Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.guiUsunięto());
                                } catch (Exception e) {
                                    Metody_Pomocnicze.tostDługi(Ekran_Zarzadzania.this, Wyświetlanie_Tekstu.błądUsuwania());
                                }
                            }

                        }
                    }
                })
                .setNegativeButton(Wyświetlanie_Tekstu.guiNie(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Nie usuwaj
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void edytujElement(int pozycjaElementu) {

        if (czymZarządzamy.equals("jednostki")) {
            final Jednostki edytowanaJednostka = Zmienne_Globalne.listaJednostek.get(pozycjaElementu);

            final Dialog_Wprowadzania_Jednostek dlgJednostki = new Dialog_Wprowadzania_Jednostek(edytowanaJednostka);
            dlgJednostki.działajBezOk(Ekran_Zarzadzania.this);

            dlgJednostki.btnDodaj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlgJednostki.działaniaKończąceEdytujIstniejącą(Ekran_Zarzadzania.this);

                    przeładujJednostki();

                    dlgJednostki.dialog.hide();

                }
            });

        } else if (czymZarządzamy.equals("kategorie")) {
            Kategorie_Zdarzeń edytowanaKategoria = Zmienne_Globalne.listaKategoriiZdarzeń.get(pozycjaElementu);

            final Dialog_Wprowadzania_Kategorii_Zdarzen dlgJednostki = new Dialog_Wprowadzania_Kategorii_Zdarzen(edytowanaKategoria);
            dlgJednostki.działajBezOk(Ekran_Zarzadzania.this);

            dlgJednostki.btnDodaj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlgJednostki.działaniaKończąceEdytujIstniejącąKategorie(Ekran_Zarzadzania.this);

                    przeładujKategorie();

                    dlgJednostki.dialog.hide();

                }
            });

        } else if (czymZarządzamy.equals("zdarzenia")) {
            Grupy_Zdarzeń edytowanaGrupa = Zmienne_Globalne.listaGrupZdarzeń.get(pozycjaElementu);

            final Dialog_Wprowadzania_Grup_Zdarzen dlgZdarzenia = new Dialog_Wprowadzania_Grup_Zdarzen(Ekran_Zarzadzania.this, edytowanaGrupa);
            dlgZdarzenia.działajBezOk(Ekran_Zarzadzania.this);

            dlgZdarzenia.btnDodaj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlgZdarzenia.działaniaKończąceStandardowe(Ekran_Zarzadzania.this);

                    Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Zarzadzania.this);
                    Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Zarzadzania.this);
                    Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Zarzadzania.this);
                    Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Zarzadzania.this);

                    przeładujZdarzenia();

                    dlgZdarzenia.dialog.hide();

                }
            });
        }
    }


    private void przeładujJednostki() {
        List<String> dane = new ArrayList<String>();
        for (Jednostki jednostka : Zmienne_Globalne.listaJednostek) {
            dane.add(jednostka.getNazwa() + " [" + jednostka.getSkrót() + "]");
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Zarzadzania.this, android.R.layout.simple_list_item_1, dane);
        lvwZarządzaneElementy.setAdapter(adapter);
    }

    private void przeładujKategorie() {
        List<String> dane = new ArrayList<String>();
        for (Kategorie_Zdarzeń kategoria : Zmienne_Globalne.listaKategoriiZdarzeń) {
            dane.add(kategoria.getNazwa_PK());
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Zarzadzania.this, android.R.layout.simple_list_item_1, dane);
        lvwZarządzaneElementy.setAdapter(adapter);
    }

    private void przeładujZdarzenia() {
        List<String> dane = new ArrayList<String>();
        for (Grupy_Zdarzeń grupaZdarzeń : Zmienne_Globalne.listaGrupZdarzeń) {
            dane.add(grupaZdarzeń.getNazwaGrupyZdarzeń_PK());
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Zarzadzania.this, android.R.layout.simple_list_item_1, dane);
        lvwZarządzaneElementy.setAdapter(adapter);
    }

}
