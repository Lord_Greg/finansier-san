package com.example.greg.finansier_san;

import com.example.greg.finansier_san.Modele_Danych.Bilanse_Miesiąca;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Ważności;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Scenariusze_Wydarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Ustawienia;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Zdarzenia_w_Scenariuszach;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 29.12.15.
 */
public class Zmienne_Globalne {
    public static int y = 1900;
    public static Date dzisiejszaData = new Date();
    public static Date wybranaData = new Date();
    public static String aktualnyScenariusz = "Rzeczywisty";

    public static List<Grupy_Zdarzeń> listaGrupZdarzeń = new ArrayList<Grupy_Zdarzeń>();
    public static List<Jednostki> listaJednostek = new ArrayList<Jednostki>();
    public static List<Kategorie_Ważności> listaKategoriiWażności = new ArrayList<Kategorie_Ważności>();
    public static List<Kategorie_Zdarzeń> listaKategoriiZdarzeń = new ArrayList<Kategorie_Zdarzeń>();
    public static List<Przychody> listaPrzychodów = new ArrayList<Przychody>();
    public static List<Rozchody> listaRozchodów = new ArrayList<Rozchody>();
    public static List<Scenariusze_Wydarzeń> listaScenariuszyWydarzeń = new ArrayList<Scenariusze_Wydarzeń>();
    public static Ustawienia ustawienia = new Ustawienia();
    public static List<Zdarzenia_w_Scenariuszach> listaZdarzeńWscenariuszach = new ArrayList<Zdarzenia_w_Scenariuszach>();

    public static List<Bilanse_Miesiąca> listaBilansów = new ArrayList<Bilanse_Miesiąca>();
}
