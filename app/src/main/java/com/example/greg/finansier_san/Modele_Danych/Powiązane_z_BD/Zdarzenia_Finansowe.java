package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;

import java.util.Date;

/**
 * Created by Greg on 2015-11-27.
 */
public class Zdarzenia_Finansowe {
    protected String nazwaGrupy_PK_FK, opis;
    protected int numerWystąpienia_PK;
    protected double kwota;
    protected Date data;
    protected boolean czyPowiadomieniaWłączone;

    public Zdarzenia_Finansowe(){}

    public Zdarzenia_Finansowe(String nazwaGrupy, int nrWystąpienia, double kwota, Date data, String opis, String czyPowiadomieniaWłączone){
        nazwaGrupy_PK_FK=nazwaGrupy;
        numerWystąpienia_PK =nrWystąpienia;
        this.kwota=kwota;
        this.data=new Date(data.getYear(), data.getMonth(), data.getDate());
        this.opis=opis;
        if(czyPowiadomieniaWłączone.equals("0")){
            this.czyPowiadomieniaWłączone=false;
        }
        else {
            this.czyPowiadomieniaWłączone=true;
        }
    }

    public Zdarzenia_Finansowe(String nazwaGrupy, int nrWystąpienia, double kwota, Date data, String opis, boolean czyPowiadomieniaWłączone){
        nazwaGrupy_PK_FK=nazwaGrupy;
        numerWystąpienia_PK =nrWystąpienia;
        this.kwota=kwota;
        this.data=new Date(data.getYear(), data.getMonth(), data.getDate());
        this.opis=opis;
        this.czyPowiadomieniaWłączone=czyPowiadomieniaWłączone;
    }


    //region Gettery/Settery
    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }


    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = new Date(data.getYear(), data.getMonth(), data.getDate());
    }


    public double getKwota() {
        return kwota;
    }

    public void setKwota(double kwota) {
        this.kwota = kwota;
    }


    public int getNumerWystąpienia_PK() {
        return numerWystąpienia_PK;
    }

    public void setNumerWystąpienia_PK(int numerWystąpienia_PK) {
        this.numerWystąpienia_PK = numerWystąpienia_PK;
    }


    public String getNazwaGrupy_PK_FK() {
        return nazwaGrupy_PK_FK;
    }

    public void setNazwaGrupy_PK_FK(String nazwaGrupy_PK_FK) {
        this.nazwaGrupy_PK_FK = nazwaGrupy_PK_FK;
    }


    public boolean getCzyPowiadomieniaWłączone() {
        return czyPowiadomieniaWłączone;
    }

    public void setCzyPowiadomieniaWłączone(boolean czyPowiadomieniaWłączone) {
        this.czyPowiadomieniaWłączone = czyPowiadomieniaWłączone;
    }

    //endregion


    public void zapiszNowyDoBazy(Context kontekst, Łącznik połączenieBD){

        String[] wartości = {
                nazwaGrupy_PK_FK,
                String.valueOf(numerWystąpienia_PK),
                String.valueOf(kwota),
                String.valueOf(data.getDate()),
                String.valueOf(data.getMonth()),
                String.valueOf(data.getYear()),
                opis,
                Metody_Pomocnicze.boolNaStringa0lub1(czyPowiadomieniaWłączone)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_ZdarzeniaFinansowe, wartości));

        Metody_Pomocnicze.dodajZdarzenieDoAktualnegoScenariusza(kontekst, połączenieBD, nazwaGrupy_PK_FK, numerWystąpienia_PK);
    }

    public void aktualizujDoBazy(Łącznik połączenieBD, String staraNazwaGrupy){

        if (!staraNazwaGrupy.equals(nazwaGrupy_PK_FK)){
            String[] nazwyPól = {
                    Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk
            };

            String[] wartości = {
                    nazwaGrupy_PK_FK
            };

            String warunek = Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + "='" + staraNazwaGrupy + "'";

            połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_ZdarzeniaFinansowe, nazwyPól, wartości, warunek));
        }

        String[] nazwyPól = {
                Opisy.zdarzeniaFinansowe_nrWystąpieniaPK,
                Opisy.zdarzeniaFinansowe_kwota,
                Opisy.zdarzeniaFinansowe_dataDzień,
                Opisy.zdarzeniaFinansowe_dataMiesiac,
                Opisy.zdarzeniaFinansowe_dataRok,
                Opisy.zdarzeniaFinansowe_opis,
                Opisy.zdarzeniaFinansowe_czyPowiadomieniaWłączone
        };

        String[] wartości = {
                String.valueOf(numerWystąpienia_PK),
                String.valueOf(kwota),
                String.valueOf(data.getDate()),
                String.valueOf(data.getMonth()),
                String.valueOf(data.getYear()),
                opis,
                Metody_Pomocnicze.boolNaStringa0lub1(czyPowiadomieniaWłączone)
        };

        String warunek = Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + "='" + nazwaGrupy_PK_FK + "' AND " + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + "='" + String.valueOf(numerWystąpienia_PK) + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_ZdarzeniaFinansowe, nazwyPól, wartości, warunek));

        Zdarzenia_w_Scenariuszach.aktualizujNazwęZdarzeniaDoBazy(połączenieBD, staraNazwaGrupy, nazwaGrupy_PK_FK);
    }

    protected void aktualizujDatęDoBazy(Łącznik połączenieBD){

        String[] nazwyPól = {
                Opisy.zdarzeniaFinansowe_dataDzień,
                Opisy.zdarzeniaFinansowe_dataMiesiac,
                Opisy.zdarzeniaFinansowe_dataRok
        };

        String[] wartości = {
                String.valueOf(data.getDate()),
                String.valueOf(data.getMonth()),
                String.valueOf(data.getYear())
        };

        String warunek = Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + "='" + nazwaGrupy_PK_FK + "' AND " + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + "=" + String.valueOf(numerWystąpienia_PK);

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_ZdarzeniaFinansowe, nazwyPól, wartości, warunek));
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń, int numerWystąpienia, String nazwaScenariusza){
        String[] nazwyPól = {
                Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk,
                Opisy.zdarzeniaFinansowe_nrWystąpieniaPK
        };

        String[] wartości = {
                nazwaGrupyZdarzeń,
                String.valueOf(numerWystąpienia)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_ZdarzeniaFinansowe, nazwyPól, wartości));

        Zdarzenia_w_Scenariuszach.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń, numerWystąpienia, nazwaScenariusza);
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń){
        String[] nazwyPól = {
                Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_ZdarzeniaFinansowe, nazwyPól, wartości));
    }


}
