package com.example.greg.finansier_san.Teksty_Interfejsu;

/**
 * Created by Greg on 2015-11-28.
 */
public class Język_Polski {

    //region Miesiące
    public static String miesiącStyczeń = "Styczeń";
    public static String miesiącLuty = "Luty";
    public static String miesiącMarzec = "Marzec";
    public static String miesiącKwiecień = "Kwiecień";
    public static String miesiącMaj = "Maj";
    public static String miesiącCzerwiec = "Czerwiec";
    public static String miesiącLipiec = "Lipiec";
    public static String miesiącSierpień = "Sierpień";
    public static String miesiącWrzesień = "Wrzesień";
    public static String miesiącPaździernik = "Październik";
    public static String miesiącListopad = "Listopad";
    public static String miesiącGrudzień = "Grudzień";
    //endregion


    //region Dni Tygodnia
    public static String dzień_skrót_poniedziałek = "Pn";
    public static String dzień_skrót_wtorek = "Wt";
    public static String dzień_skrót_środa = "Śr";
    public static String dzień_skrót_czwartek = "Cz";
    public static String dzień_skrót_piątek = "Pt";
    public static String dzień_skrót_sobota = "Sb";
    public static String dzień_skrót_niedziela = "Nd";
    //endregion


    //region Błędy
    public static String błąd_data = "Błędna Data";
    public static String błąd_nazwaJużIstnieje = "Taka nazwa już istnieje";
    public static String błąd_połączeniaZbazą = "Błąd połączenia z bazą danych";
    public static String błąd_wprowadzonychDanych = "Wprowadzone dane są błędne";
    public static String błąd_jednostkaNieDodana = "Wystąpił błąd! Jednostka nie została dodana";
    public static String błąd_podajNazwęJednostki = "Podaj nazwę jednostki";
    public static String błąd_podajNazwęKategoriiZdarzeń = "Podaj nazwę kategorii zdarzeń";
    public static String błąd_kategoriaJużIstnieje = "Taka kategoria już istnieje";
    public static String błąd_kategoriaNieZostałaDodana = "Wystąpił błąd! Kategoria nie została dodana";
    public static String błąd_usuwania = "Błąd usuwania";
    public static String błąd_zapisu = "Błąd zapisu";
    public static String błąd_podajNazwęZdarzenia = "Podaj nazwę zdarzenia";
    public static String błąd_zdarzenieJużIstnieje = "Takie zdarzenie już istnieje";
    public static String błąd_odczytuZbd = "Błąd odczytu bazy danych";
    public static String błąd_podajKwotę = "Podaj kwotę";
    public static String błąd_podajPoczątkoweWystąpienie = "Podaj początkowe wystąpienie";
    public static String błąd_podajIlośćWystąpień = "Podaj ilość wystąpień";
    public static String błąd_wystąpienieOnumerzeJużIstnieje1 = "Wystąpienie o numerze ";
    public static String błąd_wystąpienieOnumerzeJużIstnieje2 = " już istnieje";
    //endregion


    //region Okresy Powtarzalności
    public static String powtarzalności_dni = "dni";
    public static String powtarzalności_tygodnie = "tygodnie";
    public static String powtarzalności_miesiące = "miesiące";
    public static String powtarzalności_lat = "lat";
    //endregion


    //region Teksty Zakładek
    public static String zakładka_kalendarz = "Kalendarz";
    public static String zakładka_lista = "Lista";
    //endregion


    //region Interfejs Użytkownika
    public static String gui_rozchód = "Wydatek";
    public static String gui_przychód = "Zarobek";
    public static String gui_tak = "Tak";
    public static String gui_nie = "Nie";
    public static String gui_czyNapewnoUsunąć = "Czy napewno chcesz usunąć ten element?";
    public static String gui_usuńZdarzenie = "Usuń Zdarzenie";
    public static String gui_zdarzenieZostałoUsunięte = "Zdarzenie zostało usunięte";
    public static String gui_jednostkaZostałaDodana = "Jednostka została dodana";
    public static String gui_kategoriaZostałaDodana = "Kategoria została dodana";
    public static String gui_usunięto = "Usunięto";
    public static String gui_zapisano = "Zapisano";
    public static String gui_grupaMaZdarzeniaCzyNapewnoUsunąć = "Wybrane zdarzenie posiada jedno lub więcej wystąpień. Czy jesteś pewien że chcesz je usunąć?";
    public static String gui_dodajNowe = "Dodaj nowe:";
    public static String gui_pojedynczeZdarzenie = "Pojedyncze Zdarzenie";
    public static String gui_seriaZdarzeń = "Seria Zdarzeń";
    public static String gui_symbolRozchodu = "W: ";
    public static String gui_symbolPrzychodu = "Z: ";
    public static String gui_powiadomienieRozchodów = "dostępne do kupienia";
    public static String gui_powiadomieniePrzycodów = "już dzisiaj";
    public static String gui_podajDatę = "Podaj datę ";
    public static String gui_pierwszego = "pierwszego ";
    public static String gui_zdarzenia = "zdarzenia:";
    public static String gui_zdarzeniaDodanoPomyślnie = "Zdarzenia dodano pomyślnie";
    public static String gui_edytujZdarzenie = "Edytuj Zdarzenie";
    public static String gui_numerEdytowanegoZdarzenia = "Numer edytowanego zdarzenia:";
    public static String gui_noweZdarzenie = "Nowe Zdarzenie";
    public static String gui_podajNumerZdarzenia = "Podaj numer zdarzenia:";
    public static String gui_zdarzenieDodanoPomyślnie = "Zdarzenie dodano pomyślnie";
    public static String gui_czyChceszPrzesunąćDaty = "Czy chcesz też przesunąć daty kolejnych wystąpień?";
    public static String gui_wykonanoDlaZdarzeń1 = "Wykonano dla ";
    public static String gui_wykonanoDlaZdarzeń2 = " zdarzeń";

    public static String gui_nagłówek_dzień = "Dzień";
    public static String gui_nagłówek_miesiąc = "Miesiąc";
    public static String gui_nagłówek_rok = "Rok";
    public static String gui_nagłówek_podajNazwęZdarzenia = "Podaj nazwę zdarzenia:";
    public static String gui_nagłówek_nazwa = "Nazwa";
    public static String gui_nagłówek_wybierzKategorię = "Wybierz kategorię:";
    public static String gui_nagłówek_wybierzJednostkę = "Wybierz jednostkę:";
    public static String gui_nagłówek_ukryjNrIjednostkę = "Ukryj numer wystąpienia i jednostkę";
    public static String gui_nagłówek_podajNazwęJednostki = "Podaj nazwę jednostki:";
    public static String gui_nagłówek_podajSkrótJednostki = "Podaj skrót jednostki:";
    public static String gui_nagłówek_skrót = "Skrót";
    public static String gui_nagłówek_podajNazwęKategorii = "Podaj nazwę kategorii:";
    public static String gui_nagłówek_podajOpis = "Podaj opis:";
    public static String gui_nagłówek_opis = "Opis";
    public static String gui_nagłówek_listaZakupów = "Lista zakupów:";
    public static String gui_nagłówek_nadchodzące = "Nadchodzące:";
    public static String gui_nagłówek_opcje = "Opcje";
    public static String gui_nagłówek_ukryjPrzychody = "Ukryj przychody";
    public static String gui_nagłówek_wyłączPowiadomienia = "Wyłącz powiadomienia";
    public static String gui_nagłówek_zarządzajJednostkami = "Zarządzaj Jednostkami";
    public static String gui_nagłówek_zarządzajKategoriami = "Zarządzaj Kategoriami";
    public static String gui_nagłówek_zarządzajZdarzeniami = "Zarządzaj Zdarzeniami";
    public static String gui_nagłówek_ważność_ = "Ważność: ";
    public static String gui_nagłówek_kategoria = "Kategoria: ";
    public static String gui_nagłówek_data_ = "Data: ";
    public static String gui_nagłówek_kwota_ = "Kwota: ";
    public static String gui_nagłówek_posiadane = "Posiadane";
    public static String gui_nagłówek_ukryty = "Ukryty";
    public static String gui_nagłówek_opis_ = "Opis:";
    public static String gui_nagłówek_nowaSeriaZdarzeń = "Nowa Seria Zdarzeń";
    public static String gui_nagłówek_wybierzNazwęZdarzenia_ = "Wybierz nazwę zdarzenia:";
    public static String gui_nagłówek_podajLiczbęWystąpień_ = "Podaj liczbę wystąpień:";
    public static String gui_nagłówek_podajKwotę_ = "Podaj kwotę:";
    public static String gui_nagłówek_podajDatęPierwszegoZdarzenia = "Podaj datę pierwszego zdarzenia:";
    public static String gui_nagłówek_powtarzajCo_ = "Powtarzaj co:";
    public static String gui_nagłówek_podajOpis_ = "Podaj opis:";
    public static String gui_nagłówek_włączPowiadamienia = "Włącz powiadomienia";
    public static String gui_nagłówek_niePokazuj = "Nie pokazuj";
    public static String gui_nagłówek_wybierzKategorięWażności_ = "Wybierz kategorie ważności:";
    public static String gui_nagłówek_kwota = "Kwota";
    public static String gui_nagłówek_liczbaWystąpień = "Liczba Wstąpień";
    public static String gui_nagłówek_zacznijOd = "Zacznij od";
    public static String gui_nagłówek_ile = "Ile";
    public static String gui_nagłówek_noweZdarzenie = "Nowe Zdarzenie";
    public static String gui_nagłówek_podajNumerWystąpienia_ = "Podaj numer wystąpienia:";
    public static String gui_nagłówek_numerWystąpienia = "Numer wystąpienia";
    public static String gui_nagłówek_podajDatęZdarzenia_ = "Podaj datę zdarzenia:";
    public static String gui_nagłówek_zarządzanieJednostkami = "Zarządzanie Jednostkami";
    public static String gui_nagłówek_zarządzanieKategoriami = "Zarządzanie Kategoriami";
    public static String gui_nagłówek_zarządzanieZdarzeniami = "Zarządzanie Zdarzeniami";
    public static String gui_nagłówek_dodajZdarzenie = "Dodaj Zdarzenie";
    public static String gui_nagłówek_skrótWaluty_ = "Skrót Waluty:";
    public static String gui_nagłówek_zdarzenia = "zdarzenia";


    public static String gui_nagłówek_ok = "Ok";
    public static String gui_nagłówek_anuluj = "Anuluj";
    //endregion


    //region Tytuły Dialogów
    public static String tytuł_nazwaDialoguJednostek = "Nowa Jednostka";
    public static String tytuł_nazwaDialoguKategoriiZdarzeń = "Nowa Kategoria Zdarzeń";
    public static String tytuł_nazwaDialoguGrupZdarzeń = "Nowe Zdarzenie";
    public static String tytuł_nazwaDialoguPrzesuńZdarzenia = "Przesuń Zdarzenia";
    public static String tytuł_nazwaDialoguZmianaDaty = "Wprowadź Datę";
    public static String tytuł_nazwaAlertDialoguZmianaDaty = "Zmieniono Datę";
    //endregion


    //region Czynności
    public static String czynności_usuń = "Usuń";
    public static String czynności_edytuj = "Edytuj";
    public static String czynności_zapisz = "Zapisz";
    public static String czynności_dodaj = "Dodaj";
    //endregion


    public static String kolor_domyślny = "biały";







    //region Wartości Początkowe w BD
    public static String
            BD_jednostki_nazwa_nic = "Nic",
            BD_jednostki_nazwa_tom = "Tom",
            BD_jednostki_nazwa_numer = "Numer",
            BD_jednostki_nazwa_część = "Część",
            //BD_jednostki_nazwa_tom = "Tom",

            BD_jednostki_skrót_nic = "",
            BD_jednostki_skrót_kratka = "#",
            BD_jednostki_skrót_nr = "nr ",
            BD_jednostki_skrót_cz = "cz. ",
            BD_jednostki_skrót_tom = "tom ",


            BD_kategorieZdarzeń_nazwa_nieprzypisane = "Nieprzypisane",
            BD_kategorieZdarzeń_nazwa_manga = "Manga",
            BD_kategorieZdarzeń_nazwa_prezenty = "Prezenty",
            BD_kategorieZdarzeń_nazwa_zarobki = "Zarobki",
            BD_kategorieZdarzeń_nazwa_kieszonkowe = "Kieszonkowe",
            BD_kategorieZdarzeń_nazwa_rachunki = "Rachunki",
            BD_kategorieZdarzeń_nazwa_prasa = "Prasa",


            BD_kategorieWażności_opis_S = "Specjalne - nagłe, nieplanowane wydatki, które nie podlegają dyskusji (np. zbita szyba czy prezent na nieplanowaną okazję)",
            BD_kategorieWażności_opis_A = "Absolutne - najważniejsze i bezdyskusyjne wydatki (np. rachunek za prąd czy jedzenie)",
            BD_kategorieWażności_opis_B = "Bardzo Ważne - wydatki które wydają się niezbędne do życia, ale w najgorszym wypadku można się bez nich obejść, lub wydatki których dostępność jest limitowana czasowo(np. nowe ubranie czy czasopismo)",
            BD_kategorieWażności_opis_C = "Częściowo Ważne - wydatki bez których można się spokojnie obejść ale nie zawsze to robimy (np. wyjście ze znajomymi do restauracji)",
            BD_kategorieWażności_opis_D = "Do Przemyślenia - wydatki które kuszą ale nie są niezbędne do życia (np. gadżety i ozdoby)",
            BD_kategorieWażności_opis_E = "Ewentualnie - wydatki całkowicie opcjonalne",


            BD_scenariusze_nazwa_rzeczywisty = "Rzeczywisty";
    //endregion

}
