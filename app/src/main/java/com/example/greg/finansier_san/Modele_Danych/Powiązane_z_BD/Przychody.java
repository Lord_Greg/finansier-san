package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.Date;

/**
 * Created by Greg on 2015-11-29.
 */
public class Przychody extends Zdarzenia_Finansowe {
    private boolean czyWyświetlany;

    public Przychody(String nazwaGrupy, int nrWystąpienia, double kwota, Date data, String opis, String czyPowiadomieniaWłączone, String czyWyświetlany){
        super(nazwaGrupy,nrWystąpienia,kwota,data,opis,czyPowiadomieniaWłączone);
        if(czyWyświetlany.equals("0")){
            this.czyWyświetlany=false;
        }
        else {
            this.czyWyświetlany=true;
        }
    }

    public Przychody(String nazwaGrupy, int nrWystąpienia, double kwota, Date data, String opis, boolean czyPowiadomieniaWłączone, boolean czyWyświetlany){
        super(nazwaGrupy,nrWystąpienia,kwota,data,opis,czyPowiadomieniaWłączone);
        this.czyWyświetlany=czyWyświetlany;
    }

    public Przychody() {}


    //region Gettery/Settery
    public boolean getCzyWyświetlany(){
        return czyWyświetlany;
    }

    public void setCzyWyświetlany(boolean czyWyświetlany) {
        this.czyWyświetlany = czyWyświetlany;
    }
    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        String warunek = Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk+"="+Opisy.przychody_nazwaZdarzeniaPKfk+" AND "+Opisy.zdarzeniaFinansowe_nrWystąpieniaPK+"="+Opisy.przychody_nrWystąpieniaPKfk;
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_ZdarzeniaFinansowe + ", " + Opisy.tab_Przychody, warunek), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaPrzychodów.add(new Przychody(kursor.getString(0), kursor.getInt(1), kursor.getDouble(2), new Date(kursor.getInt(5), kursor.getInt(4), kursor.getInt(3)), kursor.getString(6), kursor.getString(7), kursor.getString(10)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        //zapisz zdarzenie finansowe
        super.zapiszNowyDoBazy(kontekst, połączenieBD);

        String[] wartości = {
                nazwaGrupy_PK_FK,
                String.valueOf(numerWystąpienia_PK),
                Metody_Pomocnicze.boolNaStringa0lub1(czyWyświetlany)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_Przychody, wartości));

        połączenieBD.rozłączBD();
    }

    public void aktualizujDoBazy(Łącznik połączenieBD, String staraNazwaGrupy){

        super.aktualizujDoBazy(połączenieBD, staraNazwaGrupy);

        if (!staraNazwaGrupy.equals(nazwaGrupy_PK_FK)){
            String[] nazwyPól = {
                    Opisy.przychody_nazwaZdarzeniaPKfk
            };

            String[] wartości = {
                    nazwaGrupy_PK_FK
            };

            String warunek = Opisy.przychody_nazwaZdarzeniaPKfk + "='" + staraNazwaGrupy + "'";

            połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Przychody, nazwyPól, wartości, warunek));
        }

        String[] nazwyPól = {
                Opisy.przychody_nrWystąpieniaPKfk,
                Opisy.przychody_czyWyświetlany
        };

        String[] wartości = {
                String.valueOf(numerWystąpienia_PK),
                Metody_Pomocnicze.boolNaStringa0lub1(czyWyświetlany)
        };

        String warunek = Opisy.przychody_nazwaZdarzeniaPKfk + "='" + nazwaGrupy_PK_FK + "' AND " + Opisy.przychody_nrWystąpieniaPKfk + "='" + String.valueOf(numerWystąpienia_PK) + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Przychody, nazwyPól, wartości, warunek));
    }

    public void aktualizujDatęDoBazy(Łącznik połączenieBD) {
        super.aktualizujDatęDoBazy(połączenieBD);
    }

    public void aktualizujUkrycieDoBD(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        String[] nazwyPól = {
                Opisy.przychody_czyWyświetlany
        };

        String[] wartości = {
                Metody_Pomocnicze.boolNaStringa0lub1(czyWyświetlany)
        };

        String warunek = Opisy.przychody_nazwaZdarzeniaPKfk + "='" + nazwaGrupy_PK_FK + "' AND " + Opisy.przychody_nrWystąpieniaPKfk + "=" + String.valueOf(numerWystąpienia_PK);

        połączenieBD.połączBD(kontekst);
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Przychody, nazwyPól, wartości, warunek));
        połączenieBD.rozłączBD();

    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń, int numerWystąpienia, String nazwaScenariusza){
        String[] nazwyPól = {
                Opisy.przychody_nazwaZdarzeniaPKfk,
                Opisy.przychody_nrWystąpieniaPKfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń,
                String.valueOf(numerWystąpienia)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_Przychody, nazwyPól, wartości));

        Zdarzenia_Finansowe.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń, numerWystąpienia, nazwaScenariusza);
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń){
        String[] nazwyPól = {
                Opisy.przychody_nazwaZdarzeniaPKfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_Przychody, nazwyPól, wartości));

        //usunięcie Zdarzenia Finansowego propagowane w Grupach Zdarzeń
    }

}
