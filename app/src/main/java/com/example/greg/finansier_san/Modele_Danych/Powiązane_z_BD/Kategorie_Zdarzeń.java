package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.List;

/**
 * Created by Greg on 2015-11-26.
 */
public class Kategorie_Zdarzeń {
    private String nazwa_PK, opis, kolor;

    public Kategorie_Zdarzeń(String nazwa, String opis, String kolor){
        nazwa_PK=nazwa;
        this.opis=opis;
        this.kolor=kolor;
    }

    public Kategorie_Zdarzeń() {}


    public void zapisz(Context kontekst)
    {
        if(czyWolnoDodać(Zmienne_Globalne.listaKategoriiZdarzeń))
        {
            try {
                zapiszwBD(kontekst);
                Zmienne_Globalne.listaKategoriiZdarzeń.add(this);
            }
            catch (Exception e){}
        }
        else
        {
            //nie dodawaj i zwróć błąd
            Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądNazwaJużIstnieje());
        }
    }

    public void zapiszwBD(Context kontekst)
    {
        Łącznik połączenieZbazą = new Łącznik();
        połączenieZbazą.połączBD(kontekst);

        String[] wartości = new String[]
                {
                        nazwa_PK,
                        opis,
                        kolor
                };

        połączenieZbazą.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_KategorieZdarzeń, wartości));

        połączenieZbazą.rozłączBD();
    }

    public boolean czyWolnoDodać(List<Kategorie_Zdarzeń> listaElementów)
    {
        for (Kategorie_Zdarzeń element : listaElementów) {
            if(element.getNazwa_PK().equals(getNazwa_PK()))
            {
                return false;
            }
        }
        return true;
    }


    //region Gettery/Settery
    public String getNazwa_PK() {
        return nazwa_PK;
    }

    public void setNazwa_PK(String nazwa_PK) {
        this.nazwa_PK = nazwa_PK;
    }


    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }


    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_KategorieZdarzeń), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaKategoriiZdarzeń.add(new Kategorie_Zdarzeń(kursor.getString(0), kursor.getString(1), kursor.getString(2)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] wartości = {
                nazwa_PK,
                opis,
                kolor
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_KategorieZdarzeń, wartości));

        połączenieBD.rozłączBD();
    }

    public void usuńZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);


        String[] nazwyPól = {
                Opisy.grupyZdarzen_nazwaKategoriiFK
        };
        String[] wartości = {
                Wyświetlanie_Tekstu.BDkategorieZdarzeń_nazwa_nieprzypisane() //"Nieprzypisane" - domyślna
        };

        String warunek = Opisy.grupyZdarzen_nazwaKategoriiFK + "='" + nazwa_PK + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_GrupyZdarzeń, nazwyPól, wartości, warunek));


        nazwyPól = new String[]{
                Opisy.kategorieZdarzeń_nazwaPK
        };
        wartości = new String[]{
                nazwa_PK
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_KategorieZdarzeń, nazwyPól, wartości));

        połączenieBD.rozłączBD();
    }

    public void aktualizujDoBazy(Context kontekst, String staraNazwaKategorii){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] nazwyPól = {
                Opisy.kategorieZdarzeń_nazwaPK,
                Opisy.kategorieZdarzeń_opis,
                Opisy.kategorieZdarzeń_kolor
        };

        String[] wartości = {
                nazwa_PK,
                opis,
                kolor
        };

        String warunek = Opisy.kategorieZdarzeń_nazwaPK + "='" + staraNazwaKategorii + "'";
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_KategorieZdarzeń, nazwyPól, wartości, warunek));

        if(!nazwa_PK.equals(staraNazwaKategorii))
        {
            nazwyPól = new String[]{
                    Opisy.grupyZdarzen_nazwaKategoriiFK
            };

            wartości = new String[]{
                    nazwa_PK
            };

            warunek = Opisy.grupyZdarzen_nazwaKategoriiFK + "='" + staraNazwaKategorii + "'";
            połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_GrupyZdarzeń, nazwyPól, wartości, warunek));
        }

        połączenieBD.rozłączBD();
    }
}
