package com.example.greg.finansier_san.Ekrany;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Ważności;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-12-16.
 */
public class Ekran_Przegladania_Zdarzenia extends AppCompatActivity {

    boolean czyZmienionoStatus;
    boolean czyZmienionoUkrycie ;


    TextView txvPrzychodCzyRozchod;
    TextView txvNazwaZdarzenia;
    TextView txvKategoria;
    TextView txvWażność;
    TextView txvData;
    TextView txvKwota;
    CheckBox chbCzyPosiadane;
    CheckBox chbCzyUkryty;
    TextView txvOpis;
    Button btnEdytuj;
    Button btnUsuń;

    boolean czyToRozchód = true;
    Grupy_Zdarzeń rozpatrywanaGrupa = new Grupy_Zdarzeń();
    Rozchody rozpatrywanyRozchód = new Rozchody();
    Kategorie_Ważności rozpatrywanaKategoriaważności = new Kategorie_Ważności();
    Jednostki rozpatrywanaJednostka = new Jednostki();
    Przychody rozpatrywanyPrzychód = new Przychody();
    boolean czyPierwszyRaz;
    String nazwaGrupy;
    int nrWystąpienia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_przegladania_zdarzenia);

        czyPierwszyRaz=true;
        czyZmienionoStatus = false;
        czyZmienionoUkrycie = false;

        //region Inicjalizacja kontrolek
        txvPrzychodCzyRozchod = (TextView)findViewById(R.id.txvTyp);
        txvNazwaZdarzenia = (TextView)findViewById(R.id.txvNazwa);
        txvKategoria = (TextView)findViewById(R.id.txvKategoria);
        txvWażność = (TextView)findViewById(R.id.txvWaznosc);
        txvData = (TextView)findViewById(R.id.txvData);
        txvKwota = (TextView)findViewById(R.id.txvKwota);
        chbCzyPosiadane = (CheckBox)findViewById(R.id.chbCzyPosiadane);
        chbCzyUkryty = (CheckBox)findViewById(R.id.chbCzyUkryty);
        txvOpis = (TextView)findViewById(R.id.txvOpis);
        btnEdytuj = (Button)findViewById(R.id.btnEdytuj);
        btnUsuń = (Button)findViewById(R.id.btnUsun);

        TextView txvNagłówekWażność = (TextView)findViewById(R.id.txvNaglowekWaznosc);
        TextView txvNagłówekKategoria = (TextView)findViewById(R.id.txvNaglowekKategoria);
        TextView txvNagłówekData = (TextView)findViewById(R.id.txvNaglowekData);
        TextView txvNagłówekKwota = (TextView)findViewById(R.id.txvNaglowekKwota);
        TextView txvNagłówekOpis = (TextView)findViewById(R.id.txvNaglowekOpis);

        //region Ustawianie tekstu kontrolek

        txvNagłówekWażność.setText(Wyświetlanie_Tekstu.gui_Nagłówek_ważność());
        txvNagłówekKategoria.setText(Wyświetlanie_Tekstu.gui_Nagłówek_kategoria());
        txvNagłówekData.setText(Wyświetlanie_Tekstu.gui_Nagłówek_data());
        txvNagłówekKwota.setText(Wyświetlanie_Tekstu.gui_Nagłówek_kwota_());
        chbCzyPosiadane.setText(Wyświetlanie_Tekstu.gui_Nagłówek_posiadane());
        chbCzyUkryty.setText(Wyświetlanie_Tekstu.gui_Nagłówek_ukryty());
        txvNagłówekOpis.setText(Wyświetlanie_Tekstu.gui_Nagłówek_opis_());
        btnEdytuj.setText(Wyświetlanie_Tekstu.czynnościEdytuj());
        btnUsuń.setText(Wyświetlanie_Tekstu.czynnościUsuń());
        //endregion



        Bundle przekazaneWartości = getIntent().getExtras(); // oczekiwane wartości: boolean "czyToRozchód", String "nazwa", int "numer"
        if(przekazaneWartości != null){ // zawsze będzie prawdziwe
            String przychódLubRozchód;

            nazwaGrupy = przekazaneWartości.getString("nazwa");
            nrWystąpienia = przekazaneWartości.getInt("numer");

            czyToRozchód = przekazaneWartości.getBoolean("czyToRozchód");
            if(czyToRozchód){
                txvNagłówekWażność.setVisibility(View.VISIBLE);
                txvWażność.setVisibility(View.VISIBLE);
                chbCzyPosiadane.setVisibility(View.VISIBLE);

                chbCzyUkryty.setVisibility(View.INVISIBLE);

                przychódLubRozchód = Wyświetlanie_Tekstu.guiRozchód();
            }
            else{
                txvNagłówekWażność.setVisibility(View.INVISIBLE);
                txvWażność.setVisibility(View.INVISIBLE);
                chbCzyPosiadane.setVisibility(View.INVISIBLE);

                chbCzyUkryty.setVisibility(View.VISIBLE);

                przychódLubRozchód = Wyświetlanie_Tekstu.guiPrzychód();
            }
            txvPrzychodCzyRozchod.setText(przychódLubRozchód);



            przeładujWyświetlaneInformacje();
        }
        //endregion

        btnEdytuj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent i = new Intent(Ekran_Przegladania_Zdarzenia.this, Ekran_Wprowadzania_Serii_Zdarzen.class);
                Intent i = new Intent(Ekran_Przegladania_Zdarzenia.this, Ekran_Wprowadzania_Zdarzen.class);
                i.putExtra("nazwaGrupy", rozpatrywanaGrupa.getNazwaGrupyZdarzeń_PK());
                if(czyToRozchód){
                    i.putExtra("czyToRozchód", true);
                    i.putExtra("numer", rozpatrywanyRozchód.getNumerWystąpienia_PK());
                }
                else{
                    i.putExtra("czyToRozchód", false);
                    i.putExtra("numer", rozpatrywanyPrzychód.getNumerWystąpienia_PK());
                }
                startActivity(i);
            }
        });

        btnUsuń.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(Ekran_Przegladania_Zdarzenia.this)
                        .setTitle(Wyświetlanie_Tekstu.guiUsuńZdarzenie())
                        .setMessage(Wyświetlanie_Tekstu.guiCzyNapewnoUsunąć())
                        .setPositiveButton(Wyświetlanie_Tekstu.guiTak(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Usuń
                                try {
                                    Łącznik połączenieBD = new Łącznik();
                                    połączenieBD.połączBD(Ekran_Przegladania_Zdarzenia.this);
                                    if (czyToRozchód) {
                                        Rozchody.usuńZbazy(połączenieBD, rozpatrywanyRozchód.getNazwaGrupy_PK_FK(), rozpatrywanyRozchód.getNumerWystąpienia_PK(), Zmienne_Globalne.aktualnyScenariusz);
                                        Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Przegladania_Zdarzenia.this);
                                    }
                                    else {
                                        Przychody.usuńZbazy(połączenieBD, rozpatrywanyPrzychód.getNazwaGrupy_PK_FK(), rozpatrywanyPrzychód.getNumerWystąpienia_PK(), Zmienne_Globalne.aktualnyScenariusz);
                                        Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Przegladania_Zdarzenia.this);
                                    }
                                    Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Przegladania_Zdarzenia.this);

                                    połączenieBD.rozłączBD();
                                    Metody_Pomocnicze.tostDługi(Ekran_Przegladania_Zdarzenia.this, Wyświetlanie_Tekstu.guiZdarzenieZostałoUsunięte());

                                    finish();
                                }
                                catch (Exception e){Metody_Pomocnicze.tostDługi(Ekran_Przegladania_Zdarzenia.this, Wyświetlanie_Tekstu.błądUsuwania());}
                            }
                        })
                        .setNegativeButton(Wyświetlanie_Tekstu.guiNie(), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // Nie usuwaj
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        //endregion


        chbCzyPosiadane.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rozpatrywanyRozchód.setCzyPosiadane(chbCzyPosiadane.isChecked());
                czyZmienionoStatus = true;
            }
        });

        chbCzyUkryty.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                rozpatrywanyPrzychód.setCzyWyświetlany(!chbCzyUkryty.isChecked());
                czyZmienionoUkrycie = true;
            }
        });
    }


    protected void onStart(){
        super.onStart();
        if(!czyPierwszyRaz){
            przeładujWyświetlaneInformacje();
        }
        else{
            czyPierwszyRaz=false;
        }
    }


    protected void onPause(){
        super.onPause();

        if(czyZmienionoStatus){
            rozpatrywanyRozchód.aktualizujStatusPosiadaniaDoBD(Ekran_Przegladania_Zdarzenia.this);
            czyZmienionoStatus=false;
        }
        if(czyZmienionoUkrycie){
            rozpatrywanyPrzychód.aktualizujUkrycieDoBD(Ekran_Przegladania_Zdarzenia.this);
            czyZmienionoUkrycie=false;
        }
    }


    private void przeładujWyświetlaneInformacje(){
        String wyświetlanaNazwa, opis, data, kwota;

        rozpatrywanaGrupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(nazwaGrupy);

        if(czyToRozchód) {
            rozpatrywanyRozchód = Metody_Pomocnicze.znajdźRozchód(nazwaGrupy, nrWystąpienia);
            rozpatrywanaKategoriaważności = Metody_Pomocnicze.znajdźKategorieWażności(rozpatrywanyRozchód.getPoziomWażności_FK());

            wyświetlanaNazwa = rozpatrywanyRozchód.getNazwaGrupy_PK_FK();
            if (rozpatrywanaGrupa.getCzyJawnaNumeracja()) {
                rozpatrywanaJednostka = Metody_Pomocnicze.znajdźJednostkę(rozpatrywanaGrupa.getIdJednostki_FK());
                wyświetlanaNazwa += " " + rozpatrywanaJednostka.getSkrót() + String.valueOf(rozpatrywanyRozchód.getNumerWystąpienia_PK());
            }

            data = String.valueOf(rozpatrywanyRozchód.getData().getDate()) + " " + Metody_Pomocnicze.zwróćNazwęMiesiąca(rozpatrywanyRozchód.getData().getMonth()) + " " + String.valueOf(rozpatrywanyRozchód.getData().getYear() + Zmienne_Globalne.y);
            kwota = String.format("%.2f", rozpatrywanyRozchód.getKwota());
            //kwota = String.valueOf(rozpatrywanyRozchód.getKwota());
            txvWażność.setText(rozpatrywanaKategoriaważności.getSymbol());
            chbCzyPosiadane.setChecked(rozpatrywanyRozchód.getCzyPosiadane());
            opis = rozpatrywanyRozchód.getOpis();
        }
        else{
            rozpatrywanyPrzychód = Metody_Pomocnicze.znajdźPrzychód(nazwaGrupy, nrWystąpienia);

            wyświetlanaNazwa = rozpatrywanyPrzychód.getNazwaGrupy_PK_FK();
            if(rozpatrywanaGrupa.getCzyJawnaNumeracja()){
                rozpatrywanaJednostka = Metody_Pomocnicze.znajdźJednostkę(rozpatrywanaGrupa.getIdJednostki_FK());
                wyświetlanaNazwa += " " + rozpatrywanaJednostka.getSkrót() + String.valueOf(rozpatrywanyPrzychód.getNumerWystąpienia_PK());
            }
            data = String.valueOf(rozpatrywanyPrzychód.getData().getDate()) + " " + Metody_Pomocnicze.zwróćNazwęMiesiąca(rozpatrywanyPrzychód.getData().getMonth()) + " " + String.valueOf(rozpatrywanyPrzychód.getData().getYear() + Zmienne_Globalne.y);
            kwota = String.format("%.2f", rozpatrywanyPrzychód.getKwota());
            //kwota = String.valueOf(rozpatrywanyPrzychód.getKwota());
            chbCzyUkryty.setChecked(!rozpatrywanyPrzychód.getCzyWyświetlany());
            opis = rozpatrywanyPrzychód.getOpis();
        }
        txvNazwaZdarzenia.setText(wyświetlanaNazwa);
        txvKategoria.setText(rozpatrywanaGrupa.getNazwaKategoriiZdarzenia_FK());
        txvData.setText(data);
        txvKwota.setText(kwota + Zmienne_Globalne.ustawienia.getWaluta().getSymbolWaluty());
        txvOpis.setText(opis);
    }

}
