package com.example.greg.finansier_san.Ekrany;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ListView;

import com.example.greg.finansier_san.Dialogi.Dialog_Wyboru_Daty;
import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Bilanse_Miesiąca;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 13.01.16.
 */
public class Ekran_Glowny_Kalendarz  extends Activity {

    ListView lvwMiesiącMinus, lvwMiesiącWybierz, lvwMiesiącBilans, lvwMiesiącPlus;
    ListView lvwZdarzeniaDnia;
    GridView gvwNazwyDniTygodnia;
    GridView gvwBilans;
    GridView gvwKalendarz;
    int ilośćTygodni;
    Bilanse_Miesiąca bilansObecnegoMiesiąca;
    List<Przychody> listaPrzychodówObecnegoMiesiąca, listaPrzychodówDnia;
    List<Rozchody> listaRozchodówObecnegoMiesiąca, listaRozchodówDnia;
    FrameLayout[] fltPodświetlenie;

    /*
        Date - dzień miesiąca
        Dni tygodnia: 0 - 6 od niedzieli
        Miesiące: 0 - 11
        wyprowadzanie roku -> zawsze +y=1900
        wprowadzanie roku 2015 -> zawsze 2015-y
    */

    @Override
    protected void onStart(){
        super.onStart();

        przeładujZakładkęKalendarza();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_glowny_kalendarz);

        Zmienne_Globalne.dzisiejszaData = new Date();
        Zmienne_Globalne.wybranaData = new Date();
        Metody_Pomocnicze.przepiszDate(Zmienne_Globalne.dzisiejszaData, Zmienne_Globalne.wybranaData);


        //region Inicjalizacja kontrolek
        gvwBilans = (GridView) findViewById(R.id.gvwBilans);
        gvwBilans.setNumColumns(1);
        gvwKalendarz = (GridView) findViewById(R.id.gvwKalendarz);
        gvwKalendarz.setNumColumns(7);

        lvwMiesiącMinus=(ListView)findViewById(R.id.lvwMiesiacMinus);
        lvwMiesiącWybierz=(ListView)findViewById(R.id.lvwMiesiacZmien);
        lvwMiesiącBilans =(ListView)findViewById(R.id.lvwMiesiacBilans);
        lvwMiesiącPlus=(ListView)findViewById(R.id.lvwMiesiacPlus);

        lvwZdarzeniaDnia=(ListView)findViewById(R.id.lvwZdarzeniaDnia);

        //region Inicjalizacja podświetleń kalendarza
        fltPodświetlenie = new FrameLayout[42];
        fltPodświetlenie[0] = (FrameLayout)findViewById(R.id.fltPozycja0);
        fltPodświetlenie[1] = (FrameLayout)findViewById(R.id.fltPozycja1);
        fltPodświetlenie[2] = (FrameLayout)findViewById(R.id.fltPozycja2);
        fltPodświetlenie[3] = (FrameLayout)findViewById(R.id.fltPozycja3);
        fltPodświetlenie[4] = (FrameLayout)findViewById(R.id.fltPozycja4);
        fltPodświetlenie[5] = (FrameLayout)findViewById(R.id.fltPozycja5);
        fltPodświetlenie[6] = (FrameLayout)findViewById(R.id.fltPozycja6);
        fltPodświetlenie[7] = (FrameLayout)findViewById(R.id.fltPozycja7);
        fltPodświetlenie[8] = (FrameLayout)findViewById(R.id.fltPozycja8);
        fltPodświetlenie[9] = (FrameLayout)findViewById(R.id.fltPozycja9);
        fltPodświetlenie[10] = (FrameLayout)findViewById(R.id.fltPozycja10);
        fltPodświetlenie[11] = (FrameLayout)findViewById(R.id.fltPozycja11);
        fltPodświetlenie[12] = (FrameLayout)findViewById(R.id.fltPozycja12);
        fltPodświetlenie[13] = (FrameLayout)findViewById(R.id.fltPozycja13);
        fltPodświetlenie[14] = (FrameLayout)findViewById(R.id.fltPozycja14);
        fltPodświetlenie[15] = (FrameLayout)findViewById(R.id.fltPozycja15);
        fltPodświetlenie[16] = (FrameLayout)findViewById(R.id.fltPozycja16);
        fltPodświetlenie[17] = (FrameLayout)findViewById(R.id.fltPozycja17);
        fltPodświetlenie[18] = (FrameLayout)findViewById(R.id.fltPozycja18);
        fltPodświetlenie[19] = (FrameLayout)findViewById(R.id.fltPozycja19);
        fltPodświetlenie[20] = (FrameLayout)findViewById(R.id.fltPozycja20);
        fltPodświetlenie[21] = (FrameLayout)findViewById(R.id.fltPozycja21);
        fltPodświetlenie[22] = (FrameLayout)findViewById(R.id.fltPozycja22);
        fltPodświetlenie[23] = (FrameLayout)findViewById(R.id.fltPozycja23);
        fltPodświetlenie[24] = (FrameLayout)findViewById(R.id.fltPozycja24);
        fltPodświetlenie[25] = (FrameLayout)findViewById(R.id.fltPozycja25);
        fltPodświetlenie[26] = (FrameLayout)findViewById(R.id.fltPozycja26);
        fltPodświetlenie[27] = (FrameLayout)findViewById(R.id.fltPozycja27);
        fltPodświetlenie[28] = (FrameLayout)findViewById(R.id.fltPozycja28);
        fltPodświetlenie[29] = (FrameLayout)findViewById(R.id.fltPozycja29);
        fltPodświetlenie[30] = (FrameLayout)findViewById(R.id.fltPozycja30);
        fltPodświetlenie[31] = (FrameLayout)findViewById(R.id.fltPozycja31);
        fltPodświetlenie[32] = (FrameLayout)findViewById(R.id.fltPozycja32);
        fltPodświetlenie[33] = (FrameLayout)findViewById(R.id.fltPozycja33);
        fltPodświetlenie[34] = (FrameLayout)findViewById(R.id.fltPozycja34);
        fltPodświetlenie[35] = (FrameLayout)findViewById(R.id.fltPozycja35);
        fltPodświetlenie[36] = (FrameLayout)findViewById(R.id.fltPozycja36);
        fltPodświetlenie[37] = (FrameLayout)findViewById(R.id.fltPozycja37);
        fltPodświetlenie[38] = (FrameLayout)findViewById(R.id.fltPozycja38);
        fltPodświetlenie[39] = (FrameLayout)findViewById(R.id.fltPozycja39);
        fltPodświetlenie[40] = (FrameLayout)findViewById(R.id.fltPozycja40);
        fltPodświetlenie[41] = (FrameLayout)findViewById(R.id.fltPozycja41);
        //endregion

        przeładujZakładkęKalendarza();

        ArrayAdapter adapter;
        String[] daneDoAdaptera;

        gvwNazwyDniTygodnia = (GridView)findViewById(R.id.gvwNazwyDniTygodnia);
        gvwNazwyDniTygodnia.setNumColumns(7);
        daneDoAdaptera = new String[] {Wyświetlanie_Tekstu.dzieńSkrótPoniedziałek(), Wyświetlanie_Tekstu.dzieńSkrótWtorek(), Wyświetlanie_Tekstu.dzieńSkrótŚroda(), Wyświetlanie_Tekstu.dzieńSkrótCzwartek(), Wyświetlanie_Tekstu.dzieńSkrótPiątek(), Wyświetlanie_Tekstu.dzieńSkrótSobota(), Wyświetlanie_Tekstu.dzieńSkrótNiedziela()};
        adapter = new ArrayAdapter(Ekran_Glowny_Kalendarz.this, R.layout.styl_grid_dni_tygodnia, daneDoAdaptera);
        gvwNazwyDniTygodnia.setAdapter(adapter);

        daneDoAdaptera = new String[] {"<"};
        adapter = new ArrayAdapter(Ekran_Glowny_Kalendarz.this, R.layout.styl_listview_strzalek_zmiany_miesiaca, daneDoAdaptera);
        lvwMiesiącMinus.setAdapter(adapter);

        daneDoAdaptera = new String[] { ">" };
        adapter = new ArrayAdapter(Ekran_Glowny_Kalendarz.this, R.layout.styl_listview_strzalek_zmiany_miesiaca, daneDoAdaptera);
        lvwMiesiącPlus.setAdapter(adapter);

        //endregion

        //region Klik zmiany daty
        lvwMiesiącWybierz.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Dialog_Wyboru_Daty dlgWybierzDate = new Dialog_Wyboru_Daty();
                dlgWybierzDate.działajBezOk(Ekran_Glowny_Kalendarz.this);

                dlgWybierzDate.btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgWybierzDate.czynnościKońcowe();

                        przeładujZakładkęKalendarza();

                        dlgWybierzDate.dialog.hide();
                    }
                });


            }
        });
        //endregion

        //region Klik miesiąc lewo/prawo
        lvwMiesiącMinus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Metody_Pomocnicze.zmieńMiesiąc(Zmienne_Globalne.wybranaData, Zmienne_Globalne.wybranaData.getMonth() - 1, true);
                przeładujZakładkęKalendarza();
            }
        });

        lvwMiesiącPlus.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Metody_Pomocnicze.zmieńMiesiąc(Zmienne_Globalne.wybranaData, Zmienne_Globalne.wybranaData.getMonth() + 1, true);
                przeładujZakładkęKalendarza();
            }
        });

        //endregion

        //region Klik dnia
        gvwKalendarz.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                boolean czyMożnaPrzejśćDoNastępnegoEkranu = false;
                try {
                    Date dataPomocnicza = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), Integer.parseInt(gvwKalendarz.getAdapter().getItem(position).toString()));

                    if(Metody_Pomocnicze.czyDatySąTakieSame(Zmienne_Globalne.wybranaData, dataPomocnicza)) {
                        czyMożnaPrzejśćDoNastępnegoEkranu = true;
                    }
                    else {
                        Metody_Pomocnicze.przepiszDate(dataPomocnicza, Zmienne_Globalne.wybranaData);
                        przeładujGridKalendarza();
                        przeładujZdarzeniaDnia();
                        czyMożnaPrzejśćDoNastępnegoEkranu = false;
                    }
                } catch (Exception e) {
                    czyMożnaPrzejśćDoNastępnegoEkranu = false;
                }
                if (czyMożnaPrzejśćDoNastępnegoEkranu) {
                    Intent i = new Intent(Ekran_Glowny_Kalendarz.this, Ekran_Zdarzen_Danego_Dnia.class);
                    startActivity(i);
                }
            }
        });
        //endregion

        //region Klik zdarzenia dnia
        lvwZdarzeniaDnia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listaPrzychodówDnia.size()>0 || listaRozchodówDnia.size()>0) {
                    Intent i = new Intent(Ekran_Glowny_Kalendarz.this, Ekran_Przegladania_Zdarzenia.class);

                    if(position>=listaRozchodówDnia.size()) { //przychód
                        Przychody przychód = listaPrzychodówDnia.get(position-listaRozchodówDnia.size());
                        i.putExtra("nazwa", przychód.getNazwaGrupy_PK_FK());
                        i.putExtra("numer", przychód.getNumerWystąpienia_PK());
                        i.putExtra("czyToRozchód", false);
                    }
                    else{
                        Rozchody rozchód = listaRozchodówDnia.get(position);
                        i.putExtra("nazwa", rozchód.getNazwaGrupy_PK_FK());
                        i.putExtra("numer", rozchód.getNumerWystąpienia_PK());
                        i.putExtra("czyToRozchód", true);
                    }
                    startActivity(i);
                }
            }
        });

        //endregion


        wyświetlPowiadomienia();
    }


    //region Przeładowanie zakładki
    private void przeładujZakładkęKalendarza(){
        przeładujGridKalendarza();

        podliczBilanse();

        przeładujGridBilansówTygodniowych();

        przeładujPasekZnazwąMiesiąca();
        przeładujBilansMiesiąca();

        przeładujZdarzeniaDnia();
    }

    private void przeładujGridKalendarza() {
        int ileDniMaMiesiąc = Metody_Pomocnicze.wyznaczIlośćDniWmiesiącu(Zmienne_Globalne.wybranaData);
        final Date dataPomocnicza = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), 1);
        final String[] dniMiesiąca = new String[wyznaczDzieńTygodnia(dataPomocnicza)-1 + ileDniMaMiesiąc];

        for(int i=0; i<fltPodświetlenie.length; i++) {
            fltPodświetlenie[i].setBackgroundColor(0x00FFFFFF);
        }

        wyznaczMiesiąc(dniMiesiąca, Zmienne_Globalne.wybranaData);
        wyznaczIlośćTygodni(dniMiesiąca.length);

        ArrayAdapter adapter = new ArrayAdapter(Ekran_Glowny_Kalendarz.this, R.layout.styl_grid_kalendarza, dniMiesiąca) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                int kolorTła = 0x00FFFFFF; // Transparent

                if( Zmienne_Globalne.wybranaData.getMonth() == Zmienne_Globalne.dzisiejszaData.getMonth() && Zmienne_Globalne.wybranaData.getYear()== Zmienne_Globalne.dzisiejszaData.getYear() && position == (wyznaczMiejsceNaGridzie(Zmienne_Globalne.dzisiejszaData)) ){
                    //dzisiaj
                    kolorTła = 0xFF0000FF; // Niebieski
                }
                if( position == (wyznaczMiejsceNaGridzie(Zmienne_Globalne.wybranaData)) ){
                    //wybrana data
                    kolorTła = 0xFFFF9900; // Pomarańczowo-żółty
                }

                int jakiToDzień = wyznaczDzieńMiesiąca(position);

                for (Rozchody rozchód:listaRozchodówObecnegoMiesiąca) {
                    if(rozchód.getData().getDate() <= jakiToDzień){
                        if(rozchód.getData().getDate() == jakiToDzień){
                            view.setBackgroundResource(R.drawable.ikona_zdarzenia);
                        }
                    }
                    else {
                        break; // lista jest sortowana przy tworzeniu
                    }
                }

                for (Przychody przychód:listaPrzychodówObecnegoMiesiąca) {
                    if(Zmienne_Globalne.ustawienia.getCzyWyświetlaćPrzychody() && przychód.getData().getDate() <= jakiToDzień){
                        if(przychód.getData().getDate() == jakiToDzień && przychód.getCzyWyświetlany()){
                            view.setBackgroundResource(R.drawable.ikona_zdarzenia);
                        }
                    }
                    else {
                        break; // lista jest sortowana przy tworzeniu
                    }
                }

                fltPodświetlenie[position].setBackgroundColor(kolorTła);

                return view;
            }
        };
        gvwKalendarz.setAdapter(adapter);
    }

    private void przeładujGridBilansówTygodniowych() {
        final String[] daneDoAdaptera = new String[ilośćTygodni];

        for(int i = 0; i<ilośćTygodni; i++){
            daneDoAdaptera[i] = przekształćNaWartośćBilansuDoWyświetlenia(bilansObecnegoMiesiąca.getBilanseTygodniowe()[i]);
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Ekran_Glowny_Kalendarz.this, R.layout.styl_grid_bilansow, daneDoAdaptera) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                double wartośćBilansu = Double.parseDouble(daneDoAdaptera[position]);
                int kolor = 0x00FFFFFF; // Transparentny
                if (wartośćBilansu>0) {
                    kolor = 0xff66ff33; // Zielony
                }
                else {
                    if (wartośćBilansu<0) {
                        kolor = Color.RED;
                    }
                    else {
                        kolor = Color.YELLOW;
                    }
                }
                view.setBackgroundColor(kolor);

                return view;
            }
        };

        gvwBilans.setAdapter(adapter);
    }

    private void przeładujPasekZnazwąMiesiąca() {
        String[] daneDoAdaptera = new String[] { Metody_Pomocnicze.zwróćNazwęMiesiąca(Zmienne_Globalne.wybranaData.getMonth()) + " " + String.valueOf(Zmienne_Globalne.wybranaData.getYear()+ Zmienne_Globalne.y) };
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Glowny_Kalendarz.this, R.layout.styl_listview_nazwy_miesiaca, daneDoAdaptera);

        lvwMiesiącWybierz.setAdapter(adapter);
    }

    private void przeładujBilansMiesiąca(){
        final String[] daneDoAdaptera = { przekształćNaWartośćBilansuDoWyświetlenia(bilansObecnegoMiesiąca.getBilansMiesięczny()) };

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Ekran_Glowny_Kalendarz.this, R.layout.styl_grid_bilansow, daneDoAdaptera){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = super.getView(position, convertView, parent);

                double wartośćBilansu = Double.parseDouble(daneDoAdaptera[position]);
                int kolor = 0x00FFFFFF; // Transparentny
                if (wartośćBilansu > 0) {
                    kolor = 0xff66ff33; //Zielony
                } else {
                    if (wartośćBilansu < 0) {
                        kolor = Color.RED;
                    } else {
                        kolor = Color.YELLOW;
                    }
                }

                view.setBackgroundColor(kolor);

                return view;
            }
        };
        lvwMiesiącBilans.setAdapter(adapter);
    }
    //endregion



    private void wyświetlPowiadomienia(){
        if(Zmienne_Globalne.ustawienia.getCzyPowiadomieniaWłączone()){
            String powiadomienieDoWyświetleniaRozchody="";

            for (Rozchody rozchód:listaRozchodówObecnegoMiesiąca) {
                if(!rozchód.getCzyPosiadane() && rozchód.getData().getDate()<=Zmienne_Globalne.dzisiejszaData.getDate() && rozchód.getCzyPowiadomieniaWłączone()) {
                    Grupy_Zdarzeń grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(rozchód.getNazwaGrupy_PK_FK());


                    if (!powiadomienieDoWyświetleniaRozchody.equals("")) {
                        powiadomienieDoWyświetleniaRozchody += ", ";
                    }

                    powiadomienieDoWyświetleniaRozchody += rozchód.getNazwaGrupy_PK_FK();

                    if (grupa.getCzyJawnaNumeracja()) {
                        Jednostki jednostka = Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK());
                        powiadomienieDoWyświetleniaRozchody += " " + jednostka.getSkrót() + String.valueOf(rozchód.getNumerWystąpienia_PK());
                    }

                }
            }

            String powiadomienieDoWyświetleniaPrzychody="";
            for (Przychody przychód:listaPrzychodówObecnegoMiesiąca) {
                if(przychód.getData().getDate()==Zmienne_Globalne.dzisiejszaData.getDate() && przychód.getCzyPowiadomieniaWłączone()) {
                    Grupy_Zdarzeń grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(przychód.getNazwaGrupy_PK_FK());

                    if (!powiadomienieDoWyświetleniaPrzychody.equals("")) {
                        powiadomienieDoWyświetleniaPrzychody += ", ";
                    }

                    powiadomienieDoWyświetleniaPrzychody += przychód.getNazwaGrupy_PK_FK();

                    if (grupa.getCzyJawnaNumeracja()) {
                        Jednostki jednostka = Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK());
                        powiadomienieDoWyświetleniaPrzychody += " " + jednostka.getSkrót() + String.valueOf(przychód.getNumerWystąpienia_PK());
                    }
                }
            }

            if(!powiadomienieDoWyświetleniaRozchody.equals("")) {
                powiadomienieDoWyświetleniaRozchody += " ";

                powiadomienieDoWyświetleniaRozchody += Wyświetlanie_Tekstu.guiPowiadomienieRozchodów();

                powiadomienieDoWyświetleniaRozchody += "!";
                Metody_Pomocnicze.tostDługi(Ekran_Glowny_Kalendarz.this, powiadomienieDoWyświetleniaRozchody);
            }

            if(!powiadomienieDoWyświetleniaPrzychody.equals("")) {
                powiadomienieDoWyświetleniaPrzychody += " ";

                powiadomienieDoWyświetleniaPrzychody += Wyświetlanie_Tekstu.guiPowiadomieniePrzycodów();

                powiadomienieDoWyświetleniaPrzychody += "!";
                Metody_Pomocnicze.tostDługi(Ekran_Glowny_Kalendarz.this, powiadomienieDoWyświetleniaPrzychody);
            }

        }
    }

    private void przeładujZdarzeniaDnia(){
        listaPrzychodówDnia = new ArrayList<Przychody>();
        listaRozchodówDnia = new ArrayList<Rozchody>();

        List<String> daneDoAdaptera = new ArrayList<String>();

        for (Rozchody rozchód:listaRozchodówObecnegoMiesiąca) {
            if(rozchód.getData().getDate() <= Zmienne_Globalne.wybranaData.getDate()) {
                if (rozchód.getData().getDate() == Zmienne_Globalne.wybranaData.getDate()) {
                    String nazwaDoAdaptera = Wyświetlanie_Tekstu.guiSymbolRozchodu();
                    listaRozchodówDnia.add(rozchód);
                    nazwaDoAdaptera += rozchód.getNazwaGrupy_PK_FK();

                    Grupy_Zdarzeń grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(rozchód.getNazwaGrupy_PK_FK());

                    if (grupa.getCzyJawnaNumeracja()) {
                        nazwaDoAdaptera += " " + Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK()).getSkrót();
                        nazwaDoAdaptera += String.valueOf(rozchód.getNumerWystąpienia_PK());
                    }

                    daneDoAdaptera.add(nazwaDoAdaptera);
                }
            }
            else {
                break; //lista jest posortowana
            }
        }

        if(Zmienne_Globalne.ustawienia.getCzyWyświetlaćPrzychody()) {
            for (Przychody przychód : listaPrzychodówObecnegoMiesiąca) {
                if (przychód.getData().getDate() <= Zmienne_Globalne.wybranaData.getDate()) {
                    if (przychód.getData().getDate() == Zmienne_Globalne.wybranaData.getDate() && przychód.getCzyWyświetlany()) {
                        String nazwaDoAdaptera = Wyświetlanie_Tekstu.guiSymbolPrzychodu();
                        listaPrzychodówDnia.add(przychód);
                        nazwaDoAdaptera += przychód.getNazwaGrupy_PK_FK();

                        Grupy_Zdarzeń grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(przychód.getNazwaGrupy_PK_FK());

                        if (grupa.getCzyJawnaNumeracja()) {
                            nazwaDoAdaptera += Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK()).getSkrót();
                            nazwaDoAdaptera += String.valueOf(przychód.getNumerWystąpienia_PK());
                        }

                        daneDoAdaptera.add(nazwaDoAdaptera);
                    }
                } else {
                    break; //lista jest posortowana
                }
            }
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Ekran_Glowny_Kalendarz.this, R.layout.styl_grid_dni_tygodnia, daneDoAdaptera);

        lvwZdarzeniaDnia.setAdapter(adapter);
    }

    private void podliczBilanse(){
        Date dataPomocnicza = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), 1);
        double[] bilanseTygodni = new double[ilośćTygodni];
        int dzieńTygodniaPierwszego = wyznaczDzieńTygodnia(dataPomocnicza);
        double bilansMiesiąca = 0.0;
        listaPrzychodówObecnegoMiesiąca = new ArrayList<Przychody>();
        listaRozchodówObecnegoMiesiąca = new ArrayList<Rozchody>();

        dataPomocnicza.setDate(-dzieńTygodniaPierwszego + 2); //dzień na 0 cofa o 1

        for (int i = 0; i < bilanseTygodni.length; i++) {
            double bilansTygodnia = 0.0;
            for (int j = 0; j < 7; j++, dataPomocnicza.setDate(dataPomocnicza.getDate() + 1)) {

                for (Przychody przychód : Zmienne_Globalne.listaPrzychodów) {
                    if (przychód.getData().getDate() == dataPomocnicza.getDate() && przychód.getData().getMonth() == dataPomocnicza.getMonth() && przychód.getData().getYear() == dataPomocnicza.getYear()) {
                        bilansTygodnia += przychód.getKwota();

                        if (przychód.getData().getMonth() == Zmienne_Globalne.wybranaData.getMonth()) {
                            listaPrzychodówObecnegoMiesiąca.add(przychód); //odrazu posortowane
                        }
                    }
                }

                for (Rozchody rozchód : Zmienne_Globalne.listaRozchodów) {
                    if (rozchód.getData().getDate() == dataPomocnicza.getDate() && rozchód.getData().getMonth() == dataPomocnicza.getMonth() && rozchód.getData().getYear() == dataPomocnicza.getYear()) {
                        bilansTygodnia -= rozchód.getKwota();

                        if (rozchód.getData().getMonth() == Zmienne_Globalne.wybranaData.getMonth()) {
                            listaRozchodówObecnegoMiesiąca.add(rozchód); //odrazu posortowane
                        }
                    }
                }
            }
            bilanseTygodni[i] = bilansTygodnia;
            bilansMiesiąca += bilansTygodnia;
        }

        bilansObecnegoMiesiąca = new Bilanse_Miesiąca(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), bilanseTygodni, bilansMiesiąca);
        //Zmienne_Globalne.listaBilansów.add(new Bilanse_Miesiąca(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), bilanseTygodni, bilansMiesiąca));
    }


    private void wyznaczIlośćTygodni(int ilośćPozycjiNaGridzie){
        double tmp = (double)ilośćPozycjiNaGridzie/7;
        ilośćTygodni = (int)Math.ceil(tmp);
    }

    public void wyznaczMiesiąc(String[] dniMiesiąca, Date data){
        int i;
        Date dataPomocnicza = new Date(data.getYear(), data.getMonth(), 1);
        int dzieńTygodnia = wyznaczDzieńTygodnia(dataPomocnicza);

        for(i=0; i<dzieńTygodnia-1; i++)
        {
            dniMiesiąca[i] = " ";
        }

        int ileDniMaMiesiąc = Metody_Pomocnicze.wyznaczIlośćDniWmiesiącu(data);
        for(int j=1; j<=ileDniMaMiesiąc; j++, i++)
        {
            dniMiesiąca[i] = String.valueOf(j);
        }

    }

    private String przekształćNaWartośćBilansuDoWyświetlenia(double wartość){
        if (wartość >= 0) {
            return String.format("+%.2f", wartość); //zabezpieczenie przed bugiem w doublu
        } else {
            return String.format("%.2f", wartość); //zabezpieczenie przed bugiem w doublu
        }
    }

    private int wyznaczDzieńTygodnia(Date data) //Przekształca na format 1-7 (Pn-Nd)
    {
        if(data.getDay()==0)
        {
            return 7;
        }
        else
        {
            return data.getDay();
        }
    }

    private int wyznaczMiejsceNaGridzie(Date data){
        Date dataPomocnicza = new Date(data.getYear(), data.getMonth(), 1);
        return data.getDate()+ wyznaczDzieńTygodnia(dataPomocnicza)-2; // -1 za indeksowanie grida od 0; -1 za dzień tygodnia
    }

    private int wyznaczDzieńMiesiąca(int pozycjaNaGridzie){
        Date dataPomocnicza = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), 1);
        return pozycjaNaGridzie - wyznaczDzieńTygodnia(dataPomocnicza) + 2;
    }


}
