package com.example.greg.finansier_san.Ekrany;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 15.01.16.
 */
public class Ekran_Zdarzen_Danej_Grupy extends AppCompatActivity {

    ListView lvwWystąpienia;
    List<Przychody> listaPrzychodów;
    List<Rozchody> listaRozchodów;
    Grupy_Zdarzeń grupa = new Grupy_Zdarzeń();
    Jednostki jednostka = new Jednostki();
    boolean czyPierwszy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_zdarzen_danej_grupy);

        Bundle odebraneDane = getIntent().getExtras();
        if(!odebraneDane.equals(null)){//zawsze prawda
            grupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(odebraneDane.getString("nazwaGrupy"));
            jednostka = Metody_Pomocnicze.znajdźJednostkę(grupa.getIdJednostki_FK());
        }

        //region Inicjalizacja kontrolek
        lvwWystąpienia = (ListView)findViewById(R.id.lvwWystapienia);
        TextView txvNagłówek = (TextView)findViewById(R.id.txvNazwaGrupyZdarzen);

        czyPierwszy=true;

        txvNagłówek.setText(grupa.getNazwaGrupyZdarzeń_PK());
        przeładujWystąpienia();

        //endregion

        lvwWystąpienia.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Ekran_Zdarzen_Danej_Grupy.this, Ekran_Przegladania_Zdarzenia.class);
                i.putExtra("nazwa", grupa.getNazwaGrupyZdarzeń_PK());

                if (position < listaPrzychodów.size()) {
                    i.putExtra("czyToRozchód", false);
                    i.putExtra("numer", listaPrzychodów.get(position).getNumerWystąpienia_PK());
                } else {
                    i.putExtra("czyToRozchód", true);
                    i.putExtra("numer", listaRozchodów.get(position).getNumerWystąpienia_PK());
                }
                startActivity(i);
            }
        });

    }


    @Override
    protected void onStart(){
        super.onStart();

        if(czyPierwszy){
            czyPierwszy=false;
        }
        else{
            przeładujWystąpienia();
        }
    }


    private void przeładujWystąpienia(){
        listaPrzychodów = new ArrayList<Przychody>();
        listaRozchodów = new ArrayList<Rozchody>();

        List<String> daneDoAdaptera = new ArrayList<String>();

        for (Przychody przychód: Zmienne_Globalne.listaPrzychodów) {
            if(przychód.getNazwaGrupy_PK_FK().equals(grupa.getNazwaGrupyZdarzeń_PK())){
                listaPrzychodów.add(przychód);

                String danaDoAdaptera;
                danaDoAdaptera = Wyświetlanie_Tekstu.guiSymbolPrzychodu();

                if(grupa.getCzyJawnaNumeracja()){
                    danaDoAdaptera += jednostka.getSkrót() + String.valueOf(przychód.getNumerWystąpienia_PK());
                }

                danaDoAdaptera += "\t(" + String.valueOf(przychód.getData().getDate()) + "." + String.valueOf(przychód.getData().getMonth()+1) + "." + String.valueOf(przychód.getData().getYear()+Zmienne_Globalne.y) + ")";

                daneDoAdaptera.add(danaDoAdaptera);
            }
        }

        for (Rozchody rozchód: Zmienne_Globalne.listaRozchodów) {
            if(rozchód.getNazwaGrupy_PK_FK().equals(grupa.getNazwaGrupyZdarzeń_PK())){
                listaRozchodów.add(rozchód);

                String danaDoAdaptera;
                danaDoAdaptera = Wyświetlanie_Tekstu.guiSymbolRozchodu();

                if(grupa.getCzyJawnaNumeracja()){
                    danaDoAdaptera += jednostka.getSkrót() + String.valueOf(rozchód.getNumerWystąpienia_PK());
                }

                danaDoAdaptera += "\t(" + String.valueOf(rozchód.getData().getDate()) + "."  + String.valueOf(rozchód.getData().getMonth()+1) + "."  + String.valueOf(rozchód.getData().getYear()+Zmienne_Globalne.y) + ")";

                daneDoAdaptera.add(danaDoAdaptera);
            }
        }

        ArrayAdapter adapter = new ArrayAdapter(Ekran_Zdarzen_Danej_Grupy.this, android.R.layout.simple_list_item_1, daneDoAdaptera);
        lvwWystąpienia.setAdapter(adapter);
    }


}
