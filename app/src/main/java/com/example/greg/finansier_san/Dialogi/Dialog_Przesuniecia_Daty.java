package com.example.greg.finansier_san.Dialogi;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;

import java.util.Date;

/**
 * Created by Admin on 29.12.15.
 */
public class Dialog_Przesuniecia_Daty {

    Button btnPlusDzień;
    Button btnPlusMiesiąc;
    Button btnPlusRok;
    public EditText etxnIleDni;
    public EditText etxnIleMiesięcy;
    public EditText etxnIleLat;
    Button btnMinusDzień;
    Button btnMinusMiesiąc;
    Button btnMinusRok;
    public Button btnOk;
    Button btnAnuluj;
    public Dialog dialog;

    public void działajBezOk(Context kontekst, Date staraData, Date nowaData){
        dialog = new Dialog(kontekst);
        dialog.setTitle(Wyświetlanie_Tekstu.tytułyNazwaDialoguPrzesuńZdarzenia());
        dialog.setContentView(R.layout.dialog_przesuniecia_daty);
        dialog.show();

        //region Inicjalizacja kontrolek
        btnPlusDzień = (Button) dialog.findViewById(R.id.btnPlusDzien);
        btnPlusMiesiąc = (Button) dialog.findViewById(R.id.btnPlusMiesiac);
        btnPlusRok = (Button) dialog.findViewById(R.id.btnPlusRok);
        etxnIleDni = (EditText) dialog.findViewById(R.id.etxnIleDni);
        etxnIleMiesięcy = (EditText) dialog.findViewById(R.id.etxnIleMiesiecy);
        etxnIleLat = (EditText) dialog.findViewById(R.id.etxNIleLat);
        btnMinusDzień = (Button) dialog.findViewById(R.id.btnMinusDzien);
        btnMinusMiesiąc = (Button) dialog.findViewById(R.id.btnMinusMiesiac);
        btnMinusRok = (Button) dialog.findViewById(R.id.btnMinusRok);
        btnOk = (Button) dialog.findViewById(R.id.btnOk);
        btnAnuluj = (Button) dialog.findViewById(R.id.btnAnuluj);

        TextView txvNdzień = (TextView)dialog.findViewById(R.id.txvNaglowekDzien);
        TextView txvNmiesiąc = (TextView)dialog.findViewById(R.id.txvNaglowekMiesiac);
        TextView txvNrok = (TextView)dialog.findViewById(R.id.txvNaglowekRok);

        //region Ustawianie tekstu kontrolek
        txvNdzień.setText(Wyświetlanie_Tekstu.gui_Nagłówek_dzień());
        txvNmiesiąc.setText(Wyświetlanie_Tekstu.gui_Nagłówek_miesiąc());
        txvNrok.setText(Wyświetlanie_Tekstu.gui_Nagłówek_rok());
        btnOk.setText(Wyświetlanie_Tekstu.gui_nagłówek_ok());
        btnAnuluj.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());
        //endregion

        etxnIleDni.setText(String.valueOf(nowaData.getDate()-staraData.getDate()));
        etxnIleMiesięcy.setText(String.valueOf(nowaData.getMonth() - staraData.getMonth()));
        etxnIleLat.setText(String.valueOf(nowaData.getYear()-staraData.getYear()));

        //endregion

        //region Buttony +/-
        btnPlusDzień.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dzień = Integer.parseInt(etxnIleDni.getText().toString());
                dzień++;
                etxnIleDni.setText(String.valueOf(dzień));
            }
        });

        btnMinusDzień.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int dzień = Integer.parseInt(etxnIleDni.getText().toString());
                dzień--;
                etxnIleDni.setText(String.valueOf(dzień));
            }
        });

        btnPlusMiesiąc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int miesiąc = Integer.parseInt(etxnIleMiesięcy.getText().toString());
                miesiąc++;
                etxnIleMiesięcy.setText(String.valueOf(miesiąc));
            }
        });

        btnMinusMiesiąc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int miesiąc = Integer.parseInt(etxnIleMiesięcy.getText().toString());
                miesiąc--;
                etxnIleMiesięcy.setText(String.valueOf(miesiąc));
            }
        });

        btnPlusRok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rok = Integer.parseInt(etxnIleLat.getText().toString());
                rok++;
                etxnIleLat.setText(String.valueOf(rok));
            }
        });

        btnMinusRok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int rok = Integer.parseInt(etxnIleLat.getText().toString());
                rok--;
                etxnIleLat.setText(String.valueOf(rok));
            }
        });
        //endregion

        btnAnuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

    }

}