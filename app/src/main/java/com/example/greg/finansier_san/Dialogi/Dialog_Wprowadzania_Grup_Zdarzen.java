package com.example.greg.finansier_san.Dialogi;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 15.01.16.
 */
public class Dialog_Wprowadzania_Grup_Zdarzen {

    public Dialog dialog;
    Context kontekst;

    EditText etxNazwaGrupy;
    Spinner spnKategoria;
    Button btnKategoriaDodaj;
    Spinner spnJednostka;
    Button btnJednostkaDodaj;
    CheckBox chbCzyUkrycNumerIJednostke;

    public Button btnDodaj;
    Button btnAnuluj;
    Grupy_Zdarzeń grupa;
    boolean czyNowy;

    Dialog_Wprowadzania_Jednostek dlgJednostki;
    Dialog_Wprowadzania_Kategorii_Zdarzen dlgKategorie;


    public Dialog_Wprowadzania_Grup_Zdarzen(Context kontekst){
        this.kontekst=kontekst;
        grupa = new Grupy_Zdarzeń();
        czyNowy = true;
    }

    public Dialog_Wprowadzania_Grup_Zdarzen(Context kontekst, Grupy_Zdarzeń istniejącaGrupa){
        this.kontekst=kontekst;
        grupa=istniejącaGrupa;
        czyNowy=false;
    }


    public void działajBezOk(final Context kontekst) {
        dialog = new Dialog(kontekst);
        dialog.setTitle(Wyświetlanie_Tekstu.tytułyNazwaDialoguGrupZdarzeń());
        dialog.setContentView(R.layout.dialog_wprowadzania_grup_zdarzen);
        dialog.show();

        //region Inicjalizacja kontrolek
        etxNazwaGrupy = (EditText)dialog.findViewById(R.id.etxNazwa);
        spnKategoria = (Spinner)dialog.findViewById(R.id.spnKategoria);
        btnKategoriaDodaj = (Button)dialog.findViewById(R.id.btnDodajKategorie);
        spnJednostka = (Spinner)dialog.findViewById(R.id.spnJednostka);
        btnJednostkaDodaj = (Button)dialog.findViewById(R.id.btnDodajJednostke);
        chbCzyUkrycNumerIJednostke = (CheckBox)dialog.findViewById(R.id.chbCzyUkrycNumerIJednostke);
        btnDodaj = (Button)dialog.findViewById(R.id.btnDodaj);
        btnAnuluj = (Button)dialog.findViewById(R.id.btnAnuluj);

        TextView txvNnazwaZdarzenia = (TextView)dialog.findViewById(R.id.txvNazwa);
        TextView txvNkategoria = (TextView)dialog.findViewById(R.id.txvKategoria);
        TextView txvNjednostka = (TextView)dialog.findViewById(R.id.txvJednostka);

        //region Ustawianie tekstu kontrolek
        txvNnazwaZdarzenia.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajNazwęZdarzenia());
        etxNazwaGrupy.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_nazwa());
        txvNkategoria.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wybierzKategorię());
        btnKategoriaDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        txvNjednostka.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wybierzJednostkę());
        btnJednostkaDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        chbCzyUkrycNumerIJednostke.setText(Wyświetlanie_Tekstu.gui_Nagłówek_UkryjNrIjednostkę());
        btnDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        btnAnuluj.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());
        //endregion


        załadujKategorieZdarzeńDoSpinnera(spnKategoria, Zmienne_Globalne.listaKategoriiZdarzeń);
        załadujJednostkiDoSpinnera(spnJednostka, Zmienne_Globalne.listaJednostek);


        if(!czyNowy){
            etxNazwaGrupy.setText(grupa.getNazwaGrupyZdarzeń_PK());
            spnKategoria.setSelection(znajdźPozycjeElementuSpinnera(grupa.getNazwaKategoriiZdarzenia_FK(), spnKategoria));
            spnJednostka.setSelection(Metody_Pomocnicze.znajdźPozycjeJednostkiNaLiście(grupa.getIdJednostki_FK()));
            chbCzyUkrycNumerIJednostke.setChecked(!grupa.getCzyJawnaNumeracja());
        }
        //endregion


        btnAnuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        btnJednostkaDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();

                dlgJednostki = new Dialog_Wprowadzania_Jednostek(dialog);
                dlgJednostki.działajBezOk(kontekst);

                dlgJednostki.btnDodaj.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgJednostki.działaniaKończąceZapiszNowąJednostkę(kontekst);


                        dlgJednostki.dialog.hide();

                        załadujJednostkiDoSpinnera(spnJednostka, Zmienne_Globalne.listaJednostek);
                        spnJednostka.setSelection(dlgJednostki.idNowejJednostki); //idNowejJednostki to pierwszy wolny int>0
                        dialog.show();
                    }
                });
            }
        });


        btnKategoriaDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.hide();

                dlgKategorie = new Dialog_Wprowadzania_Kategorii_Zdarzen(dialog);

                dlgKategorie.działajBezOk(kontekst);
                dlgKategorie.btnDodaj.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgKategorie.działaniaKończąceDodajNowąKategorie(kontekst);


                        dlgKategorie.dialog.hide();

                        załadujKategorieZdarzeńDoSpinnera(spnKategoria, Zmienne_Globalne.listaKategoriiZdarzeń);
                        spnKategoria.setSelection(spnKategoria.getCount()-1);
                        dialog.show();
                    }
                });
            }
        });

    }


    public void działaniaKończąceStandardowe(Context kontekst) {

        String nazwaStarejGrupy;
        if(czyNowy)
        {
            nazwaStarejGrupy = "";
        }
        else {
            nazwaStarejGrupy = grupa.getNazwaGrupyZdarzeń_PK();
        }

        try {
            if (etxNazwaGrupy.getText().toString().equals(null) || etxNazwaGrupy.getText().toString().equals("")) {
                throw new Exception(Wyświetlanie_Tekstu.błądPodajNazwęZdarzenia());
            }
            if (!Metody_Pomocnicze.czyGrupaOk(etxNazwaGrupy.getText().toString(), nazwaStarejGrupy)) {
                throw new Exception(Wyświetlanie_Tekstu.błądZdarzenieJużIstnieje());
            }

            grupa.setNazwaGrupyZdarzeń_PK(etxNazwaGrupy.getText().toString());
            grupa.setIdJednostki_FK(Zmienne_Globalne.listaJednostek.get(spnJednostka.getSelectedItemPosition()).getId_PK());
            grupa.setNazwaKategoriiZdarzenia_FK(spnKategoria.getSelectedItem().toString());
            grupa.setCzyJawnaNumeracja(!chbCzyUkrycNumerIJednostke.isChecked());

            if(czyNowy) {
                grupa.zapiszNowyDoBazy(kontekst);
                Zmienne_Globalne.listaGrupZdarzeń.add(grupa);
            }
            else {
                Łącznik połączenieBD = new Łącznik();
                połączenieBD.połączBD(kontekst);

                grupa.aktualizujKaskadowoDoBazy(połączenieBD, nazwaStarejGrupy);


                połączenieBD.rozłączBD();
            }

            Metody_Pomocnicze.tostKrótki(kontekst, Wyświetlanie_Tekstu.guiZapisano());
        }
        catch(Exception e) {
            Metody_Pomocnicze.tostKrótki(kontekst, e.getMessage());
        }

    }


    private void załadujKategorieZdarzeńDoSpinnera(Spinner spnKategoria, List<Kategorie_Zdarzeń> listaKategorii){
        List<String> dane = new ArrayList<String>();
        for (Kategorie_Zdarzeń kategoria:listaKategorii) {
            dane.add(kategoria.getNazwa_PK());
        }
        ArrayAdapter adapter = new ArrayAdapter(kontekst, R.layout.styl_elementu_spinnera, dane);
        spnKategoria.setAdapter(adapter);
    }

    private void załadujJednostkiDoSpinnera(Spinner spnJednostki, List<Jednostki> listaJednostek){
        List<String> dane = new ArrayList<String>();
        for (Jednostki jednostka:listaJednostek) {
            dane.add(jednostka.getNazwa() + " [" + jednostka.getSkrót() + "]");
        }
        ArrayAdapter adapter = new ArrayAdapter(kontekst, R.layout.styl_elementu_spinnera, dane);
        spnJednostki.setAdapter(adapter);
    }

    private int znajdźPozycjeElementuSpinnera(String szukanaWartość, Spinner spinner){
        for (int i=0; i<spinner.getCount(); i++) {
            if(spinner.getItemAtPosition(i).equals(szukanaWartość)){
                return i;
            }
        }
        return -1; //pilnować żeby się nie wykonało
    }

    private int wyciągnijIdJednostki(Spinner spnJednostki, List<Jednostki> listaJednostek){
        String nazwaJednostki="", skrótJednostki="", wybranaPozycja;
        wybranaPozycja=spnJednostki.getSelectedItem().toString();
        int i;
        for(i=0; wybranaPozycja.charAt(i+1) != '['; i++) //+1 bo nie chcemy spacji przed '['
        {
            nazwaJednostki+=wybranaPozycja.charAt(i);
        }

        for(i=i+2; wybranaPozycja.charAt(i) != ']'; i++)
        {
            skrótJednostki+=wybranaPozycja.charAt(i);
        }

        for (Jednostki jednostka : listaJednostek) {
            if(jednostka.getSkrót().equals(skrótJednostki) && jednostka.getNazwa().equals(nazwaJednostki))
            {
                return jednostka.getId_PK();
            }
        }
        return -1; //nigdy sie nie wykona
    }


}
