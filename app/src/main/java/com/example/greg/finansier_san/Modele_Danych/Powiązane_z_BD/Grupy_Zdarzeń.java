package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-26.
 */
public class Grupy_Zdarzeń {
    private String nazwaGrupyZdarzeń_PK;
    private int idJednostki_FK;
    private String nazwaKategoriiZdarzenia_FK;
    private boolean czyJawnaNumeracja;

    public Grupy_Zdarzeń(){}

    public Grupy_Zdarzeń(String nazwa, int jednostka, String kategoria, String czyJawnaNumeracja)
    {
        nazwaGrupyZdarzeń_PK=nazwa;
        idJednostki_FK=jednostka;
        nazwaKategoriiZdarzenia_FK=kategoria;
        if(czyJawnaNumeracja.equals("0")){
            this.czyJawnaNumeracja=false;
        }
        else {
            this.czyJawnaNumeracja=true;
        }
    }

    public Grupy_Zdarzeń(String nazwa, int jednostka, String kategoria, boolean czyJawnaNumeracja)
    {
        nazwaGrupyZdarzeń_PK=nazwa;
        idJednostki_FK=jednostka;
        nazwaKategoriiZdarzenia_FK=kategoria;
        this.czyJawnaNumeracja=czyJawnaNumeracja;
    }


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_GrupyZdarzeń), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaGrupZdarzeń.add(new Grupy_Zdarzeń(kursor.getString(0), kursor.getInt(1), kursor.getString(2), kursor.getString(3)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszNowyDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] wartości = {
                nazwaGrupyZdarzeń_PK,
                String.valueOf(idJednostki_FK),
                nazwaKategoriiZdarzenia_FK,
                Metody_Pomocnicze.boolNaStringa0lub1(czyJawnaNumeracja)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_GrupyZdarzeń, wartości));

        połączenieBD.rozłączBD();
    }

    public void aktualizujDoBazy(Łącznik połączenieBD, String staraNazwaGrupy){
        String[] nazwyPól = {
                Opisy.grupyZdarzen_nazwaPK,
                Opisy.grupyZdarzen_idJednostkiFK,
                Opisy.grupyZdarzen_nazwaKategoriiFK,
                Opisy.grupyZdarzen_czyJawnaNumeracja
        };


        String[] wartości = {
                nazwaGrupyZdarzeń_PK,
                String.valueOf(idJednostki_FK),
                nazwaKategoriiZdarzenia_FK,
                Metody_Pomocnicze.boolNaStringa0lub1(czyJawnaNumeracja)
        };


        String warunek = Opisy.grupyZdarzen_nazwaPK + "='" + staraNazwaGrupy + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_GrupyZdarzeń, nazwyPól, wartości, warunek));

    }

    public void aktualizujKaskadowoDoBazy(Łącznik połączenieBD, String staraNazwaGrupy){
        String[] nazwyPól = {
                Opisy.grupyZdarzen_nazwaPK,
                Opisy.grupyZdarzen_idJednostkiFK,
                Opisy.grupyZdarzen_nazwaKategoriiFK,
                Opisy.grupyZdarzen_czyJawnaNumeracja
        };

        String[] wartości = {
                nazwaGrupyZdarzeń_PK,
                String.valueOf(idJednostki_FK),
                nazwaKategoriiZdarzenia_FK,
                Metody_Pomocnicze.boolNaStringa0lub1(czyJawnaNumeracja)
        };

        String warunek = Opisy.grupyZdarzen_nazwaPK + "='" + staraNazwaGrupy + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_GrupyZdarzeń, nazwyPól, wartości, warunek));

        //zdarzenia
        nazwyPól = new String[]{
                Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk
        };
        wartości = new String[]{
                nazwaGrupyZdarzeń_PK
        };
        warunek = Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + "='" + staraNazwaGrupy + "'";
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_ZdarzeniaFinansowe, nazwyPól, wartości, warunek));

        //rozchody
        nazwyPól = new String[]{
                Opisy.rozchody_nazwaZdarzeniaPKfk
        };
        wartości = new String[]{
                nazwaGrupyZdarzeń_PK
        };
        warunek = Opisy.rozchody_nazwaZdarzeniaPKfk + "='" + staraNazwaGrupy + "'";
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Rozchody, nazwyPól, wartości, warunek));

        //przychody
        nazwyPól = new String[]{
                Opisy.przychody_nazwaZdarzeniaPKfk
        };
        wartości = new String[]{
                nazwaGrupyZdarzeń_PK
        };
        warunek = Opisy.przychody_nazwaZdarzeniaPKfk + "='" + staraNazwaGrupy + "'";
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Przychody, nazwyPól, wartości, warunek));

        //zdarzenia w scenariuszach
        nazwyPól = new String[]{
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk
        };
        wartości = new String[]{
                nazwaGrupyZdarzeń_PK
        };
        warunek = Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk + "='" + staraNazwaGrupy + "'";
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_ZdarzeniaWscenariuszach, nazwyPól, wartości, warunek));

    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń){
        //na wypadek gdyby constrainty raczyły zadziałać
        Zdarzenia_w_Scenariuszach.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń);
        Przychody.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń);
        Rozchody.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń);
        Zdarzenia_Finansowe.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń);

        String[] nazwyPól = {
                Opisy.grupyZdarzen_nazwaPK
        };

        String[] wartości = {
                nazwaGrupyZdarzeń
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_GrupyZdarzeń, nazwyPól, wartości));

    }


    //region Gettery/Settery

    public boolean getCzyJawnaNumeracja() {
        return czyJawnaNumeracja;
    }

    public void setCzyJawnaNumeracja(boolean czyJawnaNumeracja) {
        this.czyJawnaNumeracja = czyJawnaNumeracja;
    }


    public int getIdJednostki_FK() {
        return idJednostki_FK;
    }

    public void setIdJednostki_FK(int idJednostki_FK) {
        this.idJednostki_FK = idJednostki_FK;
    }


    public String getNazwaKategoriiZdarzenia_FK() {
        return nazwaKategoriiZdarzenia_FK;
    }

    public void setNazwaKategoriiZdarzenia_FK(String nazwaKategoriiZdarzenia_FK) {
        this.nazwaKategoriiZdarzenia_FK = nazwaKategoriiZdarzenia_FK;
    }


    public String getNazwaGrupyZdarzeń_PK() {
        return nazwaGrupyZdarzeń_PK;
    }

    public void setNazwaGrupyZdarzeń_PK(String nazwaGrupyZdarzeń_PK) {
        this.nazwaGrupyZdarzeń_PK = nazwaGrupyZdarzeń_PK;
    }
    //endregion
}
