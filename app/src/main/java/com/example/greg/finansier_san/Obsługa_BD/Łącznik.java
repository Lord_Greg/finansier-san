package com.example.greg.finansier_san.Obsługa_BD;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by Greg on 2015-12-01.
 */
public class Łącznik {

    public SQLiteDatabase baza;

    public void połączBD(Context kontekst){
        baza = kontekst.openOrCreateDatabase(Opisy.nazwaPlikuBazyDanych, SQLiteDatabase.OPEN_READWRITE, null);
    }

    public void rozłączBD(){
        baza.close();
    }

}
