package com.example.greg.finansier_san.Ekrany;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Grup_Zdarzen;
import com.example.greg.finansier_san.Dialogi.Dialog_Wyboru_Daty;
import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Ważności;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Greg on 2015-12-02.
 */
public class Ekran_Wprowadzania_Serii_Zdarzen extends AppCompatActivity {

    boolean czyToRozchód = true;
    boolean czyNoweZdarzenie = true;
    String nazwaStarejGrupy = "";

    Grupy_Zdarzeń edytowanaGrupa;
    Rozchody edytowanyRozchód = new Rozchody();
    Kategorie_Ważności edytowanaKategoriaważności;
    Przychody edytowanyPrzychód;

    Date poprzedniaData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_wprowadzania_serii_zdarzen);

        final Date dataZdarzenia = new Date();
        poprzedniaData = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), Zmienne_Globalne.wybranaData.getDate());

        //region Inicjalizacja kontrolek
        RadioButton rbtPrzychód = (RadioButton)findViewById(R.id.rbtPrzychod);
        final RadioButton rbtRozchód = (RadioButton)findViewById(R.id.rbtRozchod);
        final Spinner spnGrupaZdarzeń = (Spinner)findViewById(R.id.spnGrupaZdarzen);
        final Button btnGrupaZdarzeńDodaj = (Button)findViewById(R.id.btnDodajGrupeZdarzen);
        final EditText etxnLiczbaWystąpień = (EditText)findViewById(R.id.etxnLiczbaWystapien);
        final EditText etxnPoczątkoweWystąpienie = (EditText)findViewById(R.id.etxnPoczatekNumeracji);
        final EditText etxnKwota = (EditText)findViewById(R.id.etxnKwota);
        final ListView lvwData = (ListView)findViewById(R.id.lvwData);
        final EditText etxnPowtarzalniśćLiczba = (EditText)findViewById(R.id.etxnPowtarzalnoscLiczba);
        final Spinner spnPowtarzalność = (Spinner)findViewById(R.id.spnPowtarzalnoscJednostka);
        final EditText etxOpis = (EditText)findViewById(R.id.etxOpis);
        final CheckBox chbCzyWłączPowiadomienia = (CheckBox)findViewById(R.id.chbCzyWlaczPowiadomienia);
        final CheckBox chbCzyUkryć = (CheckBox)findViewById(R.id.chbCzyUkryc);
        final TextView txvKategoriaWażności = (TextView)findViewById(R.id.txvWaznosc);
        final Spinner spnKategoriaWażności = (Spinner)findViewById(R.id.spnWaznosc);
        Button btnDodajZdarzenie = (Button)findViewById(R.id.btnDodaj);
        Button btnAnulujZdarzenie = (Button)findViewById(R.id.btnAnuluj);

        TextView txvNagłówekLiczbaWystąpień = (TextView)findViewById(R.id.txvLiczbaWystapien);
        TextView txvNagłówekData = (TextView)findViewById(R.id.txvDataPierwszego);
        TextView txvNagłówekPowtarzalność = (TextView)findViewById(R.id.txvPowtarzalnosc);

        TextView txvNEkranSeriaZdarzeń = (TextView)findViewById(R.id.txvEkranNowaSeriaZdarzen);
        TextView txvNpodajNazwę = (TextView)findViewById(R.id.txvNazwa);
        TextView txvNLiczbaWystąpień = (TextView)findViewById(R.id.txvLiczbaWystapien);
        TextView txvNKwota = (TextView)findViewById(R.id.txvKwota);
        TextView txvNDataPierwszego = (TextView)findViewById(R.id.txvDataPierwszego);
        TextView txvNPowtarzalność = (TextView)findViewById(R.id.txvPowtarzalnosc);
        TextView txvNOpis = (TextView)findViewById(R.id.txvOpis);
        TextView txvNWażność = (TextView)findViewById(R.id.txvWaznosc);


        //region Ustawianie tekstu kontrolek
        txvNEkranSeriaZdarzeń.setText(Wyświetlanie_Tekstu.gui_Nagłówek_nowaSeriaZdarzeń());
        rbtPrzychód.setText(Wyświetlanie_Tekstu.guiPrzychód());
        rbtRozchód.setText(Wyświetlanie_Tekstu.guiRozchód());
        txvNpodajNazwę.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wybierzNazwęZdarzenia());
        btnGrupaZdarzeńDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        txvNLiczbaWystąpień.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajLiczbęWystąpień_());
        etxnLiczbaWystąpień.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_liczbaWystąpień());
        etxnPoczątkoweWystąpienie.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_zacznijOd());
        txvNKwota.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajKwotę_());
        etxnKwota.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_kwota());
        txvNDataPierwszego.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajDatęPierwszegoZdarzenia());
        txvNPowtarzalność.setText(Wyświetlanie_Tekstu.gui_Nagłówek_powtarzajCo_());
        etxnPowtarzalniśćLiczba.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_ile());
        txvNOpis.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajOpis_());
        etxOpis.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_opis());
        chbCzyWłączPowiadomienia.setText(Wyświetlanie_Tekstu.gui_Nagłówek_włączPowiadamienia());
        chbCzyUkryć.setText(Wyświetlanie_Tekstu.gui_Nagłówek_niePokazuj());
        txvNWażność.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wybierzKategorięWażności_());
        btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        btnAnulujZdarzenie.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());
        //endregion


        załadujGrupyZdarzeńDoSpinnera(spnGrupaZdarzeń, Zmienne_Globalne.listaGrupZdarzeń);
        załadujKategorieWażnościDoSpinnera(spnKategoriaWażności, Zmienne_Globalne.listaKategoriiWażności);

        String nagłówekDaty = Wyświetlanie_Tekstu.guiPodajDatę();

        Bundle przekazaneWartości = getIntent().getExtras();
        if(przekazaneWartości!=null)
        {
            //region Inicjalizacja przy edycji zdarzenia
            czyNoweZdarzenie = false;

            txvNagłówekLiczbaWystąpień.setVisibility(View.INVISIBLE);
            etxnLiczbaWystąpień.setVisibility(View.INVISIBLE);
            etxnPoczątkoweWystąpienie.setVisibility(View.INVISIBLE);
            txvNagłówekPowtarzalność.setVisibility(View.INVISIBLE);
            etxnPowtarzalniśćLiczba.setVisibility(View.INVISIBLE);
            spnPowtarzalność.setVisibility(View.INVISIBLE);

            btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.czynnościZapisz());

            edytowanaGrupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(przekazaneWartości.getString("nazwaGrupy"));
            nazwaStarejGrupy=edytowanaGrupa.getNazwaGrupyZdarzeń_PK();

            spnGrupaZdarzeń.setSelection(Zmienne_Globalne.listaGrupZdarzeń.indexOf(edytowanaGrupa));

            czyToRozchód = przekazaneWartości.getBoolean("czyToRozchód");

            if(czyToRozchód) {
                txvKategoriaWażności.setVisibility(View.VISIBLE);
                spnKategoriaWażności.setVisibility(View.VISIBLE);
                chbCzyUkryć.setVisibility(View.INVISIBLE);

                edytowanyRozchód = Metody_Pomocnicze.znajdźRozchód(edytowanaGrupa.getNazwaGrupyZdarzeń_PK(), przekazaneWartości.getInt("numer"));
                edytowanaKategoriaważności = Metody_Pomocnicze.znajdźKategorieWażności(edytowanyRozchód.getPoziomWażności_FK());

                spnKategoriaWażności.setSelection(edytowanaKategoriaważności.getPoziomWażności_PK());
                rbtRozchód.setChecked(true);
                etxnKwota.setText(String.valueOf(edytowanyRozchód.getKwota()));
                Metody_Pomocnicze.przepiszDate(edytowanyRozchód.getData(), dataZdarzenia);

                chbCzyWłączPowiadomienia.setChecked(edytowanyRozchód.getCzyPowiadomieniaWłączone());
                etxOpis.setText(edytowanyRozchód.getOpis());
            }
            else{
                nagłówekDaty+=Wyświetlanie_Tekstu.guiPierwszego();

                txvKategoriaWażności.setVisibility(View.INVISIBLE);
                spnKategoriaWażności.setVisibility(View.INVISIBLE);

                edytowanyPrzychód = Metody_Pomocnicze.znajdźPrzychód(edytowanaGrupa.getNazwaGrupyZdarzeń_PK(), przekazaneWartości.getInt("numer"));

                rbtPrzychód.setChecked(true);
                etxnKwota.setText(String.valueOf(edytowanyPrzychód.getKwota()));
                Metody_Pomocnicze.przepiszDate(edytowanyPrzychód.getData(), dataZdarzenia);
                chbCzyWłączPowiadomienia.setChecked(edytowanyPrzychód.getCzyPowiadomieniaWłączone());
                etxOpis.setText(edytowanyPrzychód.getOpis());
            }
            //endregion
        }
        else
        {
            //region Inicjalizacja przy nowym zdarzeniu

            txvNagłówekLiczbaWystąpień.setVisibility(View.VISIBLE);
            etxnLiczbaWystąpień.setVisibility(View.VISIBLE);
            etxnPoczątkoweWystąpienie.setVisibility(View.VISIBLE);
            txvNagłówekPowtarzalność.setVisibility(View.VISIBLE);
            etxnPowtarzalniśćLiczba.setVisibility(View.VISIBLE);
            spnPowtarzalność.setVisibility(View.VISIBLE);
            chbCzyUkryć.setVisibility(View.INVISIBLE);
            btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.czynnościDodaj());
            nazwaStarejGrupy = "";

            załadujPowtarzalnościDoSpinnera(spnPowtarzalność);
            //endregion
        }

        nagłówekDaty+=Wyświetlanie_Tekstu.guiZdarzenia();
        txvNagłówekData.setText(nagłówekDaty);
        rbtRozchód.setEnabled(czyNoweZdarzenie);
        rbtPrzychód.setEnabled(czyNoweZdarzenie);
        etxnLiczbaWystąpień.setEnabled(czyNoweZdarzenie);
        etxnPowtarzalniśćLiczba.setEnabled(czyNoweZdarzenie);
        Metody_Pomocnicze.przepiszDate(Zmienne_Globalne.wybranaData, dataZdarzenia);

        załadujDatęDoListView(lvwData, Zmienne_Globalne.wybranaData);

        //endregion

        rbtRozchód.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    czyToRozchód = true;
                    chbCzyUkryć.setVisibility(View.INVISIBLE);

                    txvKategoriaWażności.setVisibility(View.VISIBLE);
                    spnKategoriaWażności.setVisibility(View.VISIBLE);
                } else {
                    czyToRozchód = false;
                    txvKategoriaWażności.setVisibility(View.INVISIBLE);
                    spnKategoriaWażności.setVisibility(View.INVISIBLE);

                    chbCzyUkryć.setVisibility(View.VISIBLE);
                }
            }
        });


        lvwData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                final Dialog_Wyboru_Daty dlgWybierzDate = new Dialog_Wyboru_Daty();
                dlgWybierzDate.działajBezOk(Ekran_Wprowadzania_Serii_Zdarzen.this);

                dlgWybierzDate.btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgWybierzDate.czynnościKońcowe();

                        załadujDatęDoListView(lvwData, Zmienne_Globalne.wybranaData);

                        dlgWybierzDate.dialog.hide();
                    }
                });
            }


        });


        btnGrupaZdarzeńDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_Wprowadzania_Grup_Zdarzen dlgDodajGrupęZdarzeń = new Dialog_Wprowadzania_Grup_Zdarzen(Ekran_Wprowadzania_Serii_Zdarzen.this);
                dlgDodajGrupęZdarzeń.działajBezOk(Ekran_Wprowadzania_Serii_Zdarzen.this);

                dlgDodajGrupęZdarzeń.btnDodaj.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        {
                            dlgDodajGrupęZdarzeń.działaniaKończąceStandardowe(Ekran_Wprowadzania_Serii_Zdarzen.this);

                            załadujGrupyZdarzeńDoSpinnera(spnGrupaZdarzeń, Zmienne_Globalne.listaGrupZdarzeń);
                            spnGrupaZdarzeń.setSelection(spnGrupaZdarzeń.getAdapter().getCount() - 1);

                            dlgDodajGrupęZdarzeń.dialog.hide();
                        }
                    }
                });
            }
        });


        btnDodajZdarzenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //region Dodawanie i edycja zdarzenia
                try {

                    if(etxnKwota.getText().toString().equals(""))
                    {
                        throw new Exception(Wyświetlanie_Tekstu.błądPodajKwotę());
                    }

                    double kwota = Double.valueOf(etxnKwota.getText().toString());

                    if(czyNoweZdarzenie) {
                        //region Dodaj nowe zdarzenie
                        if(etxnPoczątkoweWystąpienie.getText().toString().equals(""))
                        {
                            throw new Exception(Wyświetlanie_Tekstu.błądPodajPoczątkoweWystąpienie());
                        }
                        if(etxnLiczbaWystąpień.getText().toString().equals(""))
                        {
                            throw  new Exception(Wyświetlanie_Tekstu.błądPodajIlośćWystąpień());
                        }
                        int wystąpienieStartowe = Integer.parseInt(etxnPoczątkoweWystąpienie.getText().toString()), ileWystąpień = Integer.parseInt(etxnLiczbaWystąpień.getText().toString());
                        int powtarzalnośćLiczba;
                        try {
                            powtarzalnośćLiczba = Integer.parseInt(etxnPowtarzalniśćLiczba.getText().toString());
                        } catch (Exception e) {
                            powtarzalnośćLiczba = 0;
                        }

                        int[] numeryWystąpień = new int[powtarzalnośćLiczba+1];
                        for(int i=0; i<numeryWystąpień.length; i++){
                            numeryWystąpień[i]=wystąpienieStartowe+i;
                        }

                        int numerIstniejącegoWystąpienia = Metody_Pomocnicze.czyWystąpienieIstnieje(spnGrupaZdarzeń.getSelectedItem().toString(), numeryWystąpień);

                        if(numerIstniejącegoWystąpienia > -1) {
                            throw  new Exception(Wyświetlanie_Tekstu.błądWystąpienieOnumerzeJużIstnieje1() + String.valueOf(numerIstniejącegoWystąpienia) + Wyświetlanie_Tekstu.błądWystąpienieOnumerzeJużIstnieje2());
                        }

                        if (czyToRozchód) //dodajemy rozchód
                        {
                            int poziomWażności = Metody_Pomocnicze.znajdźKategorieWażności(spnKategoriaWażności.getSelectedItem().toString()).getPoziomWażności_PK();
                            for (int i = 0; i < ileWystąpień; i++) {
                                Rozchody nowyRozchód = new Rozchody(spnGrupaZdarzeń.getSelectedItem().toString(), wystąpienieStartowe + i, kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), poziomWażności, false, false, "", -1);

                                nowyRozchód.zapiszDoBazy(Ekran_Wprowadzania_Serii_Zdarzen.this);
                                Zmienne_Globalne.listaRozchodów.add(nowyRozchód);

                                if (i + 1 < ileWystąpień) {
                                    switch ((int) spnPowtarzalność.getSelectedItemId()) {
                                        case 0: {
                                            Metody_Pomocnicze.zwiększDateOdni(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        case 1: {
                                            Metody_Pomocnicze.zwiększDateOtygodnie(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        case 2: {
                                            Metody_Pomocnicze.zwiększDateOmiesiące(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        case 3: {
                                            Metody_Pomocnicze.zwiększDateOlata(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        default: { //nigdy się nie wykona
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            for (int i = 0; i < ileWystąpień; i++) {
                                Przychody nowyPrzychód = new Przychody(spnGrupaZdarzeń.getSelectedItem().toString(), wystąpienieStartowe + i, kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), !chbCzyUkryć.isChecked());
                                nowyPrzychód.zapiszDoBazy(Ekran_Wprowadzania_Serii_Zdarzen.this);
                                Zmienne_Globalne.listaPrzychodów.add(nowyPrzychód);

                                if (i + 1 < ileWystąpień) {
                                    switch ((int) spnPowtarzalność.getSelectedItemId()) {
                                        case 0: {
                                            Metody_Pomocnicze.zwiększDateOdni(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        case 1: {
                                            Metody_Pomocnicze.zwiększDateOtygodnie(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        case 2: {
                                            Metody_Pomocnicze.zwiększDateOmiesiące(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        case 3: {
                                            Metody_Pomocnicze.zwiększDateOlata(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                        default: { //nigdy się nie wykona
                                            Metody_Pomocnicze.zwiększDateOdni(dataZdarzenia, powtarzalnośćLiczba);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        Metody_Pomocnicze.tostKrótki(Ekran_Wprowadzania_Serii_Zdarzen.this, Wyświetlanie_Tekstu.guiZdarzeniaDodanoPomyślnie());
                        //endregion
                    }
                    else {
                        //region Edycja Istniejącego Zdarzenia
                        Łącznik połączenieBD = new Łącznik();
                        połączenieBD.połączBD(Ekran_Wprowadzania_Serii_Zdarzen.this);

                        if(czyToRozchód) {
                            int poziomWażności = Metody_Pomocnicze.znajdźKategorieWażności(spnKategoriaWażności.getSelectedItem().toString()).getPoziomWażności_PK();
                            Rozchody zmodyfikowanyRozchód = new Rozchody(spnGrupaZdarzeń.getSelectedItem().toString(), edytowanyRozchód.getNumerWystąpienia_PK(), kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), poziomWażności, edytowanyRozchód.getCzyZapłacone(), edytowanyRozchód.getCzyPosiadane(), edytowanyRozchód.getNazwaPrenumeraty_FK(), edytowanyRozchód.getNrPrenumeraty_FK());

                            zmodyfikowanyRozchód.aktualizujDoBazy(połączenieBD, nazwaStarejGrupy);
                        }
                        else{
                            Przychody zmodyfikowanyPrzychód = new Przychody(spnGrupaZdarzeń.getSelectedItem().toString(), edytowanyPrzychód.getNumerWystąpienia_PK(), kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), !chbCzyUkryć.isChecked());

                            zmodyfikowanyPrzychód.aktualizujDoBazy(połączenieBD, nazwaStarejGrupy);
                        }
                        połączenieBD.rozłączBD();

                        //Przeładowanie list
                        Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Wprowadzania_Serii_Zdarzen.this);
                        Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Wprowadzania_Serii_Zdarzen.this);
                        Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Wprowadzania_Serii_Zdarzen.this);
                        Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Wprowadzania_Serii_Zdarzen.this);

                        Metody_Pomocnicze.tostKrótki(Ekran_Wprowadzania_Serii_Zdarzen.this, Wyświetlanie_Tekstu.guiZapisano());
                        //endregion
                    }
                }
                catch (Exception e){
                    Metody_Pomocnicze.tostDługi(Ekran_Wprowadzania_Serii_Zdarzen.this, e.getMessage());
                }
                //endregion

                finish();
            }
        });

        btnAnulujZdarzenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Metody_Pomocnicze.przepiszDate(poprzedniaData, Zmienne_Globalne.wybranaData);

                finish();
            }
        });
    }


    private void załadujDatęDoListView(ListView lvwDoUstawienia, Date data){
        ArrayAdapter adapter;
        String[] daneDoAdaptera = new String[] { String.valueOf(data.getDate()) + " " + Metody_Pomocnicze.zwróćNazwęMiesiąca(data.getMonth()) + " " + String.valueOf(data.getYear()+Zmienne_Globalne.y) };
        adapter = new ArrayAdapter(Ekran_Wprowadzania_Serii_Zdarzen.this, android.R.layout.simple_list_item_1, daneDoAdaptera);
        lvwDoUstawienia.setAdapter(adapter);

    }

    private void załadujGrupyZdarzeńDoSpinnera(Spinner spnGrupa, List<Grupy_Zdarzeń> listaGrup){
        List<String> dane = new ArrayList<String>();
        for (Grupy_Zdarzeń grupa:listaGrup) {
            dane.add(grupa.getNazwaGrupyZdarzeń_PK());
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Wprowadzania_Serii_Zdarzen.this, android.R.layout.simple_spinner_dropdown_item, dane);
        spnGrupa.setAdapter(adapter);
    }

    private void załadujPowtarzalnościDoSpinnera(Spinner spnPowtarzalności){
        List<String> dane = new ArrayList<String>();

        dane.add(Wyświetlanie_Tekstu.powtarzalnościDni());
        dane.add(Wyświetlanie_Tekstu.powtarzalnościTygodnie());
        dane.add(Wyświetlanie_Tekstu.powtarzalnościMiesiące());
        dane.add(Wyświetlanie_Tekstu.powtarzalnościLat());

        ArrayAdapter adapter = new ArrayAdapter(Ekran_Wprowadzania_Serii_Zdarzen.this, android.R.layout.simple_spinner_dropdown_item, dane);
        spnPowtarzalności.setAdapter(adapter);
    }

    private void załadujKategorieWażnościDoSpinnera(Spinner spnKategoria, List<Kategorie_Ważności> listaKategorii){
        List<String> dane = new ArrayList<String>();
        for (Kategorie_Ważności kategoria:listaKategorii) {
            dane.add(kategoria.getSymbol());
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Wprowadzania_Serii_Zdarzen.this, android.R.layout.simple_spinner_dropdown_item, dane);
        spnKategoria.setAdapter(adapter);
    }


    private int znajdźPozycjeElementuSpinnera(String szukanaWartość, Spinner spinner){
        for (int i=0; i<spinner.getCount(); i++) {
            if(spinner.getItemAtPosition(i).equals(szukanaWartość)){
                return i;
            }
        }
        return -1; //pilnować żeby się nie wykonało
    }

}
