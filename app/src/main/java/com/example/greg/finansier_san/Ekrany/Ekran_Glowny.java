package com.example.greg.finansier_san.Ekrany;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.io.File;

public class Ekran_Glowny extends TabActivity /*AppCompatActivity*/ {

    //region Metody startowe aplikacji
    private void inicjalizujBD(Context kontekst) {
        if(!czyPlikBazyDanychIstnieje(Opisy.nazwaPlikuBazyDanych)){
            stwórzBD(kontekst);
        }

        try {
            Metody_Pomocnicze.wczytajWszystkoZbazy(kontekst);
        }
        catch (Exception e) {
            Metody_Pomocnicze.tostDługi(Ekran_Glowny.this, Wyświetlanie_Tekstu.błądOdczytuZbd());
        }
    }


    public void stwórzBD(Context kontekst)
    {
        try {
            Łącznik połączenieZbazą = new Łącznik();
            połączenieZbazą.połączBD(kontekst);

            Polecenia_SQL.utwórzBD(połączenieZbazą);

            połączenieZbazą.rozłączBD();
        }
        catch (Exception e) { Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądPołączeniaZbazą()); }
    }

    public boolean czyPlikBazyDanychIstnieje(String nazwaPliku) {
        File plik = getApplicationContext().getDatabasePath(nazwaPliku);

        if (!plik.exists()) {
            return false;
        } else {
            return true;
        }
    }

    //endregion


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekran__glowny);

        inicjalizujBD(Ekran_Glowny.this);

        TabHost tabHost = getTabHost();

        tabHost.addTab(tabHost.newTabSpec("tabhKalendarz").setIndicator(Wyświetlanie_Tekstu.zakładkaKalendarz()).setContent(new Intent(Ekran_Glowny.this, Ekran_Glowny_Kalendarz.class)));
        tabHost.addTab(tabHost.newTabSpec("tabhLista").setIndicator(Wyświetlanie_Tekstu.zakładkaLista()).setContent(new Intent(Ekran_Glowny.this, Ekran_Glowny_Lista.class)));

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        if(Zmienne_Globalne.ustawienia.getJęzyk().equals(Wyświetlanie_Tekstu.językAplikacjiPolski))
        getMenuInflater().inflate(R.menu.menu_ekran__glowny, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent i = new Intent(Ekran_Glowny.this, Ekran_Opcji.class);
            startActivity(i);


            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
