package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-29.
 */
public class Scenariusze_Wydarzeń {
    private String nazwa_PK, opis;
    private double kwotaWportfelu, oszczędności;

    public Scenariusze_Wydarzeń(String nazwa, double portfel, double oszczędności, String opis){
        nazwa_PK=nazwa;
        kwotaWportfelu=portfel;
        this.oszczędności=oszczędności;
        this.opis=opis;
    }


    //region Gettery/Settery
    public String getNazwa_PK() {
        return nazwa_PK;
    }

    public void setNazwa_PK(String nazwa_PK) {
        this.nazwa_PK = nazwa_PK;
    }


    public double getKwotaWportfelu() {
        return kwotaWportfelu;
    }

    public void setKwotaWportfelu(double kwotaWportfelu) {
        this.kwotaWportfelu = kwotaWportfelu;
    }


    public double getOszczędności() {
        return oszczędności;
    }

    public void setOszczędności(double oszczędności) {
        this.oszczędności = oszczędności;
    }


    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }
    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_ScenariuszeWydarzeń), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaScenariuszyWydarzeń.add(new Scenariusze_Wydarzeń(kursor.getString(0), kursor.getDouble(1), kursor.getDouble(2), kursor.getString(3)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] wartości = {
                nazwa_PK,
                String.valueOf(kwotaWportfelu),
                String.valueOf(oszczędności),
                opis
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_ScenariuszeWydarzeń, wartości));

        połączenieBD.rozłączBD();
    }

}
