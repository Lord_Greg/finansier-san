package com.example.greg.finansier_san.Dialogi;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-12-02.
 */
public class Dialog_Wprowadzania_Kategorii_Zdarzen {

    public Dialog dialog;

    EditText etxNazwaKategorii;
    EditText etxOpis;
    public Button btnDodaj;
    Button btnAnuluj;
    Kategorie_Zdarzeń kategoria;
    boolean czyNowy;
    boolean czyOżywićDialog;
    Dialog dialogDoOżywienia;

    public Dialog_Wprowadzania_Kategorii_Zdarzen(Kategorie_Zdarzeń edytowanaKategoria) {
        kategoria = edytowanaKategoria;
        czyNowy=false;
        czyOżywićDialog=false;
    }

    public Dialog_Wprowadzania_Kategorii_Zdarzen() {
        kategoria = new Kategorie_Zdarzeń();
        czyNowy=true;
        czyOżywićDialog=false;
    }

    public Dialog_Wprowadzania_Kategorii_Zdarzen(Dialog dialogDoOżywienia) {
        kategoria = new Kategorie_Zdarzeń();
        czyNowy=true;
        this.dialogDoOżywienia=dialogDoOżywienia;
        this.czyOżywićDialog=true;
    }

    public void działajBezOk(final Context kontekst) {
        dialog = new Dialog(kontekst);
        dialog.setTitle(Wyświetlanie_Tekstu.tytułyNazwaDialoguKategoriiZdarzeń());
        dialog.setContentView(R.layout.dialog_wprowadzania_kategorii_zdarzen);
        dialog.show();

        //region Inicjalizacja kontrolek
        etxNazwaKategorii = (EditText) dialog.findViewById(R.id.etxKategoria);
        etxOpis = (EditText) dialog.findViewById(R.id.etxOpis);
        btnDodaj = (Button) dialog.findViewById(R.id.btnDodaj);
        btnAnuluj = (Button) dialog.findViewById(R.id.btnAnuluj);

        TextView txvNnazwa = (TextView)dialog.findViewById(R.id.txvNazwa);
        TextView txvNopis = (TextView)dialog.findViewById(R.id.txvOpis);

        //region Ustawianie tekstu kontrolek
        txvNnazwa.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajNazwęKategorii());
        etxNazwaKategorii.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_nazwa());
        txvNopis.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajOpis());
        etxOpis.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_opis());
        btnDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        btnAnuluj.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());

        if(!czyNowy){
            etxNazwaKategorii.setText(kategoria.getNazwa_PK());
            etxOpis.setText(kategoria.getOpis());
        }
        //endregion

        //endregion


        btnAnuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

                if(czyOżywićDialog){
                    dialogDoOżywienia.show();
                }

            }
        });

    }


    public void działaniaKończąceDodajNowąKategorie(Context kontekst) {
        String nazwaKategorii = etxNazwaKategorii.getText().toString(), opis = etxOpis.getText().toString();

        if (nazwaKategorii.equals("")) {
            Toast.makeText(kontekst, Wyświetlanie_Tekstu.błądPodajNazwęKategoriiZdarzeń(), Toast.LENGTH_LONG).show();
        }
        else {
            if(Metody_Pomocnicze.czyNazwaKategoiiZdarzeńJestWolna(nazwaKategorii)) {

                Kategorie_Zdarzeń nowakategoria = new Kategorie_Zdarzeń(nazwaKategorii, opis, Wyświetlanie_Tekstu.kolorDomyślny()); //kolor niezaimplementowany

                try {
                    nowakategoria.zapiszDoBazy(kontekst);
                    Zmienne_Globalne.listaKategoriiZdarzeń.add(nowakategoria);
                    Metody_Pomocnicze.tostKrótki(kontekst, Wyświetlanie_Tekstu.guiKategoriaDodana());
                } catch (Exception e) {
                    Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądKategoriaNieDodana());
                }

            }
            else{
                Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądKategoriaJużIstnieje());
            }

        }
    }


    public void działaniaKończąceEdytujIstniejącąKategorie(Context kontekst) {
        String staraNazwaKategorii = kategoria.getNazwa_PK();
        String nazwaKategorii = etxNazwaKategorii.getText().toString(), opis = etxOpis.getText().toString();
        if (nazwaKategorii.equals("")) {
            Toast.makeText(kontekst, Wyświetlanie_Tekstu.błądPodajNazwęKategoriiZdarzeń(), Toast.LENGTH_LONG).show();
        }
        else {
            if(Metody_Pomocnicze.czyNazwaKategoiiZdarzeńJestWolna(nazwaKategorii, staraNazwaKategorii)) {

                kategoria.setNazwa_PK(nazwaKategorii);
                kategoria.setOpis(opis);

                try {
                    kategoria.aktualizujDoBazy(kontekst, staraNazwaKategorii);
                    Metody_Pomocnicze.tostKrótki(kontekst, Wyświetlanie_Tekstu.guiZapisano());
                } catch (Exception e) {
                    Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądZapisu());
                }
            }
            else{
                Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądKategoriaJużIstnieje());
            }
        }
    }


}
