package com.example.greg.finansier_san.Modele_Danych;

import java.util.Date;

/**
 * Created by Greg on 2015-11-29.
 */
public class Bilanse_Miesiąca {
    private Date data;
    private double[] bilanseTygodniowe;
    private double bilansMiesięczny;

    public Bilanse_Miesiąca(int rok, int miesiąc, double[] bilanseTygodniowe, double bilansMiesięczny){
        data = new Date(rok, miesiąc, 1);
        this.bilanseTygodniowe = new double[bilanseTygodniowe.length];
        for(int i=0; i<bilanseTygodniowe.length; i++){
            this.bilanseTygodniowe[i] = bilanseTygodniowe[i];
        }
        this.bilansMiesięczny = bilansMiesięczny;
    }

    public Bilanse_Miesiąca(double[] bilanseTygodniowe) //głównie do zgłaszania braku bilansu
    {
        this.bilanseTygodniowe = new double[bilanseTygodniowe.length];
        for(int i=0; i<bilanseTygodniowe.length; i++){
            this.bilanseTygodniowe[i] = bilanseTygodniowe[i];
        }
    }


    //region Gettery/Settery

    public Date getData() {
        return data;
    }

    public void setData_PK_FK(Date data) {
        this.data = new Date(data.getYear(), data.getMonth(), data.getDate());
    }

    public double[] getBilanseTygodniowe() {
        return bilanseTygodniowe;
    }

    public void setBilanseTygodniowe(double[] bilanseTygodniowe) {
        this.bilanseTygodniowe = new double[bilanseTygodniowe.length];
        for(int i=0; i<bilanseTygodniowe.length; i++){
            this.bilanseTygodniowe[i] = bilanseTygodniowe[i];
        }
    }

    public double getBilansMiesięczny() {
        return bilansMiesięczny;
    }

    public void setBilansMiesięczny(double bilansMiesięczny) {
        this.bilansMiesięczny = bilansMiesięczny;
    }
    //endregion

}
