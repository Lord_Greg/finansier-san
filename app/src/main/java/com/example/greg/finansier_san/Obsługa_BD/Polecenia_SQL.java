package com.example.greg.finansier_san.Obsługa_BD;

import com.example.greg.finansier_san.Teksty_Interfejsu.Język_Polski;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;

/**
 * Created by Greg on 2015-12-02.
 */
public class Polecenia_SQL {

    public static void utwórzBD(Łącznik połączenieZbazą){
        String polecenie = "";
        String[] polaZtypami, polaZpk, polaZfk, polaZcelamiFK;

        polecenie = Polecenia_SQL.włączObsługeFK();
        połączenieZbazą.baza.execSQL(polecenie);


        //Jednostki
        polaZtypami = new String[] {
                Opisy.jednostki_idPK + " " + Opisy.TYP_DANYCH_INT,
                Opisy.jednostki_nazwa + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.jednostki_skrót + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.jednostki_idPK
        };

        polaZfk = null;

        polaZcelamiFK = null;

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_Jednostki, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);


        //Kategorie Zdarzeń
        polaZtypami = new String[] {
                Opisy.kategorieZdarzeń_nazwaPK + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.kategorieZdarzeń_opis + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.kategorieZdarzeń_kolor + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.kategorieZdarzeń_nazwaPK
        };

        polaZfk = null;

        polaZcelamiFK = null;

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_KategorieZdarzeń, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Grupy Zdarzeń
        polaZtypami = new String[] {
                Opisy.grupyZdarzen_nazwaPK + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.grupyZdarzen_idJednostkiFK + " " + Opisy.TYP_DANYCH_INT,
                Opisy.grupyZdarzen_nazwaKategoriiFK + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.grupyZdarzen_czyJawnaNumeracja + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.grupyZdarzen_nazwaPK
        };

        polaZfk = new String[] {
                Opisy.grupyZdarzen_idJednostkiFK,
                Opisy.grupyZdarzen_nazwaKategoriiFK
        };

        polaZcelamiFK = new String[] {
                Opisy.tab_Jednostki + "(" + Opisy.jednostki_idPK + ")",
                Opisy.tab_KategorieZdarzeń + "(" + Opisy.kategorieZdarzeń_nazwaPK + ")"
        };

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_GrupyZdarzeń, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Zdarzenia Finansowe
        polaZtypami = new String[] {
                Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + " " + Opisy.TYP_DANYCH_INT,
                Opisy.zdarzeniaFinansowe_kwota + " " + Opisy.TYP_DANYCH_REAL,
                Opisy.zdarzeniaFinansowe_dataDzień + " " + Opisy.TYP_DANYCH_INT,
                Opisy.zdarzeniaFinansowe_dataMiesiac + " " + Opisy.TYP_DANYCH_INT,
                Opisy.zdarzeniaFinansowe_dataRok + " " + Opisy.TYP_DANYCH_INT,
                Opisy.zdarzeniaFinansowe_opis + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.zdarzeniaFinansowe_czyPowiadomieniaWłączone + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk,
                Opisy.zdarzeniaFinansowe_nrWystąpieniaPK
        };

        polaZfk = new String[] {
                Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk
        };

        polaZcelamiFK = new String[] {
                Opisy.tab_GrupyZdarzeń + "(" + Opisy.grupyZdarzen_nazwaPK + ")"
        };

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_ZdarzeniaFinansowe, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Przychody
        polaZtypami = new String[] {
                Opisy.przychody_nazwaZdarzeniaPKfk + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.przychody_nrWystąpieniaPKfk + " " + Opisy.TYP_DANYCH_INT,
                Opisy.przychody_czyWyświetlany + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.przychody_nazwaZdarzeniaPKfk,
                Opisy.przychody_nrWystąpieniaPKfk
        };

        polaZfk = new String[] {
                Opisy.przychody_nazwaZdarzeniaPKfk + ", " +  Opisy.przychody_nrWystąpieniaPKfk
        };

        polaZcelamiFK = new String[] {
                Opisy.tab_ZdarzeniaFinansowe + "(" + Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + ", " + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + ")"
                //Opisy.tab_ZdarzeniaFinansowe + "(" + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + ")"
        };

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_Przychody, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Kategorie Ważności
        polaZtypami = new String[] {
                Opisy.kategorieWażności_poziomWażnościPK + " " + Opisy.TYP_DANYCH_INT,
                Opisy.kategorieWażności_symbol + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.kategorieWażności_kwotaDozwolona + " " + Opisy.TYP_DANYCH_REAL,
                Opisy.kategorieWażności_opis + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.kategorieWażności_poziomWażnościPK
        };

        polaZfk = null;

        polaZcelamiFK = null;

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_KategorieWażności, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Rozchody
        polaZtypami = new String[] {
                Opisy.rozchody_nazwaZdarzeniaPKfk + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.rozchody_nrWystąpieniaPKfk + " " + Opisy.TYP_DANYCH_INT,
                Opisy.rozchody_poziomWażnościFK + " " + Opisy.TYP_DANYCH_INT,
                Opisy.rozchody_czyZapłacone + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.rozchody_czyPosiadane + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.rozchody_nazwaPrenumeratyFK + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.rozchody_nrPrenumeratyFK + " " + Opisy.TYP_DANYCH_INT
        };

        polaZpk = new String[] {
                Opisy.rozchody_nazwaZdarzeniaPKfk,
                Opisy.rozchody_nrWystąpieniaPKfk
        };

        polaZfk = new String[] {
                Opisy.rozchody_nazwaZdarzeniaPKfk + ", " + Opisy.rozchody_nrWystąpieniaPKfk,
                Opisy.rozchody_poziomWażnościFK,
                Opisy.rozchody_nazwaPrenumeratyFK + ", " + Opisy.rozchody_nrPrenumeratyFK
        };

        polaZcelamiFK = new String[] {
                Opisy.tab_ZdarzeniaFinansowe + "(" + Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + ", " + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + ")",
                //Opisy.tab_ZdarzeniaFinansowe + "(" + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + ")",
                Opisy.tab_KategorieWażności + "(" + Opisy.kategorieWażności_poziomWażnościPK + ")",
                Opisy.tab_Rozchody + "(" + Opisy.rozchody_nazwaZdarzeniaPKfk + ", " + Opisy.rozchody_nrWystąpieniaPKfk + ")",
                //Opisy.tab_Rozchody + "(" + Opisy.rozchody_nrWystąpieniaPKfk + ")"
        };

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_Rozchody, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);




        //Scenariusze Wydarzeń
        polaZtypami = new String[] {
                Opisy.scenariuszeWydarzen_nazwaPK + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.scenariuszeWydarzen_kwotaWportfelu + " " + Opisy.TYP_DANYCH_REAL,
                Opisy.scenariuszeWydarzen_oszczędności + " " + Opisy.TYP_DANYCH_REAL,
                Opisy.scenariuszeWydarzen_opis + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.scenariuszeWydarzen_nazwaPK
        };

        polaZfk = null;

        polaZcelamiFK = null;

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_ScenariuszeWydarzeń, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Zdarzenia w Scenariuszach
        polaZtypami = new String[] {
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.zdarzeniaWscenariuszach_nrWystąpieniaPKfk + " " + Opisy.TYP_DANYCH_INT,
                Opisy.zdarzeniaWscenariuszach_nazwaScenariuszaPKfk + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = new String[] {
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk,
                Opisy.zdarzeniaWscenariuszach_nrWystąpieniaPKfk,
                Opisy.zdarzeniaWscenariuszach_nazwaScenariuszaPKfk
        };

        polaZfk = new String[]{
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk + ", " + Opisy.zdarzeniaWscenariuszach_nrWystąpieniaPKfk,
                Opisy.zdarzeniaWscenariuszach_nazwaScenariuszaPKfk
        };

        polaZcelamiFK = new String[]{
                Opisy.tab_ZdarzeniaFinansowe + "(" + Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk + ", " + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + ")",
                //Opisy.tab_ZdarzeniaFinansowe + "(" + Opisy.zdarzeniaFinansowe_nrWystąpieniaPK + ")",
                Opisy.tab_ScenariuszeWydarzeń + "(" + Opisy.scenariuszeWydarzen_nazwaPK + ")"
        };

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_ZdarzeniaWscenariuszach, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);



        //Ustawienia
        polaZtypami = new String[] {
                Opisy.ustawienia_czyWyświetlaćPrzychody + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.ustawienia_nazwaWaluty + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.ustawienia_symbolWaluty + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.ustawienia_język + " " + Opisy.TYP_DANYCH_TEXT,
                Opisy.ustawienia_czyPowiadomieniaWłączone + " " + Opisy.TYP_DANYCH_TEXT
        };

        polaZpk = null;

        polaZfk = null;

        polaZcelamiFK = null;

        polecenie = Polecenia_SQL.stwórzTabele(Opisy.tab_Ustawienia, polaZtypami, polaZpk, polaZfk, polaZcelamiFK);
        połączenieZbazą.baza.execSQL(polecenie);





        //Wstawianie wartości

            //Ustawienia
        String[] wartościDoWstawienia = new String[] {"1", "PLN", "zł", Wyświetlanie_Tekstu.językAplikacjiPolski, "1"};
        polecenie = wstawWartości(Opisy.tab_Ustawienia, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

            //Jednostki
        wartościDoWstawienia = new String[] {"0", Język_Polski.BD_jednostki_nazwa_nic, Język_Polski.BD_jednostki_skrót_nic};
        polecenie = wstawWartości(Opisy.tab_Jednostki, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"1", Język_Polski.BD_jednostki_nazwa_tom, Język_Polski.BD_jednostki_skrót_kratka};
        polecenie = wstawWartości(Opisy.tab_Jednostki, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"2", Język_Polski.BD_jednostki_nazwa_numer, Język_Polski.BD_jednostki_skrót_nr};
        polecenie = wstawWartości(Opisy.tab_Jednostki, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"3", Język_Polski.BD_jednostki_nazwa_część, Język_Polski.BD_jednostki_skrót_cz};
        polecenie = wstawWartości(Opisy.tab_Jednostki, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"4", Język_Polski.BD_jednostki_nazwa_tom, Język_Polski.BD_jednostki_skrót_tom};
        polecenie = wstawWartości(Opisy.tab_Jednostki, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);


            //Kategorie Zdarzeń
        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_nieprzypisane, "", Język_Polski.kolor_domyślny };
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_manga, "", "czerwony"};
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_prasa, "", "fioletowy"};
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_prezenty, "", "niebieski"};
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_rachunki, "", "pomarańczowy"};
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_zarobki, "", "zielony"};
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {Język_Polski.BD_kategorieZdarzeń_nazwa_kieszonkowe, "", "żółty"};
        polecenie = wstawWartości(Opisy.tab_KategorieZdarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);


            //Kategorie Ważności
        wartościDoWstawienia = new String[] {"0", "S", "0", Język_Polski.BD_kategorieWażności_opis_S};
        polecenie = wstawWartości(Opisy.tab_KategorieWażności, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"1", "A", "0", Język_Polski.BD_kategorieWażności_opis_A};
        polecenie = wstawWartości(Opisy.tab_KategorieWażności, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"2", "B", "0", Język_Polski.BD_kategorieWażności_opis_B};
        polecenie = wstawWartości(Opisy.tab_KategorieWażności, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"3", "C", "0", Język_Polski.BD_kategorieWażności_opis_C};
        polecenie = wstawWartości(Opisy.tab_KategorieWażności, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"4", "D", "0", Język_Polski.BD_kategorieWażności_opis_D};
        polecenie = wstawWartości(Opisy.tab_KategorieWażności, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

        wartościDoWstawienia = new String[] {"5", "E", "0", Język_Polski.BD_kategorieWażności_opis_E};
        polecenie = wstawWartości(Opisy.tab_KategorieWażności, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);


        //Scenariusze Wydarzeń
        wartościDoWstawienia = new String[] {Język_Polski.BD_scenariusze_nazwa_rzeczywisty, "0", "0", ""};
        polecenie = wstawWartości(Opisy.tab_ScenariuszeWydarzeń, wartościDoWstawienia);
        połączenieZbazą.baza.execSQL(polecenie);

    }



    public static String stwórzTabele(String nazwaTabeli, String[] nazwyPólZtypami, String[] nazwyPólPK, String[] nazwyPólŹródłaFK, String[] nazwyCeluFK)
    {
        String polecenie;

        polecenie = "CREATE TABLE " + nazwaTabeli + " (";

        polecenie += nazwyPólZtypami[0] + " ";

        for (int i = 1; i < nazwyPólZtypami.length; i++) {
            polecenie += ", " + nazwyPólZtypami[i];
        }

        if(nazwyPólPK!=null)
        {
            polecenie+=", PRIMARY KEY (" + nazwyPólPK[0];
            for (int i = 1; i < nazwyPólPK.length; i++) {
                polecenie += ", " + nazwyPólPK[i];
            }
            polecenie+=") ";
        }

        if(nazwyPólŹródłaFK!=null)
        {
            polecenie+=", FOREIGN KEY (" + nazwyPólŹródłaFK[0] + ") REFERENCES " + nazwyCeluFK[0];
            for (int i = 1; i < nazwyPólŹródłaFK.length; i++) {
                polecenie+=", FOREIGN KEY (" + nazwyPólŹródłaFK[i] + ") REFERENCES " + nazwyCeluFK[i];
            }
        }

        polecenie += ") ";

        return polecenie;
    }



    public static String wczytajCałąTabele(String nazwaTabeli)
    {
        String polecenie;
        polecenie = "SELECT * FROM " + nazwaTabeli + ";";
        return polecenie;
    }



    public static String wczytajCałąTabele(String nazwaTabeli, String warunek)
    {
        String polecenie;
        polecenie = "SELECT * FROM " + nazwaTabeli + " WHERE " + warunek + ";";
        return polecenie;
    }



    public static String wczytajNPól(String nazwaTabeli, String[] nazwyPól)
    {
        String polecenie;
        polecenie = "SELECT ";

        polecenie += nazwyPól[0];
        for (int i = 1; i < nazwyPól.length; i++)
        {
            polecenie += (", " + nazwyPól[i]);
        }

        polecenie+= " FROM " + nazwaTabeli + ";";
        return polecenie;
    }



    public static String aktualizujNPól(String nazwaTabeli, String[] nazwyPól, String[] noweWartości, String warunek)
    {
        String polecenie;
        polecenie = "UPDATE " + nazwaTabeli + " SET " + nazwyPól[0] + "='" + noweWartości[0] + "'";
        for (int i = 1; i < nazwyPól.length; i++)
        {
            polecenie += ", " + nazwyPól[i] + "='" + noweWartości[i] + "'";
        }
        polecenie += " WHERE " + warunek + ";";
        return polecenie;
    }


    public static String aktualizujWszystkieRekordy(String nazwaTabeli, String nazwaPola, String nowaWartość)
    {
        String polecenie;
        polecenie = "UPDATE " + nazwaTabeli + " SET " + nazwaPola + "='" + nowaWartość + "';";
        return polecenie;
    }


    public static String usuńRekordy(String nazwaTabeli, String[] nazwyPól, String[] wartości) {
        String polecenie;
        polecenie = "DELETE FROM " + nazwaTabeli + " WHERE " + nazwyPól[0] + "='" + wartości[0] + "'";
        for (int i = 1; i < nazwyPól.length; i++) {
            polecenie += " AND " + nazwyPól[i] + "='" + wartości[i] + "'";
        }
        polecenie += ";";
        return polecenie;
    }


    public static String wstawWartości(String nazwaTabeli, String[] wartości)
    {
        String polecenie;
        polecenie = "INSERT INTO " + nazwaTabeli + " VALUES(" + "'" + wartości[0] + "'";
        for (int i = 1; i < wartości.length; i++)
        {
            polecenie += ", '" + wartości[i] + "'";
        }
        polecenie += ")";

        return polecenie;
    }



    public static String włączObsługeFK()
    {
        String polecenie;
        polecenie = "PRAGMA foreign_keys = ON;";
        return polecenie;
    }

}
