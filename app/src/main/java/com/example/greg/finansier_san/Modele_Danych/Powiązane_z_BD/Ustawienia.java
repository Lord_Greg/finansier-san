package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Waluty;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-29.
 */
public class Ustawienia {
    private boolean czyWyświetlaćPrzychody, czyPowiadomieniaWłączone;
    private Waluty waluta;
    private String język;

    public Ustawienia(){}

    public Ustawienia(String czyWyświetlaćPrzychody, String nazwaWaluty, String symbolWaluty, String język, String czyPowiadomieniaWłączone){
        if(czyWyświetlaćPrzychody.equals("0")) {
            this.czyWyświetlaćPrzychody = false;
        }
        else
        {
            this.czyWyświetlaćPrzychody=true;
        }
        waluta = new Waluty(nazwaWaluty, symbolWaluty);
        this.język=język;

        if(czyPowiadomieniaWłączone.equals("0")){
            this.czyPowiadomieniaWłączone=false;
        }
        else
        {
            this.czyPowiadomieniaWłączone=true;
        }
    }


    //region Gettery/Settery
    public boolean getCzyWyświetlaćPrzychody() {
        return czyWyświetlaćPrzychody;
    }

    public void setCzyWyświetlaćPrzychody(boolean czyWyświetlaćPrzychody) {
        this.czyWyświetlaćPrzychody = czyWyświetlaćPrzychody;
    }


    public boolean getCzyPowiadomieniaWłączone() {
        return czyPowiadomieniaWłączone;
    }

    public void setCzyPowiadomieniaWłączone(boolean czyPowiadomieniaWłączone) {
        this.czyPowiadomieniaWłączone = czyPowiadomieniaWłączone;
    }


    public Waluty getWaluta() {
        return waluta;
    }

    public void setWaluta(Waluty waluta) {
        this.waluta = waluta;
    }


    public String getJęzyk() {
        return język;
    }

    public void setJęzyk(String język) {
        this.język = język;
    }

    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Ustawienia), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.ustawienia = new Ustawienia(kursor.getString(0), kursor.getString(1), kursor.getString(2), kursor.getString(3), kursor.getString(4));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] pola = {
                Opisy.ustawienia_czyWyświetlaćPrzychody,
                Opisy.ustawienia_nazwaWaluty,
                Opisy.ustawienia_symbolWaluty,
                Opisy.ustawienia_język,
                Opisy.ustawienia_czyPowiadomieniaWłączone
        };

        String[] wartości = {
                Metody_Pomocnicze.boolNaStringa0lub1(czyWyświetlaćPrzychody),
                waluta.getNazwaWaluty(),
                waluta.getSymbolWaluty(),
                język,
                Metody_Pomocnicze.boolNaStringa0lub1(czyPowiadomieniaWłączone)
        };

        for (int i=0; i<pola.length; i++) {
            połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujWszystkieRekordy(Opisy.tab_Ustawienia, pola[i], wartości[i]));
        }

        połączenieBD.rozłączBD();
    }
}
