package com.example.greg.finansier_san.Ekrany;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Grup_Zdarzen;
import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Jednostek;
import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Kategorii_Zdarzen;
import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 26.01.16.
 */
public class Ekran_Grup_Danej_Kategorii extends AppCompatActivity {

    ListView lvwGrupyZdarzeń;
    List<Grupy_Zdarzeń> listaGrupZdarzeń;
    boolean czyPierwszy;
    String nazwaKategorii;
    int rozważanaPozycja;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_grup_danej_kategorii);

        Bundle odebraneDane = getIntent().getExtras();
        if(!odebraneDane.equals(null)){//zawsze prawda
            nazwaKategorii=odebraneDane.getString("nazwaKategorii");
        }

        //region Inicjalizacja kontrolek
        lvwGrupyZdarzeń = (ListView)findViewById(R.id.lvwGrupyZdarzen);
        TextView txvNagłówek = (TextView)findViewById(R.id.txvNazwaKategorii);

        czyPierwszy=true;

        txvNagłówek.setText(nazwaKategorii);
        przeładujGrupy();

        //endregion

        lvwGrupyZdarzeń.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(Ekran_Grup_Danej_Kategorii.this, Ekran_Zdarzen_Danej_Grupy.class);
                i.putExtra("nazwaGrupy", lvwGrupyZdarzeń.getItemAtPosition(position).toString());
                startActivity(i);
            }
        });


        lvwGrupyZdarzeń.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                rozważanaPozycja = position;

                registerForContextMenu(view);

                return false;
            }
        });

    }


    @Override
    protected void onStart(){
        super.onStart();

        if(czyPierwszy){
            czyPierwszy=false;
        }
        else{
            przeładujGrupy();
        }
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle(Wyświetlanie_Tekstu.gui_Nagłówek_zdarzenia());
        menu.add(0, v.getId(), 0, Wyświetlanie_Tekstu.czynnościEdytuj());
        menu.add(0, v.getId(), 0, Wyświetlanie_Tekstu.czynnościUsuń());
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        if (item.getTitle().toString().equals(Wyświetlanie_Tekstu.czynnościUsuń())) {
            usuńElement(rozważanaPozycja);
        } else {
            if (item.getTitle().toString().equals(Wyświetlanie_Tekstu.czynnościEdytuj())) {
                edytujElement(rozważanaPozycja);
            } else {
                return false;
            }
        }
        return true;
    }



    private void przeładujGrupy(){
        listaGrupZdarzeń = new ArrayList<Grupy_Zdarzeń>();
        List<String> daneDoAdaptera = new ArrayList<String>();

        for (Grupy_Zdarzeń grupa : Zmienne_Globalne.listaGrupZdarzeń) {
            if(grupa.getNazwaKategoriiZdarzenia_FK().equals(nazwaKategorii)){
                listaGrupZdarzeń.add(grupa);
                daneDoAdaptera.add(grupa.getNazwaGrupyZdarzeń_PK());
            }
        }

        ArrayAdapter adapter = new ArrayAdapter(Ekran_Grup_Danej_Kategorii.this, android.R.layout.simple_list_item_1, daneDoAdaptera);
        lvwGrupyZdarzeń.setAdapter(adapter);
    }

    private void usuńElement(final int pozycjaElementu) {
        new AlertDialog.Builder(Ekran_Grup_Danej_Kategorii.this)
                .setTitle(Wyświetlanie_Tekstu.czynnościUsuń())
                .setMessage(Wyświetlanie_Tekstu.guiCzyNapewnoUsunąć())
                .setPositiveButton(Wyświetlanie_Tekstu.guiTak(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Usuń
                        final Grupy_Zdarzeń usuwanaGrupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(lvwGrupyZdarzeń.getItemAtPosition(pozycjaElementu).toString());

                        if (Metody_Pomocnicze.czyGrupaMaWystąpienia(usuwanaGrupa.getNazwaGrupyZdarzeń_PK())) {
                            new AlertDialog.Builder(Ekran_Grup_Danej_Kategorii.this)
                                    .setTitle(Wyświetlanie_Tekstu.czynnościUsuń())
                                    .setMessage(Wyświetlanie_Tekstu.guiGrupaMaZdarzeniaCzyNapewnoUsunąć())
                                    .setPositiveButton(Wyświetlanie_Tekstu.guiTak(), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                            try {
                                                Łącznik połączenieBD = new Łącznik();
                                                połączenieBD.połączBD(Ekran_Grup_Danej_Kategorii.this);

                                                usuwanaGrupa.usuńZbazy(połączenieBD, usuwanaGrupa.getNazwaGrupyZdarzeń_PK());

                                                połączenieBD.rozłączBD();


                                                Zmienne_Globalne.listaGrupZdarzeń.remove(pozycjaElementu);

                                                Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Grup_Danej_Kategorii.this);
                                                Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Grup_Danej_Kategorii.this);
                                                Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Grup_Danej_Kategorii.this);
                                                Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Grup_Danej_Kategorii.this);

                                                przeładujGrupy();

                                                Metody_Pomocnicze.tostDługi(Ekran_Grup_Danej_Kategorii.this, Wyświetlanie_Tekstu.guiUsunięto());
                                            } catch (Exception e) {
                                                Metody_Pomocnicze.tostDługi(Ekran_Grup_Danej_Kategorii.this, Wyświetlanie_Tekstu.błądUsuwania());
                                            }

                                        }
                                    })
                                    .setNegativeButton(Wyświetlanie_Tekstu.guiNie(), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Nie usuwaj
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        } else {

                            try {
                                Łącznik połączenieBD = new Łącznik();
                                połączenieBD.połączBD(Ekran_Grup_Danej_Kategorii.this);

                                usuwanaGrupa.usuńZbazy(połączenieBD, usuwanaGrupa.getNazwaGrupyZdarzeń_PK());

                                połączenieBD.rozłączBD();


                                Zmienne_Globalne.listaGrupZdarzeń.remove(pozycjaElementu);

                                Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Grup_Danej_Kategorii.this);
                                Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Grup_Danej_Kategorii.this);
                                Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Grup_Danej_Kategorii.this);
                                Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Grup_Danej_Kategorii.this);

                                przeładujGrupy();

                                Metody_Pomocnicze.tostDługi(Ekran_Grup_Danej_Kategorii.this, Wyświetlanie_Tekstu.guiUsunięto());
                            } catch (Exception e) {
                                Metody_Pomocnicze.tostDługi(Ekran_Grup_Danej_Kategorii.this, Wyświetlanie_Tekstu.błądUsuwania());
                            }
                        }


                    }
                })
                .setNegativeButton(Wyświetlanie_Tekstu.guiNie(), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // Nie usuwaj
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void edytujElement(int pozycjaElementu) {
            Grupy_Zdarzeń edytowanaGrupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(lvwGrupyZdarzeń.getItemAtPosition(pozycjaElementu).toString());

            final Dialog_Wprowadzania_Grup_Zdarzen dlgZdarzenia = new Dialog_Wprowadzania_Grup_Zdarzen(Ekran_Grup_Danej_Kategorii.this, edytowanaGrupa);
            dlgZdarzenia.działajBezOk(Ekran_Grup_Danej_Kategorii.this);

            dlgZdarzenia.btnDodaj.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dlgZdarzenia.działaniaKończąceStandardowe(Ekran_Grup_Danej_Kategorii.this);

                    Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Grup_Danej_Kategorii.this);
                    Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Grup_Danej_Kategorii.this);
                    Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Grup_Danej_Kategorii.this);
                    Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Grup_Danej_Kategorii.this);

                    przeładujGrupy();

                    dlgZdarzenia.dialog.hide();

                }
            });

    }

}