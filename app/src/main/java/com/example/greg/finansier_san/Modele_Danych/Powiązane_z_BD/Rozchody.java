package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.Date;

/**
 * Created by Greg on 2015-11-29.
 */
public class Rozchody extends Zdarzenia_Finansowe {
    private int poziomWażności_FK;
    private boolean czyZapłacone, czyPosiadane;
    private String nazwaPrenumeraty_FK;
    private int nrPrenumeraty_FK;


    public Rozchody(String nazwaGrupy, int nrWystąpienia, double kwota, Date data, String opis, String czyPowiadomieniaWłączone, int ważność, String czyZapłacone, String czyPosiadane, String nazwaPrenumeraty_FK, int nrPrenumeraty){
        super(nazwaGrupy,nrWystąpienia,kwota,data,opis,czyPowiadomieniaWłączone);

        poziomWażności_FK = ważność;

        if(czyZapłacone.equals("0")){
            this.czyZapłacone=false;
        }
        else {
            this.czyZapłacone=true;
        }

        if(czyPosiadane.equals("0")){
            this.czyPosiadane=false;
        }
        else {
            this.czyPosiadane=true;
        }

        this.nazwaPrenumeraty_FK = nazwaPrenumeraty_FK;
        nrPrenumeraty_FK=nrPrenumeraty;
    }

    public Rozchody(String nazwaGrupy, int nrWystąpienia, double kwota, Date data, String opis, boolean czyPowiadomieniaWłączone, int ważność, boolean czyZapłacone, boolean czyPosiadane, String nazwaPrenumeraty_FK, int nrPrenumeraty){
        super(nazwaGrupy,nrWystąpienia,kwota,data,opis,czyPowiadomieniaWłączone);

        poziomWażności_FK = ważność;
        this.czyZapłacone=czyZapłacone;
        this.czyPosiadane=czyPosiadane;

        this.nazwaPrenumeraty_FK = nazwaPrenumeraty_FK;
        nrPrenumeraty_FK=nrPrenumeraty;
    }

    public Rozchody() {}


    //region Gettery/Settery
    public int getPoziomWażności_FK() {
        return poziomWażności_FK;
    }

    public void setPoziomWażności_FK(int poziomWażności_FK) {
        this.poziomWażności_FK = poziomWażności_FK;
    }


    public boolean getCzyZapłacone() {
        return czyZapłacone;
    }

    public void setCzyZapłacone(boolean czyZapłacone) {
        this.czyZapłacone = czyZapłacone;
    }


    public boolean getCzyPosiadane() {
        return czyPosiadane;
    }

    public void setCzyPosiadane(boolean czyPosiadane) {
        this.czyPosiadane = czyPosiadane;
    }


    public String getNazwaPrenumeraty_FK() {
        return nazwaPrenumeraty_FK;
    }

    public void setNazwaPrenumeraty_FK(String nazwaPrenumeraty_FK) {
        this.nazwaPrenumeraty_FK = nazwaPrenumeraty_FK;
    }


    public int getNrPrenumeraty_FK() {
        return nrPrenumeraty_FK;
    }

    public void setNrPrenumeraty_FK(int nrPrenumeraty_FK) {
        this.nrPrenumeraty_FK = nrPrenumeraty_FK;
    }
    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        String warunek = Opisy.zdarzeniaFinansowe_nazwaGrupyPKfk+"="+Opisy.rozchody_nazwaZdarzeniaPKfk+" AND "+Opisy.zdarzeniaFinansowe_nrWystąpieniaPK+"="+Opisy.rozchody_nrWystąpieniaPKfk;
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_ZdarzeniaFinansowe + ", " + Opisy.tab_Rozchody, warunek), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaRozchodów.add(new Rozchody(kursor.getString(0), kursor.getInt(1), kursor.getDouble(2), new Date(kursor.getInt(5), kursor.getInt(4), kursor.getInt(3)), kursor.getString(6), kursor.getString(7), kursor.getInt(10), kursor.getString(11), kursor.getString(12), kursor.getString(13), kursor.getInt(14) ));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        //zapisz zdarzenie
        super.zapiszNowyDoBazy(kontekst, połączenieBD);

        String[] wartości = {
                nazwaGrupy_PK_FK,
                String.valueOf(numerWystąpienia_PK),
                String.valueOf(poziomWażności_FK),
                Metody_Pomocnicze.boolNaStringa0lub1(czyZapłacone),
                Metody_Pomocnicze.boolNaStringa0lub1(czyPosiadane),
                nazwaPrenumeraty_FK,
                String.valueOf(nrPrenumeraty_FK)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_Rozchody, wartości));

        połączenieBD.rozłączBD();
    }

    public void aktualizujDoBazy(Łącznik połączenieBD, String staraNazwaGrupy){
        super.aktualizujDoBazy(połączenieBD, staraNazwaGrupy);

        if (!staraNazwaGrupy.equals(nazwaGrupy_PK_FK)){
            String[] nazwyPól = {
                    Opisy.rozchody_nazwaZdarzeniaPKfk
            };

            String[] wartości = {
                    nazwaGrupy_PK_FK
            };

            String warunek = Opisy.rozchody_nazwaZdarzeniaPKfk + "='" + staraNazwaGrupy + "'";

            połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Rozchody, nazwyPól, wartości, warunek));
        }

        String[] nazwyPól = {
                Opisy.rozchody_nrWystąpieniaPKfk,
                Opisy.rozchody_poziomWażnościFK,
                Opisy.rozchody_czyZapłacone,
                Opisy.rozchody_czyPosiadane,
                Opisy.rozchody_nazwaPrenumeratyFK,
                Opisy.rozchody_nrPrenumeratyFK
        };

        String[] wartości = {
                String.valueOf(numerWystąpienia_PK),
                String.valueOf(poziomWażności_FK),
                Metody_Pomocnicze.boolNaStringa0lub1(czyZapłacone),
                Metody_Pomocnicze.boolNaStringa0lub1(czyPosiadane),
                nazwaPrenumeraty_FK,
                String.valueOf(nrPrenumeraty_FK)
        };

        String warunek = Opisy.rozchody_nazwaZdarzeniaPKfk + "='" + nazwaGrupy_PK_FK + "' AND " + Opisy.rozchody_nrWystąpieniaPKfk + "='" + String.valueOf(numerWystąpienia_PK) + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Rozchody, nazwyPól, wartości, warunek));
    }

    public void aktualizujStatusPosiadaniaDoBD(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        String[] nazwyPól = {
                Opisy.rozchody_czyPosiadane
        };

        String[] wartości = {
                Metody_Pomocnicze.boolNaStringa0lub1(czyPosiadane)
        };

        String warunek = Opisy.rozchody_nazwaZdarzeniaPKfk + "='" + nazwaGrupy_PK_FK + "' AND " + Opisy.rozchody_nrWystąpieniaPKfk + "=" + String.valueOf(numerWystąpienia_PK);

        połączenieBD.połączBD(kontekst);
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Rozchody, nazwyPól, wartości, warunek));
        połączenieBD.rozłączBD();
    }

    public void aktualizujStatusPosiadaniaDoBD(Łącznik połączenieBD){
        String[] nazwyPól = {
                Opisy.rozchody_czyPosiadane
        };

        String[] wartości = {
                Metody_Pomocnicze.boolNaStringa0lub1(czyPosiadane)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_Rozchody, nazwyPól, wartości));
    }

    public void aktualizujDatęDoBazy(Łącznik połączenieBD) {
        super.aktualizujDatęDoBazy(połączenieBD);
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń, int numerWystąpienia, String nazwaScenariusza){
        String[] nazwyPól = {
                Opisy.rozchody_nazwaZdarzeniaPKfk,
                Opisy.rozchody_nrWystąpieniaPKfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń,
                String.valueOf(numerWystąpienia)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_Rozchody, nazwyPól, wartości));

        Zdarzenia_Finansowe.usuńZbazy(połączenieBD, nazwaGrupyZdarzeń, numerWystąpienia, nazwaScenariusza);
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń){
        String[] nazwyPól = {
                Opisy.rozchody_nazwaZdarzeniaPKfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_Rozchody, nazwyPól, wartości));
    }

}
