package com.example.greg.finansier_san.Teksty_Interfejsu;

import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-28.
 */
public class Wyświetlanie_Tekstu {
    private static Język_Polski teksty = new Język_Polski();

    //region Miesiące
    public static String miesiącStyczeń(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącStyczeń; else return ""; }
    public static String miesiącLuty(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącLuty; else return ""; }
    public static String miesiącMarzec(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącMarzec; else return ""; }
    public static String miesiącKwiecień(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącKwiecień; else return ""; }
    public static String miesiącMaj(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącMaj; else return ""; }
    public static String miesiącCzerwiec(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącCzerwiec; else return ""; }
    public static String miesiącLipiec(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącLipiec; else return ""; }
    public static String miesiącSierpień(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącSierpień; else return ""; }
    public static String miesiącWrzesień(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącWrzesień; else return ""; }
    public static String miesiącPaździernik(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącPaździernik; else return ""; }
    public static String miesiącListopad(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącListopad; else return ""; }
    public static String miesiącGrudzień(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.miesiącGrudzień; else return ""; }
    //endregion


    //region Dni Tygodnia
    public static String dzieńSkrótPoniedziałek(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_poniedziałek; else return ""; }
    public static String dzieńSkrótWtorek(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_wtorek; else return ""; }
    public static String dzieńSkrótŚroda(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_środa; else return ""; }
    public static String dzieńSkrótCzwartek(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_czwartek; else return ""; }
    public static String dzieńSkrótPiątek(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_piątek; else return ""; }
    public static String dzieńSkrótSobota(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_sobota; else return ""; }
    public static String dzieńSkrótNiedziela() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.dzień_skrót_niedziela; else return ""; }
    //endregion


    //region Błędy
    public static String błądData(){ if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_data; else return ""; }
    public static String błądNazwaJużIstnieje() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_nazwaJużIstnieje; else return ""; }
    public static String błądPołączeniaZbazą() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_połączeniaZbazą; else return ""; }
    public static String błądWprowadzonychDanych() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_wprowadzonychDanych; else return ""; }
    public static String błądJednostkaNieDodana() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_jednostkaNieDodana; else return ""; }
    public static String błądPodajNazwęJednostki() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_podajNazwęJednostki; else return ""; }
    public static String błądPodajNazwęKategoriiZdarzeń() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_podajNazwęKategoriiZdarzeń; else return ""; }
    public static String błądKategoriaJużIstnieje() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_kategoriaJużIstnieje; else return ""; }
    public static String błądKategoriaNieDodana() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_kategoriaNieZostałaDodana; else return ""; }
    public static String błądUsuwania() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_usuwania; else return ""; }
    public static String błądZapisu() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_zapisu; else return ""; }
    public static String błądPodajNazwęZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_podajNazwęZdarzenia; else return ""; }
    public static String błądZdarzenieJużIstnieje() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_zdarzenieJużIstnieje; else return ""; }
    public static String błądOdczytuZbd() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_odczytuZbd; else return ""; }
    public static String błądPodajKwotę() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_podajKwotę; else return ""; }
    public static String błądPodajPoczątkoweWystąpienie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_podajPoczątkoweWystąpienie; else return ""; }
    public static String błądPodajIlośćWystąpień() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_podajIlośćWystąpień; else return ""; }
    public static String błądWystąpienieOnumerzeJużIstnieje1() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_wystąpienieOnumerzeJużIstnieje1; else return ""; }
    public static String błądWystąpienieOnumerzeJużIstnieje2() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.błąd_wystąpienieOnumerzeJużIstnieje2; else return ""; }
    //endregion


    //region Powtarzalności
    public static String powtarzalnościDni() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.powtarzalności_dni; else return ""; }
    public static String powtarzalnościTygodnie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.powtarzalności_tygodnie; else return ""; }
    public static String powtarzalnościMiesiące() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.powtarzalności_miesiące; else return ""; }
    public static String powtarzalnościLat() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.powtarzalności_lat; else return ""; }
    //endregion


    //region Teksty Zakładek
    public static String zakładkaKalendarz() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.zakładka_kalendarz; else return ""; }
    public static String zakładkaLista() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.zakładka_lista; else return ""; }
    //endregion


    //region Interfejs Użytkownika
    public static String guiRozchód() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_rozchód; else return ""; };
    public static String guiPrzychód() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_przychód; else return ""; };
    public static String guiTak() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_tak; else return ""; }
    public static String guiNie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nie; else return ""; }
    public static String guiCzyNapewnoUsunąć() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_czyNapewnoUsunąć; else return ""; }
    public static String guiUsuńZdarzenie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_usuńZdarzenie; else return ""; }
    public static String guiZdarzenieZostałoUsunięte() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_zdarzenieZostałoUsunięte; else return ""; }
    public static String guiJednostkaZostałaDodana() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_jednostkaZostałaDodana; else return ""; }
    public static String guiKategoriaDodana() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_kategoriaZostałaDodana; else return ""; }
    public static String guiUsunięto() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_usunięto; else return ""; }
    public static String guiZapisano() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_zapisano; else return ""; }
    public static String guiGrupaMaZdarzeniaCzyNapewnoUsunąć() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_grupaMaZdarzeniaCzyNapewnoUsunąć; else return ""; }
    public static String guiDodajNowe() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_dodajNowe; else return ""; }
    public static String guiPojedynczeZdarzenie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_pojedynczeZdarzenie; else return ""; }
    public static String guiSeriaZdarzeń() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_seriaZdarzeń; else return ""; }
    public static String guiSymbolRozchodu() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_symbolRozchodu; else return ""; }
    public static String guiSymbolPrzychodu() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_symbolPrzychodu; else return ""; }
    public static String guiPowiadomienieRozchodów() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_powiadomienieRozchodów; else return ""; }
    public static String guiPowiadomieniePrzycodów() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_powiadomieniePrzycodów; else return ""; }
    public static String guiPodajDatę() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_podajDatę; else return ""; }
    public static String guiPierwszego() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_pierwszego; else return ""; }
    public static String guiZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_zdarzenia; else return ""; }
    public static String guiZdarzeniaDodanoPomyślnie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_zdarzeniaDodanoPomyślnie; else return ""; }
    public static String guiEdytujZdarzenie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_edytujZdarzenie; else return ""; }
    public static String guiNumerEdytowanegoZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_numerEdytowanegoZdarzenia; else return ""; }
    public static String guiNoweZdarzenie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_noweZdarzenie; else return ""; }
    public static String guiPodajNumerZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_podajNumerZdarzenia; else return ""; }
    public static String guiZdarzenieDodanoPomyślnie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_zdarzenieDodanoPomyślnie; else return ""; }
    public static String guiCzyChceszPrzesunąćDaty() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_czyChceszPrzesunąćDaty; else return ""; }
    public static String guiWykonanoDlaZdarzeń1() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_wykonanoDlaZdarzeń1; else return ""; }
    public static String guiWykonanoDlaZdarzeń2() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_wykonanoDlaZdarzeń2; else return ""; }

    public static String gui_Nagłówek_dzień() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_dzień; else return ""; }
    public static String gui_Nagłówek_miesiąc() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_miesiąc; else return ""; }
    public static String gui_Nagłówek_rok() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_rok; else return ""; }
    public static String gui_Nagłówek_podajNazwęZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajNazwęZdarzenia; else return ""; }
    public static String gui_Nagłówek_nazwa() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_nazwa; else return ""; }
    public static String gui_Nagłówek_wybierzKategorię() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_wybierzKategorię; else return ""; }
    public static String gui_Nagłówek_wybierzJednostkę() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_wybierzJednostkę; else return ""; }
    public static String gui_Nagłówek_UkryjNrIjednostkę() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_ukryjNrIjednostkę; else return ""; }
    public static String gui_Nagłówek_podajNazwęJednostki() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajNazwęJednostki; else return ""; }
    public static String gui_Nagłówek_podajSkrótJednostki() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajSkrótJednostki; else return ""; }
    public static String gui_Nagłówek_skrót() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_skrót; else return ""; }
    public static String gui_Nagłówek_podajNazwęKategorii() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajNazwęKategorii; else return ""; }
    public static String gui_Nagłówek_podajOpis() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajOpis; else return ""; }
    public static String gui_Nagłówek_opis() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_opis; else return ""; }
    public static String gui_Nagłówek_listaZakupów() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_listaZakupów; else return ""; }
    public static String gui_Nagłówek_nadchodzące() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_nadchodzące; else return ""; }
    public static String gui_Nagłówek_opcje() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_opcje; else return ""; }
    public static String gui_Nagłówek_ukryjPrzychody() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_ukryjPrzychody; else return ""; }
    public static String gui_Nagłówek_wyłączPowiadomienia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_wyłączPowiadomienia; else return ""; }
    public static String gui_Nagłówek_zarządzajJednostkami() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zarządzajJednostkami; else return ""; }
    public static String gui_Nagłówek_zarządzajKategoriami() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zarządzajKategoriami; else return ""; }
    public static String gui_Nagłówek_zarządzajZdarzeniami() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zarządzajZdarzeniami; else return ""; }
    public static String gui_Nagłówek_ważność() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_ważność_; else return ""; }
    public static String gui_Nagłówek_kategoria() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_kategoria; else return ""; }
    public static String gui_Nagłówek_data() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_data_; else return ""; }
    public static String gui_Nagłówek_kwota_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_kwota_; else return ""; }
    public static String gui_Nagłówek_posiadane() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_posiadane; else return ""; }
    public static String gui_Nagłówek_ukryty() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_ukryty; else return ""; }
    public static String gui_Nagłówek_opis_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_opis_; else return ""; }
    public static String gui_Nagłówek_nowaSeriaZdarzeń() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_nowaSeriaZdarzeń; else return ""; }
    public static String gui_Nagłówek_wybierzNazwęZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_wybierzNazwęZdarzenia_; else return ""; }
    public static String gui_Nagłówek_podajLiczbęWystąpień_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajLiczbęWystąpień_; else return ""; }
    public static String gui_Nagłówek_podajKwotę_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajKwotę_; else return ""; }
    public static String gui_Nagłówek_podajDatęPierwszegoZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajDatęPierwszegoZdarzenia; else return ""; }
    public static String gui_Nagłówek_powtarzajCo_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_powtarzajCo_; else return ""; }
    public static String gui_Nagłówek_podajOpis_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajOpis_; else return ""; }
    public static String gui_Nagłówek_włączPowiadamienia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_włączPowiadamienia; else return ""; }
    public static String gui_Nagłówek_niePokazuj() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_niePokazuj; else return ""; }
    public static String gui_Nagłówek_wybierzKategorięWażności_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_wybierzKategorięWażności_; else return ""; }
    public static String gui_Nagłówek_kwota() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_kwota; else return ""; }
    public static String gui_Nagłówek_liczbaWystąpień() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_liczbaWystąpień; else return ""; }
    public static String gui_Nagłówek_zacznijOd() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zacznijOd; else return ""; }
    public static String gui_Nagłówek_ile() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_ile; else return ""; }
    public static String gui_Nagłówek_noweZdarzenie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_noweZdarzenie; else return ""; }
    public static String gui_Nagłówek_podajNumerWystąpienia_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajNumerWystąpienia_; else return ""; }
    public static String gui_Nagłówek_numerWystąpienia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_numerWystąpienia; else return ""; }
    public static String gui_Nagłówek_podajDatęZdarzenia_() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_podajDatęZdarzenia_; else return ""; }
    public static String gui_Nagłówek_zarządzanieJednostkami() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zarządzanieJednostkami; else return ""; }
    public static String gui_Nagłówek_zarządzanieKategoriami() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zarządzanieKategoriami; else return ""; }
    public static String gui_Nagłówek_zarządzanieZdarzeniami() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zarządzanieZdarzeniami; else return ""; }
    public static String gui_Nagłówek_dodajZdarzenie() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_dodajZdarzenie; else return ""; }
    public static String gui_Nagłówek_skrótWaluty() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_skrótWaluty_; else return ""; }
    public static String gui_Nagłówek_zdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_zdarzenia; else return ""; }


    public static String gui_nagłówek_ok() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_ok; else return ""; }
    public static String gui_nagłówek_anuluj() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.gui_nagłówek_anuluj; else return ""; }
    //endregion



    //region Tytuły Dialogów
    public static String tytułyNazwaDialoguJednostek() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.tytuł_nazwaDialoguJednostek; else return ""; }
    public static String tytułyNazwaDialoguKategoriiZdarzeń() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.tytuł_nazwaDialoguKategoriiZdarzeń; else return ""; }
    public static String tytułyNazwaDialoguGrupZdarzeń() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.tytuł_nazwaDialoguGrupZdarzeń; else return ""; }
    public static String tytułyNazwaDialoguPrzesuńZdarzenia() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.tytuł_nazwaDialoguPrzesuńZdarzenia; else return ""; }
    public static String tytułyNazwaDialoguZmianaDaty() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.tytuł_nazwaDialoguZmianaDaty; else return ""; }
    public static String tytułNazwaAlertDialoguZmianaDaty() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.tytuł_nazwaAlertDialoguZmianaDaty; else return ""; }
    //endregion


    //region Czynności
    public static String czynnościUsuń() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.czynności_usuń; else return ""; }
    public static String czynnościEdytuj() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.czynności_edytuj; else return ""; }
    public static String czynnościZapisz() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.czynności_zapisz; else return ""; }
    public static String czynnościDodaj() { if(Zmienne_Globalne.ustawienia.getJęzyk().equals("polski")) return teksty.czynności_dodaj; else return ""; }
    //endregion





    public static String kolorDomyślny() { return teksty.kolor_domyślny; }

    public static String BDkategorieZdarzeń_nazwa_nieprzypisane() { return teksty.BD_kategorieZdarzeń_nazwa_nieprzypisane; }

    public static String językAplikacjiPolski = "polski";
}
