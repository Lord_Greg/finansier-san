package com.example.greg.finansier_san;

import android.content.Context;
import android.widget.EditText;
import android.widget.Toast;

import com.example.greg.finansier_san.Modele_Danych.Bilanse_Miesiąca;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Ważności;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Scenariusze_Wydarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Ustawienia;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Zdarzenia_w_Scenariuszach;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Greg on 2015-12-02.
 */
public class Metody_Pomocnicze {

    //region Wczytywanie z BD
    public static void wczytajWszystkoZbazy(Context kontekst){
        Zmienne_Globalne.ustawienia = new Ustawienia();
        Ustawienia.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaJednostek = new ArrayList<Jednostki>();
        Jednostki.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaKategoriiZdarzeń = new ArrayList<Kategorie_Zdarzeń>();
        Kategorie_Zdarzeń.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaGrupZdarzeń = new ArrayList<Grupy_Zdarzeń>();
        Grupy_Zdarzeń.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaPrzychodów = new ArrayList<Przychody>();
        Przychody.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaKategoriiWażności = new ArrayList<Kategorie_Ważności>();
        Kategorie_Ważności.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaRozchodów = new ArrayList<Rozchody>();
        Rozchody.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaScenariuszyWydarzeń = new ArrayList<Scenariusze_Wydarzeń>();
        Scenariusze_Wydarzeń.wczytajWszystkoZbazy(kontekst);
        Zmienne_Globalne.listaZdarzeńWscenariuszach = new ArrayList<Zdarzenia_w_Scenariuszach>();
        Zdarzenia_w_Scenariuszach.wczytajWszystkoZbazy(kontekst);

        Zmienne_Globalne.listaBilansów = new ArrayList<Bilanse_Miesiąca>();
    }

    public static void przeładujUstawieniaZbazy(Context kontekst) {
        Zmienne_Globalne.ustawienia = new Ustawienia();
        Ustawienia.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujJednostkiZbazy(Context kontekst) {
        Zmienne_Globalne.listaJednostek = new ArrayList<Jednostki>();
        Jednostki.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujKategorieZdarzeńZbazy(Context kontekst) {
        Zmienne_Globalne.listaKategoriiZdarzeń = new ArrayList<Kategorie_Zdarzeń>();
        Kategorie_Zdarzeń.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujGrupyZdarzeńZbazy(Context kontekst) {
        Zmienne_Globalne.listaGrupZdarzeń = new ArrayList<Grupy_Zdarzeń>();
        Grupy_Zdarzeń.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujPrzychodyZbazy(Context kontekst) {
        Zmienne_Globalne.listaPrzychodów = new ArrayList<Przychody>();
        Przychody.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujKategorieWażnościZbazy(Context kontekst) {
        Zmienne_Globalne.listaKategoriiWażności = new ArrayList<Kategorie_Ważności>();
        Kategorie_Ważności.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujRozchodyZbazy(Context kontekst) {
        Zmienne_Globalne.listaRozchodów = new ArrayList<Rozchody>();
        Rozchody.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujScenariuszeWydarzeńZbazy(Context kontekst) {
        Zmienne_Globalne.listaScenariuszyWydarzeń = new ArrayList<Scenariusze_Wydarzeń>();
        Scenariusze_Wydarzeń.wczytajWszystkoZbazy(kontekst);
    }

    public static void przeładujZdarzeniaWscenariuszachZbazy(Context kontekst) {
        Zmienne_Globalne.listaZdarzeńWscenariuszach = new ArrayList<Zdarzenia_w_Scenariuszach>();
        Zdarzenia_w_Scenariuszach.wczytajWszystkoZbazy(kontekst);
    }
    //endregion


    //region Manipulacje Czasem
    public static String zwróćNazwęMiesiąca(int miesiąc){
        switch (miesiąc) {
        case 0: {
            return Wyświetlanie_Tekstu.miesiącStyczeń();
        }
        case 1: {
            return Wyświetlanie_Tekstu.miesiącLuty();
        }
        case 2: {
            return Wyświetlanie_Tekstu.miesiącMarzec();
        }
        case 3: {
            return Wyświetlanie_Tekstu.miesiącKwiecień();
        }
        case 4: {
            return Wyświetlanie_Tekstu.miesiącMaj();
        }
        case 5: {
            return Wyświetlanie_Tekstu.miesiącCzerwiec();
        }
        case 6: {
            return Wyświetlanie_Tekstu.miesiącLipiec();
        }
        case 7: {
            return Wyświetlanie_Tekstu.miesiącSierpień();
        }
        case 8: {
            return Wyświetlanie_Tekstu.miesiącWrzesień();
        }
        case 9: {
            return Wyświetlanie_Tekstu.miesiącPaździernik();
        }
        case 10: {
            return Wyświetlanie_Tekstu.miesiącListopad();
        }
        case 11: {
            return Wyświetlanie_Tekstu.miesiącGrudzień();
        }
        default: {
            return Wyświetlanie_Tekstu.błądData();   //Nigdy się nie wykona
        }
    }
}

    public static void wczytajNazwyMiesięcy(String[] tablicaDoWczytaniaMiesięcy){
        for(int i=0; i<12; i++)
        {
            tablicaDoWczytaniaMiesięcy[i] = zwróćNazwęMiesiąca(i);
        }
    }

    public static int wyznaczIlośćDniWmiesiącu(Date dziesiejszaData){
        int ilośćDni;

        switch (dziesiejszaData.getMonth())
        {
            case 0 : { ilośćDni=31; break; }
            case 1 : {
                if ((((dziesiejszaData.getYear()+ Zmienne_Globalne.y) % 4 == 0) && ((dziesiejszaData.getYear()+ Zmienne_Globalne.y) % 100 != 0)) || ((dziesiejszaData.getYear()+ Zmienne_Globalne.y) % 400 == 0)) {
                    ilośćDni = 29;
                } else {
                    ilośćDni = 28;
                }
                break;
            }
            case 2: { ilośćDni=31; break; }
            case 3: { ilośćDni=30; break; }
            case 4: { ilośćDni=31; break; }
            case 5: { ilośćDni=30; break; }
            case 6: { ilośćDni=31; break; }
            case 7: { ilośćDni=31; break; }
            case 8: { ilośćDni=30; break; }
            case 9: { ilośćDni=31; break; }
            case 10: { ilośćDni=30; break; }
            case 11: { ilośćDni=31; break; }
            default: { ilośćDni=30; break; }
        }

        return ilośćDni;
    }

    public static void zmieńMiesiąc(Date dataDoZamiany, int miesiącDoUstawienia, boolean czyZmieniaćRok){
        if(miesiącDoUstawienia>=0 && miesiącDoUstawienia<=11)
        {
            //rok sie nie zmienia
            int ileDniMaNowyMiesiąc = wyznaczIlośćDniWmiesiącu(new Date(dataDoZamiany.getYear(), miesiącDoUstawienia, 1));
            if(ileDniMaNowyMiesiąc < dataDoZamiany.getDate())
            {
                dataDoZamiany.setDate(ileDniMaNowyMiesiąc);
            }
            dataDoZamiany.setMonth(miesiącDoUstawienia);
        }
        else
        {
            if(miesiącDoUstawienia==-1)
            {
                dataDoZamiany.setMonth(11);
                if(czyZmieniaćRok && dataDoZamiany.getYear()+ Zmienne_Globalne.y>0)
                {
                    dataDoZamiany.setYear(dataDoZamiany.getYear()-1);
                }
            }
            if(miesiącDoUstawienia==12)
            {
                dataDoZamiany.setMonth(0);
                if(czyZmieniaćRok)
                {
                    dataDoZamiany.setYear(dataDoZamiany.getYear() + 1);
                }
            }
        }
    }

    public static void wczytajNazwyMiesięcy(List<String> listaDoWczytaniaMiesięcy){
        for(int i=0; i<12; i++)
        {
            listaDoWczytaniaMiesięcy.add(zwróćNazwęMiesiąca(i));
        }
    }

    public static void przepiszRok(EditText etxnRok, Date nowaData) {
        try {
            int sprawdzanaWartość = Integer.parseInt(etxnRok.getText().toString());

            if(sprawdzanaWartość<1)
            {
                etxnRok.setText("1");
            }

        }
        catch (Exception e) {
            etxnRok.setText(String.valueOf(Zmienne_Globalne.dzisiejszaData.getYear()+ Zmienne_Globalne.y));
        }
        nowaData.setYear(Integer.parseInt(etxnRok.getText().toString()) - Zmienne_Globalne.y);
    }

    public static void przepiszDzień(EditText etxnDzień, Date nowaData) {
        try {
            int sprawdzanaWartość = Integer.parseInt(etxnDzień.getText().toString());

            if (sprawdzanaWartość < 1) {
                etxnDzień.setText(String.valueOf("1"));
            }
            else {
                int ilośćDniWmiesiącu = wyznaczIlośćDniWmiesiącu(nowaData);

                if (sprawdzanaWartość > ilośćDniWmiesiącu) {
                    etxnDzień.setText(String.valueOf(ilośćDniWmiesiącu));
                }
            }
        } catch (Exception e) {
            etxnDzień.setText("1");
        }
        nowaData.setDate(Integer.parseInt(etxnDzień.getText().toString()));
    }

    public static void przepiszDate(Date dataŹródłowa, Date dataDocelowa) {
        dataDocelowa.setYear(dataŹródłowa.getYear());
        dataDocelowa.setMonth(dataŹródłowa.getMonth());
        dataDocelowa.setDate(dataŹródłowa.getDate());
    }

    public static void zwiększDateOdni(Date dataDocelowa, int plusIle){
        dataDocelowa.setDate(dataDocelowa.getDate() + plusIle);
    }

    public static void zwiększDateOtygodnie(Date dataDocelowa, int plusIle){
        zwiększDateOdni(dataDocelowa, 7 * plusIle);
    }

    public static void zwiększDateOmiesiące(Date dataDocelowa, int plusIle){
        Date dataPomocnicza = new Date(dataDocelowa.getYear(), dataDocelowa.getMonth()+plusIle, 1);
        if(dataDocelowa.getDate() > wyznaczIlośćDniWmiesiącu(dataPomocnicza))
        {
            dataDocelowa.setDate(wyznaczIlośćDniWmiesiącu(dataPomocnicza));
        }
        dataDocelowa.setMonth(dataDocelowa.getMonth() + plusIle);
    }

    public static void zwiększDateOlata(Date dataDocelowa, int plusIle){
        Date dataPomocnicza = new Date(dataDocelowa.getYear() + plusIle, dataDocelowa.getMonth(), 1);
        if(dataDocelowa.getDate() > wyznaczIlośćDniWmiesiącu(dataPomocnicza)) //sprawdzamy luty w roku przestępnym
        {
            dataDocelowa.setDate(wyznaczIlośćDniWmiesiącu(dataPomocnicza));
        }
        dataDocelowa.setYear(dataDocelowa.getYear() + plusIle);
    }

    //endregion


    //region Testowanie Wartości
    public static boolean czyGrupaOk(String nazwaNowejGrupy, String nazwaStarejGrupy) {
        if(nazwaStarejGrupy.equals("")) {
            for (Grupy_Zdarzeń grupa : Zmienne_Globalne.listaGrupZdarzeń) {
                if (grupa.getNazwaGrupyZdarzeń_PK().equals(nazwaNowejGrupy)) {
                    return false;
                }
            }
            return true;
        }
        else{
            for (Grupy_Zdarzeń grupa : Zmienne_Globalne.listaGrupZdarzeń) {
                if (!grupa.getNazwaGrupyZdarzeń_PK().equals(nazwaStarejGrupy) && grupa.getNazwaGrupyZdarzeń_PK().equals(nazwaNowejGrupy)) {
                    return false;
                }
            }
            return true;
        }
    }

    public static boolean czyDatySąTakieSame(Date data1, Date data2){
        if(data1.getDate() == data2.getDate() && data1.getMonth() == data2.getMonth() && data1.getYear() == data2.getYear()){
            return true;
        }
        else {
            return false;
        }
    }

    public static boolean czyNazwaKategoiiZdarzeńJestWolna(String nazwaKategorii){
        for (Kategorie_Zdarzeń kategoria:Zmienne_Globalne.listaKategoriiZdarzeń) {
            if(nazwaKategorii.equals(kategoria.getNazwa_PK())){
                return false;
            }
        }

        return true;
    }

    public static boolean czyNazwaKategoiiZdarzeńJestWolna(String nazwaKategorii, String staraNazwaKategorii){
        for (Kategorie_Zdarzeń kategoria:Zmienne_Globalne.listaKategoriiZdarzeń) {
            if(nazwaKategorii.equals(kategoria.getNazwa_PK()) && !nazwaKategorii.equals(staraNazwaKategorii)){
                return false;
            }
        }

        return true;
    }

    public static boolean czyGrupaMaWystąpienia(String nazwaGrupy){
        for (Rozchody rozchód:Zmienne_Globalne.listaRozchodów) {
            if(nazwaGrupy.equals(rozchód.getNazwaGrupy_PK_FK())){
                return true;
            }
        }
        for (Przychody przychód:Zmienne_Globalne.listaPrzychodów) {
            if(nazwaGrupy.equals(przychód.getNazwaGrupy_PK_FK())){
                return true;
            }
        }

        return false;
    }

    public static int czyWystąpienieIstnieje(String nazwaGrupy, int[] sprawdzaneWystąpienia){
        for (Przychody przychód:Zmienne_Globalne.listaPrzychodów) {
            if(przychód.getNazwaGrupy_PK_FK().equals(nazwaGrupy)){
                for(int i=0; i<sprawdzaneWystąpienia.length; i++){
                    if(sprawdzaneWystąpienia[i]==przychód.getNumerWystąpienia_PK()){
                        return przychód.getNumerWystąpienia_PK();
                    }
                }
            }
        }

        for (Rozchody rozchód:Zmienne_Globalne.listaRozchodów) {
            if(rozchód.getNazwaGrupy_PK_FK().equals(nazwaGrupy)){
                for(int i=0; i<sprawdzaneWystąpienia.length; i++){
                    if(sprawdzaneWystąpienia[i]==rozchód.getNumerWystąpienia_PK()){
                        return rozchód.getNumerWystąpienia_PK();
                    }
                }
            }
        }


        return -1;
    }

    public static boolean czyZdarzenieMaNastępcę(String nazwaGrupy, int numerWystąpienia){
        for (Rozchody rozchód:Zmienne_Globalne.listaRozchodów) {
            if(rozchód.getNazwaGrupy_PK_FK().equals(nazwaGrupy) && rozchód.getNumerWystąpienia_PK() > numerWystąpienia){
                return true;
            }
        }

        for (Przychody przychód:Zmienne_Globalne.listaPrzychodów) {
            if(przychód.getNazwaGrupy_PK_FK().equals(nazwaGrupy) && przychód.getNumerWystąpienia_PK() > numerWystąpienia){
                return true;
            }
        }

        return false;
    }
    //endregion


    //region Wyszukiwania z List
    public static Kategorie_Ważności znajdźKategorieWażności(int poziomWażności){
        for (Kategorie_Ważności element: Zmienne_Globalne.listaKategoriiWażności) {
            if(poziomWażności == element.getPoziomWażności_PK()){
                return element;
            }
        }
        return new Kategorie_Ważności(); //pilnować by nigdy się nie wykonało
    }

    public static Kategorie_Ważności znajdźKategorieWażności(String symbolWażności){
        for (Kategorie_Ważności element: Zmienne_Globalne.listaKategoriiWażności) {
            if(symbolWażności.equals(element.getSymbol())){
                return element;
            }
        }
        return new Kategorie_Ważności(); //pilnować by nigdy się nie wykonało
    }

    public static Jednostki znajdźJednostkę(int idJednostki){
        for (Jednostki element: Zmienne_Globalne.listaJednostek) {
            if(idJednostki == element.getId_PK()){
                return element;
            }
        }
        return new Jednostki(); //pilnować by nigdy się nie wykonało
    }

    public static Kategorie_Zdarzeń znajdźKategorieZdarzeń(String nazwaKategorii){
        for (Kategorie_Zdarzeń element: Zmienne_Globalne.listaKategoriiZdarzeń) {
            if(nazwaKategorii.equals(element.getNazwa_PK())){
                return element;
            }
        }
        return new Kategorie_Zdarzeń(); //pilnować by nigdy się nie wykonało
    }

    public static Grupy_Zdarzeń znajdźGrupęZdarzeń(String nazwaGrupy){
        for (Grupy_Zdarzeń element: Zmienne_Globalne.listaGrupZdarzeń) {
            if(nazwaGrupy.equals(element.getNazwaGrupyZdarzeń_PK())){
                return element;
            }
        }
        return new Grupy_Zdarzeń(); //pilnować by nigdy się nie wykonało
    }

    public static Rozchody znajdźRozchód(String nazwaGrupy, int numerWystąpienia){
        for (Rozchody element: Zmienne_Globalne.listaRozchodów) {
            if(nazwaGrupy.equals(element.getNazwaGrupy_PK_FK()) && numerWystąpienia == element.getNumerWystąpienia_PK()){
                return element;
            }
        }
        return new Rozchody(); //pilnować by nigdy się nie wykonało
    }

    public static Przychody znajdźPrzychód(String nazwaGrupy, int numerWystąpienia){
        for (Przychody element: Zmienne_Globalne.listaPrzychodów) {
            if(nazwaGrupy.equals(element.getNazwaGrupy_PK_FK()) && numerWystąpienia == element.getNumerWystąpienia_PK()){
                return element;
            }
        }
        return new Przychody(); //pilnować by nigdy się nie wykonało
    }

    public static int znajdźPozycjeJednostkiNaLiście(int idJednostki){
        for (Jednostki jednostka:Zmienne_Globalne.listaJednostek) {
            if(jednostka.getId_PK() == idJednostki){
                return Zmienne_Globalne.listaJednostek.indexOf(jednostka);
            }
        }

        return -1; //pilnować żeby się nie wykonało
    }
    //endregion


    //region Toasty
    public static void tostKrótki(Context kontekst, String tekstDoWyświetlenia) {
        Toast.makeText(kontekst, tekstDoWyświetlenia, Toast.LENGTH_SHORT).show();
    }

    public static void tostDługi(Context kontekst, String tekstDoWyświetlenia) {
        Toast.makeText(kontekst, tekstDoWyświetlenia, Toast.LENGTH_LONG).show();
    }
    //endregion


    public static void dopiszApostrofy(String[] wartościDoPrzekształcenia){
        for(int i = 0; i < wartościDoPrzekształcenia.length; i++)
        {
            String poprawionaWartość="'";
            poprawionaWartość+=wartościDoPrzekształcenia[i];
            poprawionaWartość+="'";
            wartościDoPrzekształcenia[i]=poprawionaWartość;
        }
    }

    public static String boolNaStringa0lub1(boolean wartość){
        if(wartość){
            return "1";
        }
        else {
            return "0";
        }
    }

    public static void dodajZdarzenieDoAktualnegoScenariusza(Context kontekst, Łącznik połączenieBD, String nazwaGrupy, int nrWystąpienia){
        Zdarzenia_w_Scenariuszach noweZdarzenie = new Zdarzenia_w_Scenariuszach(nazwaGrupy, nrWystąpienia, Zmienne_Globalne.aktualnyScenariusz);
        Zmienne_Globalne.listaZdarzeńWscenariuszach.add(noweZdarzenie);
        noweZdarzenie.zapiszDoBazy(kontekst);
    }

}
