package com.example.greg.finansier_san.Ekrany;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Greg on 2015-12-02.
 */
public class Ekran_Opcji extends AppCompatActivity {

    CheckBox chbCzyPowiadomieniaWłączone;
    CheckBox chbCzyPrzychodyUkryte;
    EditText etxWalutaSkrót;
    ListView lvwZarządzaj;

    boolean czyDokonanoZmiany;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_opcji);

        //region Inicjalizacja kontrolek
        chbCzyPowiadomieniaWłączone = (CheckBox)findViewById(R.id.chbWylaczPowiadomienia);
        chbCzyPrzychodyUkryte = (CheckBox)findViewById(R.id.chbUkryjPrzychody);
        etxWalutaSkrót = (EditText)findViewById(R.id.etxWalutaSkrot);
        lvwZarządzaj = (ListView)findViewById(R.id.lvwZarzadzaj);

        TextView txvNEkranOpcje = (TextView)findViewById(R.id.txvEkranOpcje);
        TextView txvNWalutaSkrót = (TextView)findViewById(R.id.txvWalutaSkrot);

        //region Ustawianie tekstu kontrolek
        txvNEkranOpcje.setText(Wyświetlanie_Tekstu.gui_Nagłówek_opcje());
        chbCzyPrzychodyUkryte.setText(Wyświetlanie_Tekstu.gui_Nagłówek_ukryjPrzychody());
        chbCzyPowiadomieniaWłączone.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wyłączPowiadomienia());
        txvNWalutaSkrót.setText(Wyświetlanie_Tekstu.gui_Nagłówek_skrótWaluty());
        //endregion

        chbCzyPowiadomieniaWłączone.setChecked(!Zmienne_Globalne.ustawienia.getCzyPowiadomieniaWłączone());
        chbCzyPrzychodyUkryte.setChecked(!Zmienne_Globalne.ustawienia.getCzyWyświetlaćPrzychody());

        etxWalutaSkrót.setText(Zmienne_Globalne.ustawienia.getWaluta().getSymbolWaluty());

        przeładujListViewZarządzania();

        //endregion

        lvwZarządzaj.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0: {
                        Intent i = new Intent(Ekran_Opcji.this, Ekran_Zarzadzania.class);
                        i.putExtra("coTo", "jednostki");
                        startActivity(i);
                        break;
                    }
                    case 1: {
                        Intent i = new Intent(Ekran_Opcji.this, Ekran_Zarzadzania.class);
                        i.putExtra("coTo", "kategorie");
                        startActivity(i);
                        break;
                    }
                    case 2: {
                        Intent i = new Intent(Ekran_Opcji.this, Ekran_Zarzadzania.class);
                        i.putExtra("coTo", "zdarzenia");
                        startActivity(i);
                        break;
                    }
                    default: {break;} // nigdy się nie wykona
                }
            }
        });

        chbCzyPrzychodyUkryte.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                czyDokonanoZmiany = true;
            }
        });

        chbCzyPowiadomieniaWłączone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                czyDokonanoZmiany = true;
            }
        });

    }


    protected void onPause(){
        super.onPause();
        if(czyDokonanoZmiany || !etxWalutaSkrót.getText().toString().equals(Zmienne_Globalne.ustawienia.getWaluta().getSymbolWaluty())){
            Zmienne_Globalne.ustawienia.setCzyPowiadomieniaWłączone(!chbCzyPowiadomieniaWłączone.isChecked());
            Zmienne_Globalne.ustawienia.setCzyWyświetlaćPrzychody(!chbCzyPrzychodyUkryte.isChecked());
            Zmienne_Globalne.ustawienia.getWaluta().setSymbolWaluty(etxWalutaSkrót.getText().toString());

            Zmienne_Globalne.ustawienia.zapiszDoBazy(Ekran_Opcji.this);

            czyDokonanoZmiany=false;
            //nie trzeba przeładowywać
        }
    }


    private void przeładujListViewZarządzania(){
        List<String> daneDoAdaptera = new ArrayList<String>();
        daneDoAdaptera.add(Wyświetlanie_Tekstu.gui_Nagłówek_zarządzajJednostkami());
        daneDoAdaptera.add(Wyświetlanie_Tekstu.gui_Nagłówek_zarządzajKategoriami());
        daneDoAdaptera.add(Wyświetlanie_Tekstu.gui_Nagłówek_zarządzajZdarzeniami());

        ArrayAdapter adapter = new ArrayAdapter(Ekran_Opcji.this, android.R.layout.simple_list_item_1, daneDoAdaptera);

        lvwZarządzaj.setAdapter(adapter);
    }

}
