package com.example.greg.finansier_san.Obsługa_BD;

/**
 * Created by Greg on 2015-12-01.
 */
public class Opisy {
    public static String
            nazwaBazyDanych = "finansier_baza",
            nazwaPlikuBazyDanych = nazwaBazyDanych + ".db",

            TYP_DANYCH_TEXT = "TEXT",
            TYP_DANYCH_REAL = "REAL",
            TYP_DANYCH_INT = "INT",
            TYP_DANYCH_BLOB = "BLOB",




            tab_Jednostki = "Jednostki",
            jednostki_idPK = "Jednostki_id", //PK
            jednostki_nazwa = "Jednostki_nazwa",
            jednostki_skrót = "Jednostki_skrot",

            tab_KategorieZdarzeń = "Kategorie_Zdarzen",
            kategorieZdarzeń_nazwaPK = "Kategorie_Zdarzen_nazwa", //PK
            kategorieZdarzeń_opis = "Kategorie_Zdarzen_opis",
            kategorieZdarzeń_kolor = "Kategorie_Zdarzen_kolor",

            tab_GrupyZdarzeń = "Grupy_Zdarzen",
            grupyZdarzen_nazwaPK = "Grupy_Zdarzen_nazwa_grupy", //PK
            grupyZdarzen_idJednostkiFK = "Grupy_Zdarzen_id_jednostki", //FK
            grupyZdarzen_nazwaKategoriiFK = "Grupy_Zdarzen_nazwa_kategorii", //FK
            grupyZdarzen_czyJawnaNumeracja = "Grupy_Zdarzen_czy_jawna_numeracja",

            tab_ZdarzeniaFinansowe = "Zdarzenia_Finansowe",
            zdarzeniaFinansowe_nazwaGrupyPKfk = "Zdarzenia_Finansowe_nazwa_grupy", //PK
            zdarzeniaFinansowe_nrWystąpieniaPK = "Zdarzenia_Finansowe_numer_wystapienia", //PK
            zdarzeniaFinansowe_kwota = "Zdarzenia_Finansowe_kwota",
            zdarzeniaFinansowe_dataDzień = "Zdarzenia_Finansowe_data_dzien",
            zdarzeniaFinansowe_dataMiesiac = "Zdarzenia_Finansowe_data_miesiac",
            zdarzeniaFinansowe_dataRok = "Zdarzenia_Finansowe_data_rok",
            zdarzeniaFinansowe_opis = "Zdarzenia_Finansowe_opis",
            zdarzeniaFinansowe_czyPowiadomieniaWłączone = "Zdarzenia_Finansowe_czy_powiadomienia_wlaczone",

            tab_Przychody = "Przychody",
            przychody_nazwaZdarzeniaPKfk = "Przychody_nazwa_zdarzenia", //PK FK
            przychody_nrWystąpieniaPKfk = "Przychody_numer_wystapienia", //PK FK
            przychody_czyWyświetlany = "Przychody_czy_wyswietlany",

            tab_Rozchody = "Rozchody",
            rozchody_nazwaZdarzeniaPKfk = "Rozchody_nazwa_zdarzenia", //PK FK
            rozchody_nrWystąpieniaPKfk = "Rozchody_numer_wystapienia", //PK FK
            rozchody_poziomWażnościFK = "Rozchody_poziom_waznosci", //FK
            rozchody_czyZapłacone = "Rozchody_czy_zaplacone",
            rozchody_czyPosiadane = "Rozchody_czy_posiadane",
            rozchody_nazwaPrenumeratyFK = "Rozchody_nazwa_prenumeraty", //FK
            rozchody_nrPrenumeratyFK = "Rozchody_numer_prenumeraty", //FK

            tab_KategorieWażności = "Kategorie_Waznosci",
            kategorieWażności_poziomWażnościPK = "Kategorie_Waznosci_poziom_waznosci", //PK
            kategorieWażności_symbol = "Kategorie_Waznosci_symbol",
            kategorieWażności_kwotaDozwolona = "Kategorie_Waznosci_kwota_dozwolona",
            kategorieWażności_opis = "Kategorie_Waznosci_opis",

            tab_Ustawienia = "Ustawienia",
            ustawienia_czyWyświetlaćPrzychody = "Ustawienia_czy_wyswietlac_przychody",
            ustawienia_nazwaWaluty = "Ustawienia_nazwa_waluty",
            ustawienia_symbolWaluty = "Ustawienia_symbol_waluty",
            ustawienia_język = "Ustawienia_jezyk",
            ustawienia_czyPowiadomieniaWłączone = "Ustawienia_czy_powiadomienia_wlaczone",

            tab_ZdarzeniaWscenariuszach = "Zdarzenia_W_Scenariuszach",
            zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk = "Zdarzenia_W_Scenariuszach_nazwa_zdarzenia", //PK FK
            zdarzeniaWscenariuszach_nrWystąpieniaPKfk = "Zdarzenia_W_Scenariuszach_numer_wystapienia", //PK FK
            zdarzeniaWscenariuszach_nazwaScenariuszaPKfk = "Zdarzenia_W_Scenariuszach_nazwa_scenariusza", //PK FK

            tab_ScenariuszeWydarzeń = "Scenariusze_Wydarzen",
            scenariuszeWydarzen_nazwaPK = "Scenariusze_Wydarzen_nazwa", //PK
            scenariuszeWydarzen_kwotaWportfelu = "Scenariusze_Wydarzen_kwota_w_portfelu",
            scenariuszeWydarzen_oszczędności = "Scenariusze_Wydarzen_oszczednosci",
            scenariuszeWydarzen_opis = "Scenariusze_Wydarzen_opis";


}
