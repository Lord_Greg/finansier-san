package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-26.
 */
public class Jednostki {
    private int id_PK;
    private String nazwa, skrót;

    public Jednostki(int id, String nazwa, String skrót){
        id_PK=id;
        this.nazwa=nazwa;
        this.skrót=skrót;
    }

    public Jednostki() {

    }


    //region Gettery/Settery

    public int getId_PK() {
        return id_PK;
    }

    public void setId_PK(int id_PK) {
        this.id_PK = id_PK;
    }


    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }


    public String getSkrót() {
        return skrót;
    }

    public void setSkrót(String skrót) {
        this.skrót = skrót;
    }
    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_Jednostki), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaJednostek.add(new Jednostki(kursor.getInt(0), kursor.getString(1), kursor.getString(2)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] wartości = {
                String.valueOf(id_PK),
                nazwa,
                skrót
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_Jednostki, wartości));

        połączenieBD.rozłączBD();
    }

    public void usuńZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);


        String[] nazwyPól = {
                Opisy.grupyZdarzen_idJednostkiFK
        };
        String[] wartości = {
                "0" // "nic []"
        };

        String warunek = Opisy.grupyZdarzen_idJednostkiFK + "='" + String.valueOf(id_PK) + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_GrupyZdarzeń, nazwyPól, wartości, warunek));


        nazwyPól = new String[]{
                Opisy.jednostki_idPK
        };
        wartości = new String[]{
                String.valueOf(id_PK)
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_Jednostki, nazwyPól, wartości));

        połączenieBD.rozłączBD();
    }

    public void aktualizujDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] nazwyPól = {
                Opisy.jednostki_nazwa,
                Opisy.jednostki_skrót
        };

        String[] wartości = {
                nazwa,
                skrót
        };

        String warunek = Opisy.jednostki_idPK + "='" + String.valueOf(id_PK) + "'";
        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_Jednostki, nazwyPól, wartości, warunek));

        połączenieBD.rozłączBD();
    }
}
