package com.example.greg.finansier_san.Dialogi;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-12-02.
 */
public class Dialog_Wprowadzania_Jednostek {

    public Dialog dialog;
    public int idNowejJednostki;

    EditText etxNazwaJednostki;
    EditText etxSkrótJednostki;
    public Button btnDodaj;
    Button btnAnuluj;
    Jednostki jednostka;
    boolean czyNowy;
    boolean czyOżywićDialog;
    Dialog dialogDoOżywienia;

    public Dialog_Wprowadzania_Jednostek(Jednostki jednostka) {
        this.jednostka=jednostka;
        czyNowy=false;
        czyOżywićDialog = false;
    }

    public Dialog_Wprowadzania_Jednostek() {
        jednostka = new Jednostki();
        czyNowy=true;
        czyOżywićDialog = false;
    }

    public Dialog_Wprowadzania_Jednostek(Dialog dialogDoOżywienia) {
        jednostka = new Jednostki();
        czyNowy=true;
        this.dialogDoOżywienia=dialogDoOżywienia;
        czyOżywićDialog = true;
    }

    public void działajBezOk(final Context kontekst) {
        dialog = new Dialog(kontekst);
        dialog.setTitle(Wyświetlanie_Tekstu.tytułyNazwaDialoguJednostek());
        dialog.setContentView(R.layout.dialog_wprowadzania_jednostek);
        dialog.show();

        //region Inicjalizacja kontrolek
        etxNazwaJednostki = (EditText) dialog.findViewById(R.id.etxNazwa);
        etxSkrótJednostki = (EditText) dialog.findViewById(R.id.etxSkrot);
        btnDodaj = (Button) dialog.findViewById(R.id.btnDodaj);
        btnAnuluj = (Button) dialog.findViewById(R.id.btnAnuluj);


        TextView txvNnazwa = (TextView)dialog.findViewById(R.id.txvNazwa);
        TextView txvNskrót = (TextView)dialog.findViewById(R.id.txvSkrot);

        //region Ustawianie tekstu kontrolek
        txvNnazwa.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajNazwęJednostki());
        etxNazwaJednostki.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_nazwa());
        txvNskrót.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajSkrótJednostki());
        etxSkrótJednostki.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_skrót());
        btnDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        btnAnuluj.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());

        if(!czyNowy){
            etxNazwaJednostki.setText(jednostka.getNazwa());
            etxSkrótJednostki.setText(jednostka.getSkrót());
        }
        //endregion

        //endregion


        btnAnuluj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                if(czyOżywićDialog)
                {
                    dialogDoOżywienia.show();
                }
            }
        });

    }


    public void działaniaKończąceZapiszNowąJednostkę(Context kontekst) {
        String nazwaJednostki = etxNazwaJednostki.getText().toString(), skrótJednostki = etxSkrótJednostki.getText().toString();
        if (nazwaJednostki.equals("")) {
            Toast.makeText(kontekst, Wyświetlanie_Tekstu.błądPodajNazwęJednostki(), Toast.LENGTH_LONG).show();
        }
        else {
            Jednostki nowaJednostka = new Jednostki();

            int idNowego = 0;
            boolean nieMamyId;

            do {
                nieMamyId = false;
                for (Jednostki jednostka : Zmienne_Globalne.listaJednostek) {
                    if (jednostka.getId_PK() == idNowego) {
                        idNowego++;
                        nieMamyId = true;
                    }
                }
            } while (nieMamyId);

            nowaJednostka.setId_PK(idNowego);
            nowaJednostka.setNazwa(nazwaJednostki);
            nowaJednostka.setSkrót(skrótJednostki);
            idNowejJednostki=idNowego;

            try {
                nowaJednostka.zapiszDoBazy(kontekst);
                Zmienne_Globalne.listaJednostek.add(nowaJednostka);
                Metody_Pomocnicze.tostKrótki(kontekst, Wyświetlanie_Tekstu.guiJednostkaZostałaDodana());
            } catch (Exception e) {
                Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądJednostkaNieDodana());
            }

        }
    }

    public void działaniaKończąceEdytujIstniejącą(Context kontekst) {
        String nazwaJednostki = etxNazwaJednostki.getText().toString(), skrótJednostki = etxSkrótJednostki.getText().toString();
        if (nazwaJednostki.equals("")) {
            Toast.makeText(kontekst, Wyświetlanie_Tekstu.błądPodajNazwęJednostki(), Toast.LENGTH_LONG).show();
        }
        else {
            try {
                jednostka.setNazwa(nazwaJednostki);
                jednostka.setSkrót(skrótJednostki);
                jednostka.aktualizujDoBazy(kontekst);
                Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.guiZapisano());
            }
            catch (Exception e) { Metody_Pomocnicze.tostDługi(kontekst, Wyświetlanie_Tekstu.błądZapisu()); }
        }
    }

}