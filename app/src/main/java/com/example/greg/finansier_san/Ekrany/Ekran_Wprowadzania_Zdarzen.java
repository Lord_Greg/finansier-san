package com.example.greg.finansier_san.Ekrany;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.greg.finansier_san.Dialogi.Dialog_Przesuniecia_Daty;
import com.example.greg.finansier_san.Dialogi.Dialog_Wprowadzania_Grup_Zdarzen;
import com.example.greg.finansier_san.Dialogi.Dialog_Wyboru_Daty;
import com.example.greg.finansier_san.Metody_Pomocnicze;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Grupy_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Jednostki;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Ważności;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Kategorie_Zdarzeń;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Przychody;
import com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD.Rozchody;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.R;
import com.example.greg.finansier_san.Teksty_Interfejsu.Wyświetlanie_Tekstu;
import com.example.greg.finansier_san.Zmienne_Globalne;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Greg on 2015-12-02.
 */
public class Ekran_Wprowadzania_Zdarzen extends AppCompatActivity {

    boolean czyToRozchód = true;
    boolean czyNoweZdarzenie = true;
    String nazwaStarejGrupy = "";

    Grupy_Zdarzeń edytowanaGrupa;
    Rozchody edytowanyRozchód = new Rozchody();
    Kategorie_Ważności edytowanaKategoriaważności;
    Przychody edytowanyPrzychód;

    Date staraDataZdarzenia = new Date();
    Date poprzedniaData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ekran_wprowadzania_zdarzen);

        final Date dataZdarzenia = new Date();
        poprzedniaData = new Date(Zmienne_Globalne.wybranaData.getYear(), Zmienne_Globalne.wybranaData.getMonth(), Zmienne_Globalne.wybranaData.getDate());

        //region Inicjalizacja kontrolek
        RadioButton rbtPrzychód = (RadioButton)findViewById(R.id.rbtPrzychod);
        final RadioButton rbtRozchód = (RadioButton)findViewById(R.id.rbtRozchod);
        final Spinner spnGrupaZdarzeń = (Spinner)findViewById(R.id.spnGrupaZdarzen);
        final Button btnGrupaZdarzeńDodaj = (Button)findViewById(R.id.btnDodajGrupeZdarzen);
        final EditText etxnNumerZdarzenia = (EditText)findViewById(R.id.etxnNumerZdarzenia);
        final EditText etxnKwota = (EditText)findViewById(R.id.etxnKwota);
        final ListView lvwData = (ListView)findViewById(R.id.lvwData);
        final EditText etxOpis = (EditText)findViewById(R.id.etxOpis);
        final CheckBox chbCzyWłączPowiadomienia = (CheckBox)findViewById(R.id.chbCzyWlaczPowiadomienia);
        final CheckBox chbCzyUkryć = (CheckBox)findViewById(R.id.chbCzyUkryc);
        final TextView txvNKategoriaWażności = (TextView)findViewById(R.id.txvWaznosc);
        final Spinner spnKategoriaWażności = (Spinner)findViewById(R.id.spnWaznosc);
        Button btnDodajZdarzenie = (Button)findViewById(R.id.btnDodaj);
        Button btnAnulujZdarzenie = (Button)findViewById(R.id.btnAnuluj);

        TextView txvNagłówekNumerWystąpienia = (TextView)findViewById(R.id.txvNumerWystapienia);
        TextView txvNagłówekEkranNoweZdarzenie = (TextView)findViewById(R.id.txvEkranNoweZdarzenie);

        TextView txvNNazwaGrupyZdarzeń = (TextView)findViewById(R.id.txvNazwa);
        TextView txvNNumerWystąpienia = (TextView)findViewById(R.id.txvNumerWystapienia);
        TextView txvNKwota = (TextView)findViewById(R.id.txvKwota);
        TextView txvNData = (TextView)findViewById(R.id.txvData);
        TextView txvNOpis = (TextView)findViewById(R.id.txvOpis);

        //region Ustawianie tekstu kontrolek
        txvNagłówekEkranNoweZdarzenie.setText(Wyświetlanie_Tekstu.gui_Nagłówek_noweZdarzenie());
        rbtPrzychód.setText(Wyświetlanie_Tekstu.guiPrzychód());
        rbtRozchód.setText(Wyświetlanie_Tekstu.guiRozchód());
        txvNNazwaGrupyZdarzeń.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wybierzNazwęZdarzenia());
        btnGrupaZdarzeńDodaj.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        txvNNumerWystąpienia.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajNumerWystąpienia_());
        etxnNumerZdarzenia.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_numerWystąpienia());
        txvNKwota.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajKwotę_());
        etxnKwota.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_kwota());
        txvNData.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajDatęZdarzenia_());
        txvNOpis.setText(Wyświetlanie_Tekstu.gui_Nagłówek_podajOpis_());
        etxOpis.setHint(Wyświetlanie_Tekstu.gui_Nagłówek_opis());
        chbCzyWłączPowiadomienia.setText(Wyświetlanie_Tekstu.gui_Nagłówek_włączPowiadamienia());
        chbCzyUkryć.setText(Wyświetlanie_Tekstu.gui_Nagłówek_niePokazuj());
        txvNKategoriaWażności.setText(Wyświetlanie_Tekstu.gui_Nagłówek_wybierzKategorięWażności_());
        btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.czynnościDodaj());
        btnAnulujZdarzenie.setText(Wyświetlanie_Tekstu.gui_nagłówek_anuluj());
        //endregion

        załadujGrupyZdarzeńDoSpinnera(spnGrupaZdarzeń, Zmienne_Globalne.listaGrupZdarzeń);
        załadujKategorieWażnościDoSpinnera(spnKategoriaWażności, Zmienne_Globalne.listaKategoriiWażności);


        Bundle przekazaneWartości = getIntent().getExtras();
        if(przekazaneWartości!=null)
        {
            //region Inicjalizacja przy edycji zdarzenia
            czyNoweZdarzenie = false;

            spnGrupaZdarzeń.setEnabled(false);
            btnGrupaZdarzeńDodaj.setEnabled(false);
            btnGrupaZdarzeńDodaj.setVisibility(View.INVISIBLE);
            etxnNumerZdarzenia.setEnabled(false);

            txvNagłówekEkranNoweZdarzenie.setText(Wyświetlanie_Tekstu.guiEdytujZdarzenie());
            txvNagłówekNumerWystąpienia.setText(Wyświetlanie_Tekstu.guiNumerEdytowanegoZdarzenia());
            btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.czynnościZapisz());

            edytowanaGrupa = Metody_Pomocnicze.znajdźGrupęZdarzeń(przekazaneWartości.getString("nazwaGrupy"));
            nazwaStarejGrupy=edytowanaGrupa.getNazwaGrupyZdarzeń_PK();

            spnGrupaZdarzeń.setSelection(Zmienne_Globalne.listaGrupZdarzeń.indexOf(edytowanaGrupa));

            czyToRozchód = przekazaneWartości.getBoolean("czyToRozchód");

            if(czyToRozchód) {
                txvNKategoriaWażności.setVisibility(View.VISIBLE);
                spnKategoriaWażności.setVisibility(View.VISIBLE);
                chbCzyUkryć.setVisibility(View.INVISIBLE);

                edytowanyRozchód = Metody_Pomocnicze.znajdźRozchód(edytowanaGrupa.getNazwaGrupyZdarzeń_PK(), przekazaneWartości.getInt("numer"));
                edytowanaKategoriaważności = Metody_Pomocnicze.znajdźKategorieWażności(edytowanyRozchód.getPoziomWażności_FK());

                etxnNumerZdarzenia.setText(String.valueOf(edytowanyRozchód.getNumerWystąpienia_PK()));

                spnKategoriaWażności.setSelection(edytowanaKategoriaważności.getPoziomWażności_PK());
                rbtRozchód.setChecked(true);
                etxnKwota.setText(String.valueOf(edytowanyRozchód.getKwota()));
                Metody_Pomocnicze.przepiszDate(edytowanyRozchód.getData(), dataZdarzenia);

                chbCzyWłączPowiadomienia.setChecked(edytowanyRozchód.getCzyPowiadomieniaWłączone());
                etxOpis.setText(edytowanyRozchód.getOpis());

                Metody_Pomocnicze.przepiszDate(edytowanyRozchód.getData(), staraDataZdarzenia);
            }
            else{
                txvNKategoriaWażności.setVisibility(View.INVISIBLE);
                spnKategoriaWażności.setVisibility(View.INVISIBLE);

                edytowanyPrzychód = Metody_Pomocnicze.znajdźPrzychód(edytowanaGrupa.getNazwaGrupyZdarzeń_PK(), przekazaneWartości.getInt("numer"));

                etxnNumerZdarzenia.setText(String.valueOf(edytowanyPrzychód.getNumerWystąpienia_PK()));

                rbtPrzychód.setChecked(true);
                etxnKwota.setText(String.valueOf(edytowanyPrzychód.getKwota()));
                Metody_Pomocnicze.przepiszDate(edytowanyPrzychód.getData(), dataZdarzenia);
                chbCzyWłączPowiadomienia.setChecked(edytowanyPrzychód.getCzyPowiadomieniaWłączone());
                chbCzyUkryć.setChecked(!edytowanyPrzychód.getCzyWyświetlany());
                etxOpis.setText(edytowanyPrzychód.getOpis());

                Metody_Pomocnicze.przepiszDate(edytowanyPrzychód.getData(), staraDataZdarzenia);
            }
            //endregion
        }
        else
        {
            //region Inicjalizacja przy nowym zdarzeniu
            chbCzyUkryć.setVisibility(View.INVISIBLE);

            txvNagłówekEkranNoweZdarzenie.setText(Wyświetlanie_Tekstu.guiNoweZdarzenie());
            txvNagłówekNumerWystąpienia.setText(Wyświetlanie_Tekstu.guiPodajNumerZdarzenia());
            btnDodajZdarzenie.setText(Wyświetlanie_Tekstu.czynnościZapisz());

            Metody_Pomocnicze.przepiszDate(Zmienne_Globalne.wybranaData, dataZdarzenia);
            nazwaStarejGrupy = "";
            //endregion
        }

        rbtRozchód.setEnabled(czyNoweZdarzenie);
        rbtPrzychód.setEnabled(czyNoweZdarzenie);

        Metody_Pomocnicze.przepiszDate(dataZdarzenia, Zmienne_Globalne.wybranaData);

        załadujDatęDoListView(lvwData, Zmienne_Globalne.wybranaData);

        //endregion


        rbtRozchód.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    czyToRozchód = true;
                    chbCzyUkryć.setVisibility(View.INVISIBLE);

                    txvNKategoriaWażności.setVisibility(View.VISIBLE);
                    spnKategoriaWażności.setVisibility(View.VISIBLE);
                } else {
                    czyToRozchód = false;
                    txvNKategoriaWażności.setVisibility(View.INVISIBLE);
                    spnKategoriaWażności.setVisibility(View.INVISIBLE);

                    chbCzyUkryć.setVisibility(View.VISIBLE);
                }
            }
        });


        lvwData.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Dialog_Wyboru_Daty dlgWybierzDate = new Dialog_Wyboru_Daty();
                dlgWybierzDate.działajBezOk(Ekran_Wprowadzania_Zdarzen.this);

                dlgWybierzDate.btnOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dlgWybierzDate.czynnościKońcowe();

                        Metody_Pomocnicze.przepiszDate(Zmienne_Globalne.wybranaData, dataZdarzenia);
                        załadujDatęDoListView(lvwData, Zmienne_Globalne.wybranaData);

                        dlgWybierzDate.dialog.dismiss();
                    }
                });
            }
        });


        btnGrupaZdarzeńDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog_Wprowadzania_Grup_Zdarzen dlgDodajGrupęZdarzeń = new Dialog_Wprowadzania_Grup_Zdarzen(Ekran_Wprowadzania_Zdarzen.this);
                dlgDodajGrupęZdarzeń.działajBezOk(Ekran_Wprowadzania_Zdarzen.this);

                dlgDodajGrupęZdarzeń.btnDodaj.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        {
                            dlgDodajGrupęZdarzeń.działaniaKończąceStandardowe(Ekran_Wprowadzania_Zdarzen.this);

                            załadujGrupyZdarzeńDoSpinnera(spnGrupaZdarzeń, Zmienne_Globalne.listaGrupZdarzeń);
                            spnGrupaZdarzeń.setSelection(spnGrupaZdarzeń.getAdapter().getCount() - 1);

                            dlgDodajGrupęZdarzeń.dialog.dismiss();
                        }
                    }
                });
            }
        });


        btnDodajZdarzenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //region Dodawanie i edycja zdarzenia
                try {
                    if(etxnKwota.getText().toString().equals("")) {
                        throw new Exception(Wyświetlanie_Tekstu.błądPodajKwotę());
                    }

                    double kwota = Double.valueOf(etxnKwota.getText().toString());

                    if(czyNoweZdarzenie) {
                        //region Dodaj nowe zdarzenie
                        if(etxnNumerZdarzenia.getText().toString().equals("")) {
                            throw new Exception(Wyświetlanie_Tekstu.guiPodajNumerZdarzenia());
                        }

                        int numerWystąpienia = Integer.parseInt(etxnNumerZdarzenia.getText().toString());

                        int[] numeryWystąpień = new int[1];
                        numeryWystąpień[0]=numerWystąpienia;

                        int numerIstniejącegoWystąpienia = Metody_Pomocnicze.czyWystąpienieIstnieje(spnGrupaZdarzeń.getSelectedItem().toString(), numeryWystąpień);

                        if(numerIstniejącegoWystąpienia > -1) {
                            throw  new Exception(Wyświetlanie_Tekstu.błądWystąpienieOnumerzeJużIstnieje1() + String.valueOf(numerIstniejącegoWystąpienia) + Wyświetlanie_Tekstu.błądWystąpienieOnumerzeJużIstnieje2());
                        }


                        if (czyToRozchód) //dodajemy rozchód
                        {
                            int poziomWażności = Metody_Pomocnicze.znajdźKategorieWażności(spnKategoriaWażności.getSelectedItem().toString()).getPoziomWażności_PK();

                            Rozchody nowyRozchód = new Rozchody(spnGrupaZdarzeń.getSelectedItem().toString(), numerWystąpienia, kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), poziomWażności, false, false, "", -1);

                            nowyRozchód.zapiszDoBazy(Ekran_Wprowadzania_Zdarzen.this);
                            Zmienne_Globalne.listaRozchodów.add(nowyRozchód);

                        }
                        else //dodajemy przychód
                        {
                            Przychody nowyPrzychód = new Przychody(spnGrupaZdarzeń.getSelectedItem().toString(), numerWystąpienia, kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), !chbCzyUkryć.isChecked());
                            nowyPrzychód.zapiszDoBazy(Ekran_Wprowadzania_Zdarzen.this);
                            Zmienne_Globalne.listaPrzychodów.add(nowyPrzychód);
                        }
                        Metody_Pomocnicze.tostKrótki(Ekran_Wprowadzania_Zdarzen.this, Wyświetlanie_Tekstu.guiZdarzenieDodanoPomyślnie());

                        finish();

                        //endregion
                    }
                    else {
                        //region Edycja istniejącego zdarzenia
                        final int nrWystąpienia;

                        Łącznik połączenieBD = new Łącznik();
                        połączenieBD.połączBD(Ekran_Wprowadzania_Zdarzen.this);

                        if(czyToRozchód) //edytujemy rozchód
                        {
                            int poziomWażności = Metody_Pomocnicze.znajdźKategorieWażności(spnKategoriaWażności.getSelectedItem().toString()).getPoziomWażności_PK();
                            Rozchody zmodyfikowanyRozchód = new Rozchody(spnGrupaZdarzeń.getSelectedItem().toString(), edytowanyRozchód.getNumerWystąpienia_PK(), kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), poziomWażności, edytowanyRozchód.getCzyZapłacone(), edytowanyRozchód.getCzyPosiadane(), edytowanyRozchód.getNazwaPrenumeraty_FK(), edytowanyRozchód.getNrPrenumeraty_FK());

                            zmodyfikowanyRozchód.aktualizujDoBazy(połączenieBD, nazwaStarejGrupy);

                            nrWystąpienia=zmodyfikowanyRozchód.getNumerWystąpienia_PK();
                        }
                        else //edytujemy przychód
                        {
                            Przychody zmodyfikowanyPrzychód = new Przychody(spnGrupaZdarzeń.getSelectedItem().toString(), edytowanyPrzychód.getNumerWystąpienia_PK(), kwota, dataZdarzenia, etxOpis.getText().toString(), chbCzyWłączPowiadomienia.isChecked(), !chbCzyUkryć.isChecked());

                            zmodyfikowanyPrzychód.aktualizujDoBazy(połączenieBD, nazwaStarejGrupy);

                            nrWystąpienia=zmodyfikowanyPrzychód.getNumerWystąpienia_PK();
                        }
                        połączenieBD.rozłączBD();

                        //Przeładowanie list
                        Metody_Pomocnicze.przeładujGrupyZdarzeńZbazy(Ekran_Wprowadzania_Zdarzen.this);
                        Metody_Pomocnicze.przeładujPrzychodyZbazy(Ekran_Wprowadzania_Zdarzen.this);
                        Metody_Pomocnicze.przeładujRozchodyZbazy(Ekran_Wprowadzania_Zdarzen.this);
                        Metody_Pomocnicze.przeładujZdarzeniaWscenariuszachZbazy(Ekran_Wprowadzania_Zdarzen.this);

                        Metody_Pomocnicze.tostKrótki(Ekran_Wprowadzania_Zdarzen.this, Wyświetlanie_Tekstu.guiZapisano());


                        //możliwość zmiany daty kolejnych wystąpień
                        if( (staraDataZdarzenia.getDate()!=dataZdarzenia.getDate() || staraDataZdarzenia.getMonth()!=dataZdarzenia.getMonth() || staraDataZdarzenia.getYear()!=dataZdarzenia.getYear() ) && Metody_Pomocnicze.czyZdarzenieMaNastępcę(edytowanaGrupa.getNazwaGrupyZdarzeń_PK(), nrWystąpienia)){

                            new AlertDialog.Builder(Ekran_Wprowadzania_Zdarzen.this)
                                    .setTitle(Wyświetlanie_Tekstu.tytułNazwaAlertDialoguZmianaDaty())
                                    .setMessage(Wyświetlanie_Tekstu.guiCzyChceszPrzesunąćDaty())
                                    .setPositiveButton(Wyświetlanie_Tekstu.guiTak(), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            //Zmień

                                            final Dialog_Przesuniecia_Daty dlgPrzesuniętaData = new Dialog_Przesuniecia_Daty();
                                            dlgPrzesuniętaData.działajBezOk(Ekran_Wprowadzania_Zdarzen.this, staraDataZdarzenia, dataZdarzenia);

                                            dlgPrzesuniętaData.btnOk.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {

                                                    int plusDni, plusMiesiące, plusLata, ileZdarzeń = 0;
                                                    plusDni = Integer.parseInt(dlgPrzesuniętaData.etxnIleDni.getText().toString());
                                                    plusMiesiące = Integer.parseInt(dlgPrzesuniętaData.etxnIleMiesięcy.getText().toString());
                                                    plusLata = Integer.parseInt(dlgPrzesuniętaData.etxnIleLat.getText().toString());
                                                    Date dataPomocnicza;

                                                    Łącznik połączenieBD = new Łącznik();
                                                    połączenieBD.połączBD(Ekran_Wprowadzania_Zdarzen.this);

                                                    for (Przychody przychód : Zmienne_Globalne.listaPrzychodów) {
                                                        if (edytowanaGrupa.getNazwaGrupyZdarzeń_PK().equals(przychód.getNazwaGrupy_PK_FK()) && przychód.getNumerWystąpienia_PK() > nrWystąpienia) {
                                                            dataPomocnicza = new Date(przychód.getData().getYear() + plusLata, przychód.getData().getMonth() + plusMiesiące, przychód.getData().getDate() + plusDni);
                                                            przychód.setData(dataPomocnicza);
                                                            przychód.aktualizujDatęDoBazy(połączenieBD);
                                                            ileZdarzeń++;
                                                        }
                                                    }
                                                    for (Rozchody rozchód : Zmienne_Globalne.listaRozchodów) {
                                                        if (edytowanaGrupa.getNazwaGrupyZdarzeń_PK().equals(rozchód.getNazwaGrupy_PK_FK()) && rozchód.getNumerWystąpienia_PK() > nrWystąpienia) {
                                                            dataPomocnicza = new Date(rozchód.getData().getYear() + plusLata, rozchód.getData().getMonth() + plusMiesiące, rozchód.getData().getDate() + plusDni);
                                                            rozchód.setData(dataPomocnicza);
                                                            rozchód.aktualizujDatęDoBazy(połączenieBD);
                                                            ileZdarzeń++;
                                                        }
                                                    }
                                                    połączenieBD.rozłączBD();

                                                    dlgPrzesuniętaData.dialog.dismiss();
                                                    Metody_Pomocnicze.tostDługi(Ekran_Wprowadzania_Zdarzen.this, Wyświetlanie_Tekstu.guiWykonanoDlaZdarzeń1() + String.valueOf(ileZdarzeń) + Wyświetlanie_Tekstu.guiWykonanoDlaZdarzeń2());

                                                    finish();
                                                }
                                            });

                                        }
                                    })
                                    .setNegativeButton(Wyświetlanie_Tekstu.guiNie(), new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            // Nie zmieniaj
                                            finish();
                                        }
                                    })
                                    .setIcon(android.R.drawable.ic_dialog_alert)
                                    .show();
                        }
                        else{
                            finish();
                        }

                        //endregion
                    }
                }
                catch (Exception e){
                    Metody_Pomocnicze.tostDługi(Ekran_Wprowadzania_Zdarzen.this, e.getMessage());
                }
                //endregion
            }
        });

        btnAnulujZdarzenie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Metody_Pomocnicze.przepiszDate(poprzedniaData, Zmienne_Globalne.wybranaData);
                finish();
            }
        });
    }


    private void załadujDatęDoListView(ListView lvwDoUstawienia, Date data){
        ArrayAdapter adapter;
        String[] daneDoAdaptera = new String[] { String.valueOf(data.getDate()) + " " + Metody_Pomocnicze.zwróćNazwęMiesiąca(data.getMonth()) + " " + String.valueOf(data.getYear()+Zmienne_Globalne.y) };
        adapter = new ArrayAdapter(Ekran_Wprowadzania_Zdarzen.this, android.R.layout.simple_list_item_1, daneDoAdaptera);
        lvwDoUstawienia.setAdapter(adapter);

    }

    private void załadujGrupyZdarzeńDoSpinnera(Spinner spnGrupa, List<Grupy_Zdarzeń> listaGrup){
        List<String> dane = new ArrayList<String>();
        for (Grupy_Zdarzeń grupa:listaGrup) {
            dane.add(grupa.getNazwaGrupyZdarzeń_PK());
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Wprowadzania_Zdarzen.this, android.R.layout.simple_spinner_dropdown_item, dane);
        spnGrupa.setAdapter(adapter);
    }

    private void załadujKategorieWażnościDoSpinnera(Spinner spnKategoria, List<Kategorie_Ważności> listaKategorii){
        List<String> dane = new ArrayList<String>();
        for (Kategorie_Ważności kategoria:listaKategorii) {
            dane.add(kategoria.getSymbol());
        }
        ArrayAdapter adapter = new ArrayAdapter(Ekran_Wprowadzania_Zdarzen.this, android.R.layout.simple_spinner_dropdown_item, dane);
        spnKategoria.setAdapter(adapter);
    }

    private int znajdźPozycjeElementuSpinnera(String szukanaWartość, Spinner spinner){
        for (int i=0; i<spinner.getCount(); i++) {
            if(spinner.getItemAtPosition(i).equals(szukanaWartość)){
                return i;
            }
        }
        return -1; //pilnować żeby się nie wykonało
    }

}
