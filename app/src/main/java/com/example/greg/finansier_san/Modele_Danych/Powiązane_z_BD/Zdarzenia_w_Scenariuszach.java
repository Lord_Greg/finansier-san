package com.example.greg.finansier_san.Modele_Danych.Powiązane_z_BD;

import android.content.Context;
import android.database.Cursor;

import com.example.greg.finansier_san.Ekrany.Ekran_Glowny;
import com.example.greg.finansier_san.Obsługa_BD.Opisy;
import com.example.greg.finansier_san.Obsługa_BD.Polecenia_SQL;
import com.example.greg.finansier_san.Obsługa_BD.Łącznik;
import com.example.greg.finansier_san.Zmienne_Globalne;

/**
 * Created by Greg on 2015-11-29.
 */
public class Zdarzenia_w_Scenariuszach {
    private String nazwaZdarzenia_PK_FK;
    private int numerWystąpieniaZdarzenia_PK_FK;
    private String nazwaScenariusza_PK_FK;

    public Zdarzenia_w_Scenariuszach(String nazwaZdarzenia, int nrZdarzenia, String nazwaScenariusza){
        nazwaZdarzenia_PK_FK=nazwaZdarzenia;
        numerWystąpieniaZdarzenia_PK_FK=nrZdarzenia;
        nazwaScenariusza_PK_FK=nazwaScenariusza;
    }


    //region Gettery/Settery
    public String getNazwaZdarzenia_PK_FK() {
        return nazwaZdarzenia_PK_FK;
    }

    public void setNazwaZdarzenia_PK_FK(String nazwaZdarzenia_PK_FK) {
        this.nazwaZdarzenia_PK_FK = nazwaZdarzenia_PK_FK;
    }


    public int getNumerWystąpieniaRozchodu_PK_FK() {
        return numerWystąpieniaZdarzenia_PK_FK;
    }

    public void setNumerWystąpieniaRozchodu_PK_FK(int numerWystąpieniaWRozchodu_PK_FK) {
        this.numerWystąpieniaZdarzenia_PK_FK = numerWystąpieniaWRozchodu_PK_FK;
    }


    public String getNazwaScenariusza_PK_FK() {
        return nazwaScenariusza_PK_FK;
    }

    public void setNazwaScenariusza_PK_FK(String nazwaScenariusza_PK_FK) {
        this.nazwaScenariusza_PK_FK = nazwaScenariusza_PK_FK;
    }

    //endregion


    public static void wczytajWszystkoZbazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);
        Cursor kursor = połączenieBD.baza.rawQuery(Polecenia_SQL.wczytajCałąTabele(Opisy.tab_ZdarzeniaWscenariuszach), null);

        if(kursor.moveToFirst()){
            while (!kursor.isAfterLast())
            {
                Zmienne_Globalne.listaZdarzeńWscenariuszach.add(new Zdarzenia_w_Scenariuszach(kursor.getString(0), kursor.getInt(1), kursor.getString(2)));
                kursor.moveToNext();
            }
        }

        kursor.close();
        połączenieBD.rozłączBD();
    }

    public void zapiszDoBazy(Context kontekst){
        Łącznik połączenieBD = new Łącznik();
        połączenieBD.połączBD(kontekst);

        String[] wartości = {
                nazwaZdarzenia_PK_FK,
                String.valueOf(numerWystąpieniaZdarzenia_PK_FK),
                nazwaScenariusza_PK_FK
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.wstawWartości(Opisy.tab_ZdarzeniaWscenariuszach, wartości));

        połączenieBD.rozłączBD();
    }

    public static void aktualizujNazwęZdarzeniaDoBazy(Łącznik połączenieBD, String staraNazwaZdarzenia, String nowaNazwaZdarzenia){
        String[] nazwyPól = {
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk
        };

        String[] wartości = {
                nowaNazwaZdarzenia
        };

        String warunek = Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk + "='" + staraNazwaZdarzenia + "'";

        połączenieBD.baza.execSQL(Polecenia_SQL.aktualizujNPól(Opisy.tab_ZdarzeniaWscenariuszach, nazwyPól, wartości, warunek));
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń, int numerWystąpienia, String nazwaScenariusza){
        String[] nazwyPól = {
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk,
                Opisy.zdarzeniaWscenariuszach_nrWystąpieniaPKfk,
                Opisy.zdarzeniaWscenariuszach_nazwaScenariuszaPKfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń,
                String.valueOf(numerWystąpienia),
                nazwaScenariusza
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_ZdarzeniaWscenariuszach, nazwyPól, wartości));
    }

    public static void usuńZbazy(Łącznik połączenieBD, String nazwaGrupyZdarzeń){
        String[] nazwyPól = {
                Opisy.zdarzeniaWscenariuszach_nazwaZdarzeniaPkfk
        };

        String[] wartości = {
                nazwaGrupyZdarzeń
        };

        połączenieBD.baza.execSQL(Polecenia_SQL.usuńRekordy(Opisy.tab_ZdarzeniaWscenariuszach, nazwyPól, wartości));
    }

}
