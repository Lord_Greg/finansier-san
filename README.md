# Finansier-san #
Napisany od podstaw kalendarz z opcją zaznaczania przychodów i wydatków (pojedynczych lub cyklicznych), przeznaczony na urządzenia z systemem Android. Aplikacja podlicza także bilans dla każdego tygodnia oraz generuje listę bieżących i nadchodzących zakupów.

Aplikacja zaliczeniowa z przedmiotu "Programowanie i obsługa systemów mobilnych" - Styczeń 2016.